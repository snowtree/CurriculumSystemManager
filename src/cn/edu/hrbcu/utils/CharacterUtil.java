package cn.edu.hrbcu.utils;

public class CharacterUtil {
	public static final String[] roman_char =  { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
	
	public static String num2roman(Integer num){
		if(num == null)
			return "";
		
		return roman_char[num - 1];
	}
}
