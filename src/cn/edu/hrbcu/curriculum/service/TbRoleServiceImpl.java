package cn.edu.hrbcu.curriculum.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;

import cn.edu.hrbcu.curriculum.dao.TbRoleMapper;
import cn.edu.hrbcu.curriculum.pojo.TbRole;
import cn.edu.hrbcu.curriculum.pojo.TbRoleExample;
import cn.edu.hrbcu.utils.Common;

/**
 * @author 尤广富
 * 2019年4月8日
 * TODO
 */
@Service
public class TbRoleServiceImpl implements TbRoleService{

	@Autowired
	TbRoleMapper tbRoleMapper;
	
	@Override
	public int insertRole(TbRole role) {
		// TODO Auto-generated method stub
		return tbRoleMapper.insert(role);
	}

	@Override
	public List<TbRole> selectRoleByName(String name) {
		// TODO Auto-generated method stub
		TbRoleExample example = new TbRoleExample();
		TbRoleExample.Criteria criteria = example.createCriteria();
		criteria.andNameEqualTo(name);
		return tbRoleMapper.selectByExample(example);
	}

	@Override
	public int updateTbRole(TbRole tbRole) {
		// TODO Auto-generated method stub
		return tbRoleMapper.updateByPrimaryKey(tbRole);
	}

	@Override
	public TbRole selectTbRoleById(TbRole tbRole) {
		// TODO Auto-generated method stub
		return tbRoleMapper.selectByPrimaryKey(tbRole.getId());
	}

	@Override
	public void deletTbRole(TbRole tbRole) {
		// TODO Auto-generated method stub
		tbRoleMapper.deleteByPrimaryKey(tbRole.getId());
	}

	@Override
	public List<TbRole> selectTbRoles(int currentPage) {
		// TODO Auto-generated method stub
		if(currentPage >= 0) {
			PageHelper.startPage(currentPage,Common.PAGE_SIZE);
		}
		TbRoleExample example = new TbRoleExample();
		TbRoleExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(0L);
		return tbRoleMapper.selectByExample(example);
	}

	@Override
	public List<TbRole> selectAllRole() {
		// TODO Auto-generated method stub
		TbRoleExample example = new TbRoleExample();
		TbRoleExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(-1L);
		return tbRoleMapper.selectByExample(example);
	}

}
