package cn.edu.hrbcu.curriculum.service;

import java.util.List;

import cn.edu.hrbcu.curriculum.pojo.TbRolePermission;

/**
 * @author Youguangfu
 *
 */
public interface TbRolePermissionService {
	int insertUserRolePermission(TbRolePermission userRolePermission);

	List<TbRolePermission> selectUserRolePermissionByName(String name);

	int updateTbRolePermission(TbRolePermission tbRolePermission);

	TbRolePermission selectTbRolePermissionById(TbRolePermission tbRolePermission);

	void deletTbRolePermission(TbRolePermission tbRolePermission);
	
	List<TbRolePermission> selectTbRolePermissions(int currentPage);

	List<TbRolePermission> selectAllUserRolePermission();

}
