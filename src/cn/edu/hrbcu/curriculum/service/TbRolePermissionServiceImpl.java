package cn.edu.hrbcu.curriculum.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.github.pagehelper.PageHelper;

import cn.edu.hrbcu.curriculum.dao.TbRolePermissionMapper;
import cn.edu.hrbcu.curriculum.pojo.TbRolePermission;
import cn.edu.hrbcu.curriculum.pojo.TbRolePermissionExample;
import cn.edu.hrbcu.utils.Common;

public class TbRolePermissionServiceImpl implements TbRolePermissionService {

	
	@Autowired
	TbRolePermissionMapper tbRolePermissionMapper;
	
	@Override
	public int insertUserRolePermission(TbRolePermission userRolePermission) {
		// TODO Auto-generated method stub
		return tbRolePermissionMapper.insert(userRolePermission);
	}

	@Override
	public List<TbRolePermission> selectUserRolePermissionByName(String name) {
		// TODO Auto-generated method stub
		TbRolePermissionExample permissionExample = new TbRolePermissionExample();
		TbRolePermissionExample.Criteria criteria = permissionExample.createCriteria();
//		criteria.
		return null;
	}

	@Override
	public int updateTbRolePermission(TbRolePermission tbRolePermission) {
		// TODO Auto-generated method stub
		return tbRolePermissionMapper.updateByPrimaryKey(tbRolePermission);
	}

	@Override
	public TbRolePermission selectTbRolePermissionById(TbRolePermission tbRolePermission) {
		// TODO Auto-generated method stub
		return tbRolePermissionMapper.selectByPrimaryKey(tbRolePermission.getId());
	}

	@Override
	public void deletTbRolePermission(TbRolePermission tbRolePermission) {
		// TODO Auto-generated method stub
		tbRolePermissionMapper.deleteByPrimaryKey(tbRolePermission.getId());
	}

	@Override
	public List<TbRolePermission> selectTbRolePermissions(int currentPage) {
		// TODO Auto-generated method stub
		if(currentPage >= 0) {
			PageHelper.startPage(currentPage,Common.PAGE_SIZE);
		}
		TbRolePermissionExample example = new TbRolePermissionExample();
		TbRolePermissionExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(0);
		return tbRolePermissionMapper.selectByExample(example);
	}

	@Override
	public List<TbRolePermission> selectAllUserRolePermission() {
		// TODO Auto-generated method stub
		TbRolePermissionExample example = new TbRolePermissionExample();
		TbRolePermissionExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(-1);
		return tbRolePermissionMapper.selectByExample(example);
	}

}
