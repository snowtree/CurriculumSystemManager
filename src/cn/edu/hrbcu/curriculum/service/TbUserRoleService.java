package cn.edu.hrbcu.curriculum.service;

import java.util.List;

import cn.edu.hrbcu.curriculum.pojo.TbRole;
import cn.edu.hrbcu.curriculum.pojo.TbUserRole;

/**
 * @author Youguangfu
 *
 */
public interface TbUserRoleService {
	int insertUserRole(TbUserRole userRole);

	List<TbUserRole> selectUserRoleByName(String name);

	int updateTbUserRole(TbUserRole tbUserRole);

	TbUserRole selectTbUserRoleById(TbUserRole tbUserRole);

	void deletTbUserRole(TbUserRole tbUserRole);
	
	List<TbUserRole> selectTbUserRoles(int currentPage);

	List<TbUserRole> selectAllUserRole();

	List<TbRole> selectRoleByUser(Long userId,Boolean isAssigned);
}
