package cn.edu.hrbcu.curriculum.service;

import java.util.List;
import cn.edu.hrbcu.curriculum.pojo.Curriculum;

public interface CurriculumService {
	
	int insertCurriculum(Curriculum curriculum);

	List<Curriculum> selectAllCurriculum();

	void deleteCurriculum(Curriculum curriculum);

	Curriculum selectCurriculumById(Curriculum curriculum);
	
	List<Curriculum> selectAllCurriculumBySpecialityandSemester(Long specialityid, int semester);
	
	List<Curriculum> selectAllCurriculumBySpecialityandCourse(Long specialityid, Long courseid);
	
	List<Curriculum> selectAllCurriculumBySpecialityandCourseType(Long specialityid, Long coursetypeId);

	List<Curriculum> selectCurriculums(int currentPage);
	
	int updateCurriculum(Long specialityid, int semester,List<Curriculum> curriculums);
	
	List<Long> selectCurriculumIdsBy(Long specialityid,int semester, List<Long> ids);
	
	List<Curriculum> selectAllCurriculumBySpeciality(Long specialityid,Boolean is);

}
