package cn.edu.hrbcu.curriculum.service;

import cn.edu.hrbcu.curriculum.pojo.Coursetype;
import java.util.List;

public interface CoursetypeService {
	int insertCoursetype(Coursetype coursetype);
	
	List<Coursetype> selectAllCoursetype();
	
	List<Coursetype> selectCoursetypes(int currentPage);
	
	List<Coursetype> selectCoursetypesByPlatform(Long platformid);
}
