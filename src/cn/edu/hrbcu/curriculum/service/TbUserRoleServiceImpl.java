package cn.edu.hrbcu.curriculum.service;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;

import cn.edu.hrbcu.curriculum.dao.TbRoleMapper;
import cn.edu.hrbcu.curriculum.dao.TbUserRoleMapper;
import cn.edu.hrbcu.curriculum.pojo.TbRole;
import cn.edu.hrbcu.curriculum.pojo.TbRoleExample;
import cn.edu.hrbcu.curriculum.pojo.TbUserRole;
import cn.edu.hrbcu.curriculum.pojo.TbUserRoleExample;
import cn.edu.hrbcu.utils.Common;

/**
 * @author Youguangfu
 *
 */
@Service
public class TbUserRoleServiceImpl implements TbUserRoleService {
	@Autowired
	TbUserRoleMapper tbUserRoleMapper;
	@Autowired
	TbRoleMapper roleMapper;
	@Autowired
	TbRoleService tbRoleService;
	
	@Override
	public int insertUserRole(TbUserRole userUserRole) {
		// TODO Auto-generated method stub
		return tbUserRoleMapper.insert(userUserRole);
	}

	@Override
	public List<TbUserRole> selectUserRoleByName(String name) {
		// TODO Auto-generated method stub
		TbUserRoleExample example = new TbUserRoleExample();
		TbUserRoleExample.Criteria criteria = example.createCriteria();
//		criteria.andandNameEqualTo(name);
		return tbUserRoleMapper.selectByExample(example);
	}

	@Override
	public int updateTbUserRole(TbUserRole tbUserRole) {
		// TODO Auto-generated method stub
		return tbUserRoleMapper.updateByPrimaryKey(tbUserRole);
	}

	@Override
	public TbUserRole selectTbUserRoleById(TbUserRole tbUserRole) {
		// TODO Auto-generated method stub
		return tbUserRoleMapper.selectByPrimaryKey(tbUserRole.getId());
	}

	@Override
	public void deletTbUserRole(TbUserRole tbUserRole) {
		// TODO Auto-generated method stub
		tbUserRoleMapper.deleteByPrimaryKey(tbUserRole.getId());
	}

	@Override
	public List<TbUserRole> selectTbUserRoles(int currentPage) {
		// TODO Auto-generated method stub
		if(currentPage >= 0) {
			PageHelper.startPage(currentPage,Common.PAGE_SIZE);
		}
		TbUserRoleExample example = new TbUserRoleExample();
		TbUserRoleExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(0L);
		return tbUserRoleMapper.selectByExample(example);
	}

	@Override
	public List<TbUserRole> selectAllUserRole() {
		// TODO Auto-generated method stub
		TbUserRoleExample example = new TbUserRoleExample();
		TbUserRoleExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(-1L);
		return tbUserRoleMapper.selectByExample(example);
	}

	@Override
	public List<TbRole> selectRoleByUser(Long userId,Boolean isAssigned) {
		// TODO Auto-generated method stub
		List<Long> ids = new ArrayList<Long>();
		List<Long> allRoleIds = new ArrayList<Long>();
		TbUserRoleExample example = new TbUserRoleExample();
		TbUserRoleExample.Criteria criteria = example.createCriteria();
		criteria.andUseridEqualTo(userId);
		
		List<TbUserRole> list = tbUserRoleMapper.selectByExample(example);
		List<TbRole> allRoles = tbRoleService.selectAllRole();
		for (TbRole tbRole : allRoles) {
			allRoleIds.add(tbRole.getId());
		}
		
		
		if(list != null && list.size() > 0){
			for (TbUserRole tbUserRole : list) {
				ids.add(tbUserRole.getRoleid());
			}
			
			TbRoleExample exa = new TbRoleExample();
			TbRoleExample.Criteria c = exa.createCriteria();
			if(isAssigned){
				c.andIdIn(ids);
			}else{
				allRoleIds.removeAll(ids);
				c.andIdIn(allRoleIds);
			}
			return roleMapper.selectByExample(exa);
		}else{
			if(isAssigned){
				return null;
			}else{
				return allRoles;
			}
		}
	}

}
