package cn.edu.hrbcu.curriculum.service;

import java.util.List;
import cn.edu.hrbcu.curriculum.pojo.Speciality;

public interface SpecialityService {
	
	int insertUser(Speciality user);

	int updateSpeciality(Speciality speciality);

	Speciality selectSpecialityById(Speciality speciality);

	void deletSpeciality(Speciality speciality);
	
	List<Speciality> selectSpecialitys(int currentPage);

	List<Speciality> selectAllSpeciality();
	

	List<Speciality> selectAllSpecialityByAcademyid(Long academyid);
	
	Speciality selectSpecialityByUserId(Long userid);

}
