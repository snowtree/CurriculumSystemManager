package cn.edu.hrbcu.curriculum.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;

import cn.edu.hrbcu.curriculum.dao.TbPermissionMapper;
import cn.edu.hrbcu.curriculum.pojo.TbPermission;
import cn.edu.hrbcu.curriculum.pojo.TbPermissionExample;
import cn.edu.hrbcu.utils.Common;


/**
 * @author 尤广富
 * 2019年4月8日
 * TODO
 */
@Service
public class TbPermissionServiceImpl implements TbPermissionService {
	@Autowired
	TbPermissionMapper tbPermissionMapper;
	
	@Override
	public int insertPermission(TbPermission permission) {
		// TODO Auto-generated method stub
		return tbPermissionMapper.insert(permission);
	}

	@Override
	public List<TbPermission> selectPermissionByName(String name) {
		// TODO Auto-generated method stub
		TbPermissionExample example = new TbPermissionExample();
		TbPermissionExample.Criteria criteria = example.createCriteria();
		criteria.andNameEqualTo(name);
		return tbPermissionMapper.selectByExample(example);
	}

	@Override
	public int updateTbPermission(TbPermission tbPermission) {
		// TODO Auto-generated method stub
		return tbPermissionMapper.updateByPrimaryKey(tbPermission);
	}

	@Override
	public TbPermission selectTbPermissionById(TbPermission tbPermission) {
		// TODO Auto-generated method stub
		return tbPermissionMapper.selectByPrimaryKey(tbPermission.getId());
	}

	@Override
	public void deletTbPermission(TbPermission tbPermission) {
		// TODO Auto-generated method stub
		tbPermissionMapper.deleteByPrimaryKey(tbPermission.getId());
	}

	@Override
	public List<TbPermission> selectTbPermissions(int currentPage) {
		// TODO Auto-generated method stub
		if(currentPage >= 0) {
			PageHelper.startPage(currentPage,Common.PAGE_SIZE);
		}
		TbPermissionExample example = new TbPermissionExample();
		TbPermissionExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(0L);
		return tbPermissionMapper.selectByExample(example);
	}

	@Override
	public List<TbPermission> selectAllPermission() {
		// TODO Auto-generated method stub
		TbPermissionExample example = new TbPermissionExample();
		TbPermissionExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(-1L);
		return tbPermissionMapper.selectByExample(example);
	}

}
