package cn.edu.hrbcu.curriculum.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;

import cn.edu.hrbcu.curriculum.dao.CurriculumMapper;
import cn.edu.hrbcu.curriculum.pojo.Course;
import cn.edu.hrbcu.curriculum.pojo.Curriculum;
import cn.edu.hrbcu.curriculum.pojo.CurriculumExample;
import cn.edu.hrbcu.utils.Common;


/**   
*    
* 项目名称：CurriculumSystemManager   
* 类名称：CurriculumServiceImpl   
* 类描述：   
* 创建人：nsow'notepad   
* 创建时间：2019年4月10日 下午8:02:52   
* @version        
*/
@Service
public class CurriculumServiceImpl implements CurriculumService{
	@Autowired
	private CurriculumMapper curriculumMapper;
	@Autowired
	CourseService courseService;
	
	@Override
	public int insertCurriculum(Curriculum curriculum) {
		return curriculumMapper.insert(curriculum);
	}

	
	@Override
	public List<Curriculum> selectAllCurriculum() {
		CurriculumExample example = new CurriculumExample();
		CurriculumExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(-1L);
		return curriculumMapper.selectByExample(example);
	}

	@Override
	public void deleteCurriculum(Curriculum curriculum) {
		curriculumMapper.deleteByPrimaryKey(curriculum.getId());
		
	}

	@Override
	public Curriculum selectCurriculumById(Curriculum curriculum) {
		return curriculumMapper.selectByPrimaryKey(curriculum.getId());
	}

	@Override
	public List<Curriculum> selectAllCurriculumBySpecialityandSemester(Long specialityid, int semester) {
		CurriculumExample example = new CurriculumExample();
		CurriculumExample.Criteria criteria = example.createCriteria();
		criteria.andSpecialityidEqualTo(specialityid);
		criteria.andSemesterEqualTo(semester);
		example.setOrderByClause("sort asc");
		
		return curriculumMapper.selectByExample(example);
	}
	
	@Override
	public List<Curriculum> selectAllCurriculumBySpecialityandCourse(Long specialityid, Long courseid) {
		CurriculumExample example = new CurriculumExample();
		CurriculumExample.Criteria criteria = example.createCriteria();
		criteria.andSpecialityidEqualTo(specialityid);
		criteria.andCourseidEqualTo(courseid);
		example.setOrderByClause("sort asc");
		
		return curriculumMapper.selectByExample(example);
	}
	
	@Override
	public int updateCurriculum(Long specialityid, int semester, List<Curriculum> curriculums) {
		int num = 0 ;
		
		List<Long> new_course_ids = new ArrayList<Long>();
		if(curriculums != null && curriculums.size() > 0){
			for (Curriculum curriculum : curriculums) {
				new_course_ids.add(curriculum.getCourseid());// 新的course的id
			}
		}
		
		List<Long> exist_ids = selectCurriculumIdsBy(specialityid,semester,null);// 所有已在本学期的课程
		List<Long> to_updated_ids = selectCurriculumIdsBy(specialityid,semester,new_course_ids);// 已存在course的id，需要修改他的sort
		List<Curriculum> to_updated_curriculums = selectCurriculumsByIds(to_updated_ids);//需要更新的Curriculum
		List<Curriculum> to_add_curriculums = new ArrayList<Curriculum>();
		
		boolean foundSame = false;
		if(to_updated_curriculums == null || to_updated_curriculums.size() == 0){
			to_add_curriculums.addAll(curriculums);
		}else{
			for (Curriculum curriculum_all_new : curriculums) {
				foundSame = false;
				for (Curriculum curriculum_update : to_updated_curriculums) {
					if(curriculum_all_new.getCourseid().equals(curriculum_update.getCourseid())){
						curriculum_update.setSort(curriculum_all_new.getSort());
						curriculum_update.setStart(curriculum_all_new.getStart());
						curriculum_update.setEnd(curriculum_all_new.getEnd());
						curriculum_update.setHpw(curriculum_all_new.getHpw());
						curriculum_update.setAvailable(curriculum_all_new.getAvailable());
						curriculum_update.setDisperse(curriculum_all_new.getDisperse());
						foundSame = true;
						break;
					}
				}
				
				if(!foundSame){
					to_add_curriculums.add(curriculum_all_new);
				}
			}
		}
		
		List<Long> remove_ids = new ArrayList<Long>();
		remove_ids.addAll(exist_ids);
		remove_ids.removeAll(to_updated_ids);// 需要去除的course的id
		
		// (1)删除没有选择的 deleteCurriculumByIds
		num += deleteCurriculumByIds(remove_ids);
		
		
		// (2)添加新加入的Curriculum
		num += addCurriculum(to_add_curriculums);
		
		
		// (3)更新需要更新的Curriculum
		num += updateCurriculum(to_updated_curriculums);
		
		return num;
	}

	public List<Curriculum> selectCurriculumsByIds(List<Long> ids) {
		if(ids == null || ids.size() == 0){
			return null;
		}
		
		CurriculumExample example = new CurriculumExample();
		CurriculumExample.Criteria criteria = example.createCriteria();
		criteria.andIdIn(ids);
		
		return curriculumMapper.selectByExample(example);
	}
	
	private int addCurriculum(List<Curriculum> curriculums){
		int num = 0;
		if(curriculums != null && curriculums.size() > 0){
			for (Curriculum curriculum : curriculums) {
				num += curriculumMapper.insert(curriculum);
			}
		}
		return num;
	}
	
	private int updateCurriculum(List<Curriculum> curriculums){
		int num = 0;
		if(curriculums != null && curriculums.size() > 0){
			for (Curriculum curriculum : curriculums) {
				num += curriculumMapper.updateByPrimaryKey(curriculum);
			}
		}
		return num;
	}
	
	private int deleteCurriculumByIds(List<Long> ids){
		if(ids == null || ids.size() == 0)
			return 0;
		
		CurriculumExample example = new CurriculumExample();
		CurriculumExample.Criteria criteria = example.createCriteria();
		criteria.andIdIn(ids);
		
		return curriculumMapper.deleteByExample(example);
	}
	
	@Override
	public List<Curriculum> selectCurriculums(int currentPage) {
		if(currentPage >= 0){
			PageHelper.startPage(currentPage, Common.PAGE_SIZE);
		}
		// TODO Auto-generated method stub
		CurriculumExample example = new CurriculumExample();
		CurriculumExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(0L);
		List<Curriculum> list = curriculumMapper.selectByExample(example);
		return list;
	}

	@Override
	public List<Long> selectCurriculumIdsBy(Long specialityid, int semester, List<Long> course_ids) {
		// TODO Auto-generated method stub
		CurriculumExample example = new CurriculumExample();
		CurriculumExample.Criteria criteria = example.createCriteria();
		criteria.andSpecialityidEqualTo(specialityid);
		criteria.andSemesterEqualTo(semester);
		if(course_ids != null && course_ids.size() > 0){
			criteria.andCourseidIn(course_ids);
		}
		
		List<Curriculum> curriculums = curriculumMapper.selectByExample(example);
		
		List<Long> exst_ids = new ArrayList<Long>();
		if(curriculums != null && curriculums.size() > 0){
			for (Curriculum curriculum : curriculums) {
				exst_ids.add(curriculum.getId());
			}
		}
		return exst_ids;
	}

	@Test
	public void TT(){
		String[] ids = null;
		List<Long> new_ids = new ArrayList<Long>();
		new_ids.add(3L);
		new_ids.add(2L);
		
		List<Long> exist_ids = new ArrayList<Long>();
		exist_ids.add(1L);
		exist_ids.add(2L);
		
		List<Long> remove_ids = new ArrayList<Long>();
		remove_ids.addAll(exist_ids);
		remove_ids.removeAll(new_ids);
		
		List<Long> update_ids = new ArrayList<Long>();
		update_ids.addAll(exist_ids);
		update_ids.retainAll(new_ids);
		
		List<Long> add_ids = new ArrayList<Long>();
		add_ids.addAll(new_ids);
		add_ids.removeAll(exist_ids);
	}

	@Override
	public List<Curriculum> selectAllCurriculumBySpecialityandCourseType(Long specialityid, Long coursetypeId) {
		List<Curriculum> curriculums = new ArrayList<Curriculum>();
		
		List<Long> courseids_ = new ArrayList<Long>();
		Course c = new Course();
		c.setCoursetypeid(coursetypeId);
		List<Course> courses_ = courseService.selectCourseBy(c);
		for (Course course : courses_) {
			courseids_.add(course.getId());
		}
		
		CurriculumExample example = new CurriculumExample();
		CurriculumExample.Criteria criteria = example.createCriteria();
		criteria.andSpecialityidEqualTo(specialityid);
		
		if(courseids_ != null && courseids_.size() > 0){
			criteria.andCourseidIn(courseids_);
			curriculums =  curriculumMapper.selectByExample(example);
		}
		
		return curriculums;
	}
	
	@Override
	public List<Curriculum> selectAllCurriculumBySpeciality(Long specialityid,Boolean is) {
		List<Curriculum> curriculums = new ArrayList<Curriculum>();
		
		CurriculumExample example = new CurriculumExample();
		CurriculumExample.Criteria criteria = example.createCriteria();
		criteria.andSpecialityidEqualTo(specialityid);
		criteria.andAvailableEqualTo(is);
		example.setOrderByClause("courseid asc");
		curriculums = curriculumMapper.selectByExample(example);
		
		return curriculums;
	}
}
