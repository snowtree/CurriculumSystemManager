package cn.edu.hrbcu.curriculum.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import cn.edu.hrbcu.curriculum.dao.CoursetypeMapper;
import cn.edu.hrbcu.curriculum.pojo.Coursetype;
import cn.edu.hrbcu.curriculum.pojo.CoursetypeExample;
import cn.edu.hrbcu.utils.Common;
@Service
public class CoursetypeServiceImpl  implements CoursetypeService{
	
	@Autowired
	private CoursetypeMapper coursetypeMapper;
	
	@Override
	public int insertCoursetype(Coursetype coursetype) {
		
		return coursetypeMapper.insert(coursetype);
	}

	@Override
	public List<Coursetype> selectAllCoursetype() {
		CoursetypeExample example = new CoursetypeExample();
		CoursetypeExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(-1L);
		return coursetypeMapper.selectByExample(example);
	}

	@Override
	public List<Coursetype> selectCoursetypes(int currentPage) {
		if(currentPage >= 0){
			PageHelper.startPage(currentPage, Common.PAGE_SIZE);
		}
		// TODO Auto-generated method stub
		CoursetypeExample example = new CoursetypeExample();
		CoursetypeExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(0L);
		List<Coursetype> list = coursetypeMapper.selectByExample(example);
		return list;
	}

	@Override
	public List<Coursetype> selectCoursetypesByPlatform(Long platformid) {
		// TODO Auto-generated method stub
		CoursetypeExample example = new CoursetypeExample();
		CoursetypeExample.Criteria criteria = example.createCriteria();
		criteria.andPlatformidEqualTo(platformid);
		List<Coursetype> list = coursetypeMapper.selectByExample(example);
		return list;
	}

}
