package cn.edu.hrbcu.curriculum.service;


import java.util.List;
import cn.edu.hrbcu.curriculum.pojo.Examinationmethod;

public interface ExaminationmethodService {
	int insertExaminationmethod(Examinationmethod method);

	
	List<Examinationmethod> selectAllExaminationmethod();
	
	List<Examinationmethod> selectExaminationmethods(int currentPage);
}
