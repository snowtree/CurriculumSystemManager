package cn.edu.hrbcu.curriculum.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.github.pagehelper.PageHelper;

import cn.edu.hrbcu.curriculum.dao.TeachingmethodMapper;
import cn.edu.hrbcu.curriculum.pojo.Teachingmethod;
import cn.edu.hrbcu.curriculum.pojo.TeachingmethodExample;

import cn.edu.hrbcu.utils.Common;

@Service
public class TeachingmethodServiceImpl implements TeachingmethodService{
	@Autowired
	private TeachingmethodMapper teachingmethodMapper;
	
	@Override
	public int insertTeachingmethod(Teachingmethod method) {
		
		return teachingmethodMapper.insert(method);
	}

	@Override
	public List<Teachingmethod> selectAllTeachingmethod() {
		TeachingmethodExample example = new TeachingmethodExample();
		TeachingmethodExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(-1L);
		return teachingmethodMapper.selectByExample(example);
	}

	@Override
	public List<Teachingmethod> selectTeachingmethods(int currentPage) {
		if(currentPage >= 0){
			PageHelper.startPage(currentPage, Common.PAGE_SIZE);
		}
		// TODO Auto-generated method stub
		TeachingmethodExample example = new TeachingmethodExample();
		TeachingmethodExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(0L);
		List<Teachingmethod> list = teachingmethodMapper.selectByExample(example);
		return list;
	}

}
