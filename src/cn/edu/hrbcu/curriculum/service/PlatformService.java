package cn.edu.hrbcu.curriculum.service;

import cn.edu.hrbcu.curriculum.pojo.Platform;
import java.util.List;

public interface PlatformService {
	int insertPlatform(Platform platform);
	
	List<Platform> selectAllPlatform();
	
	List<Platform> selectPlatforms(int currentPage);
}
