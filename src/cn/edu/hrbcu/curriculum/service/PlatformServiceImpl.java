package cn.edu.hrbcu.curriculum.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import cn.edu.hrbcu.curriculum.dao.PlatformMapper;
import cn.edu.hrbcu.curriculum.pojo.Platform;
import cn.edu.hrbcu.curriculum.pojo.PlatformExample;
import cn.edu.hrbcu.utils.Common;
@Service
public class PlatformServiceImpl  implements PlatformService{

	
	@Autowired
	private PlatformMapper platformMapper;
	
	public int insertPlatform(Platform platform) {
	
		return platformMapper.insert(platform);
	}

	public List<Platform> selectAllPlatform() {
		PlatformExample example = new PlatformExample();
		PlatformExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(-1L);
		return platformMapper.selectByExample(example);
	}

	@Override
	public List<Platform> selectPlatforms(int currentPage) {
		if(currentPage >= 0){
			PageHelper.startPage(currentPage, Common.PAGE_SIZE);
		}
		// TODO Auto-generated method stub
		PlatformExample example = new PlatformExample();
		PlatformExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(0L);
		List<Platform> list = platformMapper.selectByExample(example);
		return list;
	}

}
