package cn.edu.hrbcu.curriculum.service;

import java.util.List;
import cn.edu.hrbcu.curriculum.pojo.Teachingmethod;

public interface TeachingmethodService {
	int insertTeachingmethod(Teachingmethod method);

	
	List<Teachingmethod> selectAllTeachingmethod();
	
	List<Teachingmethod> selectTeachingmethods(int currentPage);
}
