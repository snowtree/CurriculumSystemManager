package cn.edu.hrbcu.curriculum.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;

import cn.edu.hrbcu.curriculum.dao.TbUserMapper;
import cn.edu.hrbcu.curriculum.pojo.TbUser;
import cn.edu.hrbcu.curriculum.pojo.TbUserExample;
import cn.edu.hrbcu.utils.Common;
import cn.edu.hrbcu.utils.UuidUtil;


@Service
public class TbUserServiceImpl implements TbUserService{
	
	@Autowired
	TbUserMapper bUserMapper;
	
	@Override
	public Boolean insertUser(TbUser user) {
		// TODO Auto-generated method stub
		return registerData(user);
	}

	@Override
	public List<TbUser> selectUserByLoginname(String loginname) {
		// TODO Auto-generated method stub
		TbUserExample example = new TbUserExample();
		TbUserExample.Criteria criteria = example.createCriteria();
		criteria.andLoginnameEqualTo(loginname);
		
		return bUserMapper.selectByExample(example);
	}

	@Override
	public List<TbUser> selectAllUser() {
		TbUserExample example = new TbUserExample();
		TbUserExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(-1L);
		
		return bUserMapper.selectByExample(example);
	}

	public List<TbUser> selectTbUsers(int currentPage) {
		if(currentPage >= 0){
			PageHelper.startPage(currentPage, Common.PAGE_SIZE);
		}
		// TODO Auto-generated method stub
		TbUserExample example = new TbUserExample();
		TbUserExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(0L);
		List<TbUser> list = bUserMapper.selectByExample(example);
		return list;
	}

	@Override
	public int updateTbUser(TbUser tbUser) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public TbUser selectTbUserById(TbUser tbUser) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deletTbUser(TbUser tbUser) {
		bUserMapper.deleteByPrimaryKey(tbUser.getId());
		
	}


	@Override
    public boolean registerData(TbUser user) {
        // 生成uuid
        String id = UuidUtil.get32UUID();

        // 将用户名作为盐值
        ByteSource salt = ByteSource.Util.bytes(user.getLoginname());
        /*
        * MD5加密：
        * 使用SimpleHash类对原始密码进行加密。
        * 第一个参数代表使用MD5方式加密
        * 第二个参数为原始密码
        * 第三个参数为盐值，即用户名
        * 第四个参数为加密次数
        * 最后用toHex()方法将加密后的密码转成String
        * */
        String newPs = new SimpleHash("MD5", user.getPassword(), salt, 1024).toHex();

        user.setPassword(newPs);

        // 看数据库中是否存在该账户
        List<TbUser> userInfo = selectUserByLoginname(user.getLoginname());
        if(userInfo == null || userInfo.size() == 0) {
        	bUserMapper.insert(user);
            return true;
        }
        return false;
    }

	@Override
	public List<TbUser> selectAllUserBySpecialityId(Long specialityid) {
		// TODO Auto-generated method stub
		TbUserExample example = new TbUserExample();
		TbUserExample.Criteria criteria = example.createCriteria();
		criteria.andSpecialityidEqualTo(specialityid);
		
		return bUserMapper.selectByExample(example);
	}
}
