package cn.edu.hrbcu.curriculum.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;

import cn.edu.hrbcu.curriculum.dao.ExaminationmethodMapper;
import cn.edu.hrbcu.curriculum.pojo.Examinationmethod;
import cn.edu.hrbcu.curriculum.pojo.ExaminationmethodExample;
import cn.edu.hrbcu.utils.Common;
@Service
public class ExaminationmethodServiceImpl implements ExaminationmethodService{

	
	@Autowired
	private ExaminationmethodMapper examinationmethodMapper;
	
	@Override
	public int insertExaminationmethod(Examinationmethod method) {
		return examinationmethodMapper.insert(method);
	}

	@Override
	public List<Examinationmethod> selectAllExaminationmethod() {
		ExaminationmethodExample example = new ExaminationmethodExample();
		ExaminationmethodExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(-1L);
		return examinationmethodMapper.selectByExample(example);
	}

	@Override
	public List<Examinationmethod> selectExaminationmethods(int currentPage) {
		if(currentPage >= 0){
			PageHelper.startPage(currentPage, Common.PAGE_SIZE);
		}
		// TODO Auto-generated method stub
		ExaminationmethodExample example = new ExaminationmethodExample();
		ExaminationmethodExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(0L);
		List<Examinationmethod> list = examinationmethodMapper.selectByExample(example);
		return list;
	}

}
