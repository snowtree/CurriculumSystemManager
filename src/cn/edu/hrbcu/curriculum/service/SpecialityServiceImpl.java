package cn.edu.hrbcu.curriculum.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;

import cn.edu.hrbcu.curriculum.dao.SpecialityMapper;
import cn.edu.hrbcu.curriculum.pojo.Speciality;
import cn.edu.hrbcu.curriculum.pojo.SpecialityExample;
import cn.edu.hrbcu.utils.Common;


@Service
public class SpecialityServiceImpl implements SpecialityService{
	@Autowired
	SpecialityMapper specialityMapper;
	
	@Override
	public int insertUser(Speciality user) {
		// TODO Auto-generated method stub
		return specialityMapper.insert(user);
	}

	@Override
	public List<Speciality> selectAllSpeciality() {
		SpecialityExample example = new SpecialityExample();
		SpecialityExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(-1L);
		
		return specialityMapper.selectByExample(example);
	}

	public List<Speciality> selectSpecialitys(int currentPage) {
		if(currentPage >= 0){
			PageHelper.startPage(currentPage, Common.PAGE_SIZE);
		}
		// TODO Auto-generated method stub
		SpecialityExample example = new SpecialityExample();
		SpecialityExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(0L);
		List<Speciality> list = specialityMapper.selectByExample(example);
		return list;
	}

	@Override
	public int updateSpeciality(Speciality speciality) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Speciality selectSpecialityById(Speciality speciality) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deletSpeciality(Speciality speciality) {
		specialityMapper.deleteByPrimaryKey(speciality.getId());
		
	}

	@Override
	public Speciality selectSpecialityByUserId(Long userid) {
		// TODO Auto-generated method stub
		SpecialityExample example = new SpecialityExample();
		SpecialityExample.Criteria criteria = example.createCriteria();
		criteria.andTbuseridEqualTo(userid);
		List<Speciality> list = specialityMapper.selectByExample(example);
		if(list != null && list.size() > 0)
			return list.get(0);
		
		return null;
	}

	@Override
	public List<Speciality> selectAllSpecialityByAcademyid(Long academyid) {
		SpecialityExample example = new SpecialityExample();
		SpecialityExample.Criteria criteria = example.createCriteria();
		criteria.andAcademyidEqualTo(academyid);
		
		return specialityMapper.selectByExample(example);
	}



}
