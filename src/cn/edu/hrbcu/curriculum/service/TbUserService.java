package cn.edu.hrbcu.curriculum.service;

import java.util.List;
import cn.edu.hrbcu.curriculum.pojo.TbUser;

public interface TbUserService {
	
	Boolean insertUser(TbUser user);

	List<TbUser> selectUserByLoginname(String username);

	int updateTbUser(TbUser tbUser);

	TbUser selectTbUserById(TbUser tbUser);

	void deletTbUser(TbUser tbUser);
	
	List<TbUser> selectTbUsers(int currentPage);

	List<TbUser> selectAllUser();
	
	List<TbUser> selectAllUserBySpecialityId(Long specialityid);
	
	boolean registerData(TbUser user);
	

}
