package cn.edu.hrbcu.curriculum.service;

import java.util.List;

import cn.edu.hrbcu.curriculum.pojo.TbPermission;

/**
 * @author 尤广富
 * 2019年4月8日
 * TODO
 */
public interface TbPermissionService {

	int insertPermission(TbPermission permission);

	List<TbPermission> selectPermissionByName(String name);

	int updateTbPermission(TbPermission tbPermission);

	TbPermission selectTbPermissionById(TbPermission tbPermission);

	void deletTbPermission(TbPermission tbPermission);
	
	List<TbPermission> selectTbPermissions(int currentPage);

	List<TbPermission> selectAllPermission();
}
