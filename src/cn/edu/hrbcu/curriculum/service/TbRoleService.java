package cn.edu.hrbcu.curriculum.service;

import java.util.List;

import cn.edu.hrbcu.curriculum.pojo.TbRole;

/**
 * @author 尤广富
 * 2019年4月8日
 * TODO
 */
public interface TbRoleService {

	int insertRole(TbRole role);

	List<TbRole> selectRoleByName(String name);

	int updateTbRole(TbRole tbRole);

	TbRole selectTbRoleById(TbRole tbRole);

	void deletTbRole(TbRole tbRole);
	
	List<TbRole> selectTbRoles(int currentPage);

	List<TbRole> selectAllRole();
}
