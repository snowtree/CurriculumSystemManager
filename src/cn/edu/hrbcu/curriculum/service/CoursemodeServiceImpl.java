package cn.edu.hrbcu.curriculum.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import cn.edu.hrbcu.curriculum.dao.CoursemodeMapper;
import cn.edu.hrbcu.curriculum.pojo.Coursemode;
import cn.edu.hrbcu.curriculum.pojo.CoursemodeExample;
import cn.edu.hrbcu.utils.Common;

@Service
public class CoursemodeServiceImpl implements CoursemodeService{

	
	@Autowired
	private CoursemodeMapper coursemodeMapper;
	
	@Override
	public int insertCoursemode(Coursemode coursemode) {
	
		return coursemodeMapper.insert(coursemode);
	}

	@Override
	public List<Coursemode> selectAllCoursemode() {
		CoursemodeExample example = new CoursemodeExample();
		CoursemodeExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(-1L);
		return coursemodeMapper.selectByExample(example);
	}

	@Override
	public List<Coursemode> selectCoursemodes(int currentPage) {
		if(currentPage >= 0){
			PageHelper.startPage(currentPage, Common.PAGE_SIZE);
		}
		// TODO Auto-generated method stub
		CoursemodeExample example = new CoursemodeExample();
		CoursemodeExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(0L);
		List<Coursemode> list = coursemodeMapper.selectByExample(example);
		return list;
	}

}
