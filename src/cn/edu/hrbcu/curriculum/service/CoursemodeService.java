package cn.edu.hrbcu.curriculum.service;

import cn.edu.hrbcu.curriculum.pojo.Coursemode;
import java.util.List;

public interface CoursemodeService {
	int insertCoursemode(Coursemode coursemode);

	
	List<Coursemode> selectAllCoursemode();
	
	List<Coursemode> selectCoursemodes(int currentPage);
}
