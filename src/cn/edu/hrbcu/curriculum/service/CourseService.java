package cn.edu.hrbcu.curriculum.service;

import cn.edu.hrbcu.curriculum.pojo.Course;
import cn.edu.hrbcu.curriculum.pojo.TbUser;
import java.util.List;

public interface CourseService {

	int insertCourse(Course course);
	
	List<Course> selectAllCourse();

	List<Course> selectCourses(int i,Course course);
	
	List<Course> selectCourses(int i);

	Course selectCourseById(Course course);
	
	Course selectCourseById(Long id);
	
	List<Course> selectCourseBy(Course course);
	
	List<Course> selectCourseBySpecialityId(Long specialityid, Long coursetypeid);
	
	List<Course> selectCourseBy(Course course,List<Long> ids);
	
	int updateCourse(Course course);

	void deleteCourse(Course course);
	
	Boolean makeCourseCode(int academyCode, Long specialityid, int specialityCode);

}
