package cn.edu.hrbcu.curriculum.service;

import cn.edu.hrbcu.curriculum.pojo.Academy;
import cn.edu.hrbcu.curriculum.pojo.Course;
import java.util.List;

public interface AcademyService {
	int insertAcademy(Academy academy);
	
	List<Academy> selectAllAcademy();
	
	List<Academy> selectAcademys(int currentPage);
}
