package cn.edu.hrbcu.curriculum.service;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import java.util.List;
import cn.edu.hrbcu.curriculum.dao.CourseMapper;
import cn.edu.hrbcu.curriculum.dao.CoursetypeMapper;
import cn.edu.hrbcu.curriculum.pojo.Course;
import cn.edu.hrbcu.curriculum.pojo.CourseExample;
import cn.edu.hrbcu.curriculum.pojo.Coursetype;
import cn.edu.hrbcu.utils.Common;

@Service
public class CourseServiceImpl implements  CourseService {
	@Autowired
	private CourseMapper courseMapper;
	@Autowired
	private CoursetypeService coursetypeService;
	
	public int insertCourse(Course course) {
		
		return courseMapper.insert(course);
	}

	
	public List<Course> selectAllCourse() {
		CourseExample example = new CourseExample();
		CourseExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(-1L);
		
		return courseMapper.selectByExample(example);
	}


	public List<Course> selectCourses(int currentPage,Course course) {
		if(currentPage >= 0){
			PageHelper.startPage(currentPage, Common.PAGE_SIZE);
		}
		// TODO Auto-generated method stub
		CourseExample example = new CourseExample();
		CourseExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(0L);
		if(course != null) {
			if(course.getName() != null && StringUtils.isNotEmpty(course.getName())) {
				criteria.andNameEqualTo(course.getName());
			}
			if(course.getAcademyid() != null) {
				criteria.andAcademyidEqualTo(course.getAcademyid());
			}
			if(course.getSpecialityid() != null) {
				criteria.andSpecialityidEqualTo(course.getSpecialityid());
			}
			if(course.getCoursetypeid() != null) {
				criteria.andCoursetypeidEqualTo(course.getCoursetypeid());
			}
			if(course.getScore() != null) {
				criteria.andScoreEqualTo(course.getScore() );
			}
		}
		List<Course> list = courseMapper.selectByExample(example);
		return list;
	}
	
	public List<Course> selectCourses(int currentPage) {
		if(currentPage >= 0){
			PageHelper.startPage(currentPage, Common.PAGE_SIZE);
		}
		// TODO Auto-generated method stub
		CourseExample example = new CourseExample();
		CourseExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(0L);
		List<Course> list = courseMapper.selectByExample(example);
		return list;
	}



	@Override
	public void deleteCourse(Course course) {
	
		courseMapper.deleteByPrimaryKey(course.getId());
	}


	@Override
	public Course selectCourseById(Course course) {
		
		return courseMapper.selectByPrimaryKey(course.getId());
	}


	@Override
	public Course selectCourseById(Long id) {
		return courseMapper.selectByPrimaryKey(id);
	}

	
	@Override
	public int updateCourse(Course course) {
		return courseMapper.updateByPrimaryKeySelective(course);
	}


	@Override
	public List<Course> selectCourseBy(Course course) {
		// TODO Auto-generated method stub
		CourseExample example = new CourseExample();
		CourseExample.Criteria criteria = example.createCriteria();
		
		if(course.getName() != null && !course.getName().trim().equals("")){
			criteria.andNameLike("%" + course.getName() + "%");
		}
		
		if(course.getCoursetypeid() != null){
			criteria.andCoursetypeidEqualTo(course.getCoursetypeid());
		}
		
		if(course.getCoursemodeid() != null){
			criteria.andCoursemodeidEqualTo(course.getCoursemodeid());
		}
		
		List<Course> list = courseMapper.selectByExample(example);
		return list;
	}


	@Override
	public Boolean makeCourseCode(int academyCode, Long specialityid, int specialityCode) {
		// TODO Auto-generated method stub
		List<Coursetype> coursetypes = coursetypeService.selectAllCoursetype();
		
		if(coursetypes != null && coursetypes.size() > 0){
			for (Coursetype coursetype : coursetypes) {
				List<Course> courses = selectCourseBySpecialityId(specialityid,coursetype.getId());
				if(courses != null && courses.size() > 0){
					for(int index = 0;index < courses.size() ; index++ ){
						courses.get(index).setIdentifier(
								coursetype.getCode()
								+ String.format("%02d", academyCode) 
								+ String.format("%01d",specialityCode) 
								+ String.format("%02d",index + 1)
							);
						
						updateCourse(courses.get(index));
					}
				}
			}
			return true;
		}
		return false;
	}


	@Override
	public List<Course> selectCourseBy(Course course, List<Long> ids) {
		// TODO Auto-generated method stub
		CourseExample example = new CourseExample();
		CourseExample.Criteria criteria = example.createCriteria();
		
		if(course.getName() != null && !course.getName().trim().equals("")){
			criteria.andNameLike("%" + course.getName() + "%");
		}
		
		if(course.getCoursetypeid() != null){
			criteria.andCoursetypeidEqualTo(course.getCoursetypeid());
		}
		
		if(course.getCoursemodeid() != null){
			criteria.andCoursemodeidEqualTo(course.getCoursemodeid());
		}
		
		if(ids != null && ids.size() >0){
			criteria.andIdIn(ids);
		}
		
		List<Course> list = courseMapper.selectByExample(example);
		
		return list;
	}


	@Override
	public List<Course> selectCourseBySpecialityId(Long specialityid, Long coursetypeid) {
		// TODO Auto-generated method stub
		CourseExample example = new CourseExample();
		CourseExample.Criteria criteria = example.createCriteria();
		criteria.andSpecialityidEqualTo(specialityid);
		criteria.andCoursetypeidEqualTo(coursetypeid);
		
		return courseMapper.selectByExample(example);
	}
	
}
