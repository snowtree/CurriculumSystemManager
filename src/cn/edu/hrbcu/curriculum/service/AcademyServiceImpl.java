package cn.edu.hrbcu.curriculum.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import cn.edu.hrbcu.curriculum.dao.AcademyMapper;
import cn.edu.hrbcu.curriculum.pojo.Academy;
import cn.edu.hrbcu.curriculum.pojo.AcademyExample;
import cn.edu.hrbcu.utils.Common;
@Service
public class AcademyServiceImpl  implements AcademyService{

	
	@Autowired
	private AcademyMapper academyMapper;
	
	public int insertAcademy(Academy academy) {
	
		return academyMapper.insert(academy);
	}

	public List<Academy> selectAllAcademy() {
		AcademyExample example = new AcademyExample();
		AcademyExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(-1L);
		return academyMapper.selectByExample(example);
	}

	@Override
	public List<Academy> selectAcademys(int currentPage) {
		if(currentPage >= 0){
			PageHelper.startPage(currentPage, Common.PAGE_SIZE);
		}
		// TODO Auto-generated method stub
		AcademyExample example = new AcademyExample();
		AcademyExample.Criteria criteria = example.createCriteria();
		criteria.andIdGreaterThan(0L);
		List<Academy> list = academyMapper.selectByExample(example);
		return list;
	}

}
