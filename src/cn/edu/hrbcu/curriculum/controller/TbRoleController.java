package cn.edu.hrbcu.curriculum.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;
import cn.edu.hrbcu.curriculum.pojo.TbRole;
import cn.edu.hrbcu.curriculum.service.TbRoleService;
import cn.edu.hrbcu.curriculum.service.TbUserRoleService;

@Controller
@RequestMapping("/TbRoleController")
public class TbRoleController {
	@Autowired
	TbRoleService tbRoleService;
	@Autowired
	TbUserRoleService tbUserRoleService;
	
	@RequestMapping("/QueryTbRole.action")
	public ModelAndView QueryTbRole(@RequestParam(value = "currentPage",required=false, defaultValue="1") Integer currentPage){
		List<TbRole> list = tbRoleService.selectTbRoles(currentPage);
		
		PageInfo<TbRole> page = new PageInfo<TbRole>(list);
		
		ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/QueryTbRole");
        modelAndView.addObject("pageInfo", page);
        modelAndView.addObject("page_url", "/CurriculumSystemManager/TbRoleController/QueryCourse.action");
        
		return modelAndView;
	}
	
	@RequestMapping("/QueryAllTbRole.action")
	@ResponseBody
	public Object QueryAllTbRole(){
		List<TbRole> roles = tbRoleService.selectAllRole();		
		return roles;
	}
	
	
	@RequestMapping("/QueryTbRoleById.action")
	@ResponseBody
	public Object QueryTbRoleById(@RequestParam(value = "id",required=true, defaultValue="1") Long id){
		TbRole tbRole_ = new TbRole();
		tbRole_.setId(id);
		
		TbRole role = tbRoleService.selectTbRoleById(tbRole_);		
		return role;
	}
	
	@RequestMapping("/AddTbRole.action")
	public Object AddTbRole(TbRole tbRole){
		
		tbRoleService.insertRole(tbRole);
		
		return "forward:/TbRoleController/QueryTbRole.action";
	}
	
	@RequestMapping("/DeleteTbRole.action")
	public Object DeleteCourse(TbRole tbRole){
		
		tbRoleService.deletTbRole(tbRole);
		
		return "forward:/TbRoleController/QueryTbRole.action";
	}
	
	@RequestMapping("/UpdateTbRole.action")
	public Object UpdateTbRole(TbRole tbRole){
		int cnt = tbRoleService.updateTbRole(tbRole);
		
		return "forward:/TbRoleController/QueryTbRole.action";
	}
	
	@RequestMapping("/toUpdateTbRole.action")
	public Object toUpdateTbRole(TbRole tbRole){
	    tbRole = tbRoleService.selectTbRoleById(tbRole);
		ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/UpdateTbRole");
        modelAndView.addObject("tbRole", tbRole);
		return modelAndView;
	}
	
	@RequestMapping("/toAddTbRole.action")
	public Object toAddTbRole(){
		ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/AddTbRole");
		return modelAndView;
	}
	
	@RequestMapping("/QueryRolesUser.action")
	@ResponseBody
	public Object QueryRolesUser(Long userid,Boolean isAssgned){
		List<TbRole> roles = null;
		roles = tbUserRoleService.selectRoleByUser(userid, isAssgned);
		return roles;
	}
}
