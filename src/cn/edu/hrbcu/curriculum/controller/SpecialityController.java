package cn.edu.hrbcu.curriculum.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import cn.edu.hrbcu.curriculum.pojo.Academy;
import cn.edu.hrbcu.curriculum.pojo.Course;
import cn.edu.hrbcu.curriculum.pojo.Coursemode;
import cn.edu.hrbcu.curriculum.pojo.Coursetype;
import cn.edu.hrbcu.curriculum.pojo.Examinationmethod;
import cn.edu.hrbcu.curriculum.pojo.Speciality;
import cn.edu.hrbcu.curriculum.pojo.Teachingmethod;
import cn.edu.hrbcu.curriculum.service.AcademyService;
import cn.edu.hrbcu.curriculum.service.CourseService;
import cn.edu.hrbcu.curriculum.service.CoursemodeService;
import cn.edu.hrbcu.curriculum.service.CoursetypeService;
import cn.edu.hrbcu.curriculum.service.ExaminationmethodService;
import cn.edu.hrbcu.curriculum.service.SpecialityService;
import cn.edu.hrbcu.curriculum.service.TeachingmethodService;


@Controller
@RequestMapping("/SpecialityController")
public class SpecialityController {
	@Autowired
	SpecialityService specialityService;
	
	@RequestMapping("/QuerySpeciality.action")
	public ModelAndView QuerySpeciality(@RequestParam(value = "currentPage",required=false, defaultValue="1") Integer currentPage){
		List<Speciality> list = specialityService.selectSpecialitys(currentPage);
		
		PageInfo<Speciality> page = new PageInfo<Speciality>(list);
		
		ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/QuerySpeciality");
        modelAndView.addObject("pageInfo", page);
        
		return modelAndView;
	}
	
	@RequestMapping("/QueryAllSpeciality.action")
	@ResponseBody
	public Object QueryAllSpeciality(){
		List<Speciality> list = specialityService.selectAllSpeciality();
		return list;
	}
	
	@RequestMapping("/QueryAllSpecialityByAcademyid.action")
	@ResponseBody
	public Object QueryAllSpecialityByAcademyid(@RequestParam("academyid")Long academyid){
		List<Speciality> list = specialityService.selectAllSpecialityByAcademyid(academyid);
		return list;
	}
	
}
