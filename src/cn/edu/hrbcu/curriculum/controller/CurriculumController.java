package cn.edu.hrbcu.curriculum.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import cn.edu.hrbcu.curriculum.pojo.Course;
import cn.edu.hrbcu.curriculum.pojo.Coursemode;
import cn.edu.hrbcu.curriculum.pojo.Coursetype;
import cn.edu.hrbcu.curriculum.pojo.Curriculum;
import cn.edu.hrbcu.curriculum.pojo.Examinationmethod;
import cn.edu.hrbcu.curriculum.pojo.Platform;
import cn.edu.hrbcu.curriculum.pojo.Speciality;
import cn.edu.hrbcu.curriculum.pojo.TbUser;
import cn.edu.hrbcu.curriculum.service.CourseService;
import cn.edu.hrbcu.curriculum.service.CoursemodeService;
import cn.edu.hrbcu.curriculum.service.CoursetypeService;
import cn.edu.hrbcu.curriculum.service.CurriculumService;
import cn.edu.hrbcu.curriculum.service.ExaminationmethodService;
import cn.edu.hrbcu.curriculum.service.PlatformService;
import cn.edu.hrbcu.curriculum.service.SpecialityService;
import cn.edu.hrbcu.curriculum.service.TbUserService;
import cn.edu.hrbcu.utils.CharacterUtil;
import cn.edu.hrbcu.utils.WordUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/CurriculumController")
public class CurriculumController {
	@Autowired
	CurriculumService curriculumService;
	@Autowired
	CourseService courseService;
	@Autowired
	CoursemodeService coursemodeService;
	@Autowired
	ExaminationmethodService examinationmethodService;
	@Autowired
	private SpecialityService specialityService;
	@Autowired
	private TbUserService tbUserService;
	@Autowired
	CoursetypeService coursetypeService;
	@Autowired
	PlatformService platformService;

	@RequestMapping("/QueryCurriculum.action")
	public ModelAndView QueryCurriculum(
			@RequestParam(value = "currentPage", required = false, defaultValue = "1") Integer currentPage) {
		List<Curriculum> list = curriculumService.selectCurriculums(currentPage);
		List<Course> courses = courseService.selectAllCourse();
		List<Examinationmethod> examinationmethods = examinationmethodService.selectAllExaminationmethod();
		PageInfo<Curriculum> page = new PageInfo<Curriculum>(list);

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/QueryCurriculum");
		modelAndView.addObject("pageInfo", page);
		modelAndView.addObject("courses", courses);
		modelAndView.addObject("examinationmethods", examinationmethods);
		modelAndView.addObject("page_url", "/CurriculumSystemManager/CurriculumController/QueryCurriculum.action");

		return modelAndView;
	}

	@RequestMapping("/QueryCurriculums.action")
	public ModelAndView QueryCurriculums() {
		Subject subject = SecurityUtils.getSubject();
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/QueryCurriculums");
		List<Course> courses = courseService.selectAllCourse();
		modelAndView.addObject("courses", courses);

		List<TbUser> tbUsers = tbUserService.selectUserByLoginname((String) subject.getPrincipal());
		if (tbUsers != null && tbUsers.size() > 0) {
			Speciality speciality = specialityService.selectSpecialityByUserId(tbUsers.get(0).getId());
			if (speciality != null) {
				for (int i = 1; i <= 8; i++) {
					List<Curriculum> curriculums_ = curriculumService
							.selectAllCurriculumBySpecialityandSemester(speciality.getId(), i);
					List<Map<String, Object>> curriculums = new ArrayList<Map<String, Object>>();

					if (curriculums_ != null && curriculums_.size() > 0) {
						for (Curriculum curriculum : curriculums_) {
							Map<String, Object> c = new HashMap<String, Object>();
							Course course = courseService.selectCourseById(curriculum.getCourseid());
							c.put("id", course.getId());
							c.put("name", course.getName());
							c.put("identifier", course.getIdentifier());
							c.put("score", course.getScore());
							c.put("time", course.getTheoretical() + course.getExperiement());
							c.put("theoretical", course.getTheoretical());
							c.put("experiement", course.getExperiement());
							c.put("unit", course.getUnit());
							c.put("hpw", curriculum.getHpw());
							c.put("start", curriculum.getStart());
							c.put("end", curriculum.getEnd());
							c.put("avaliable", curriculum.getAvailable());
							c.put("disperse", curriculum.getDisperse());

							curriculums.add(c);
						}
					}
					modelAndView.addObject("curriculums" + i, curriculums);
				}
			}
		}

		return modelAndView;
	}

	@RequestMapping("/CheckCurriculums.action")
	public ModelAndView CheckCurriculums(Long specialityid) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/CheckCurriculums");
		// 获取专业id
		if (specialityid == null) {
			Subject subject = SecurityUtils.getSubject();
			List<TbUser> tbUsers = tbUserService.selectUserByLoginname((String) subject.getPrincipal());
			if (tbUsers != null && tbUsers.size() > 0) {
				Speciality speciality = specialityService.selectSpecialityByUserId(tbUsers.get(0).getId());
				if (speciality == null) {
					return null;
				} else {
					specialityid = speciality.getId();
				}
			} else {
				return null;
			}
		}
		/////////////////////////////////////////////				
		if (specialityid == null) {
			return modelAndView;
		}

		List<Course> courses = courseService.selectAllCourse();
		modelAndView.addObject("courses", courses);

		for (int i = 1; i <= 8; i++) {
			List<Curriculum> curriculums_ = curriculumService.selectAllCurriculumBySpecialityandSemester(specialityid,
					i);
			List<Map<String, Object>> curriculums = new ArrayList<Map<String, Object>>();

			if (curriculums_ != null && curriculums_.size() > 0) {
				for (Curriculum curriculum : curriculums_) {
					Map<String, Object> c = new HashMap<String, Object>();
					Course course = courseService.selectCourseById(curriculum.getCourseid());
					c.put("id", course.getId());
					c.put("name", course.getName());
					c.put("identifier", course.getIdentifier());
					c.put("score", course.getScore());
					c.put("time", course.getTheoretical() + course.getExperiement());
					c.put("theoretical", course.getTheoretical());
					c.put("experiement", course.getExperiement());
					c.put("unit", course.getUnit());
					c.put("hpw", curriculum.getHpw());
					c.put("start", curriculum.getStart());
					c.put("end", curriculum.getEnd());
					c.put("avaliable", curriculum.getAvailable());
					c.put("disperse", curriculum.getDisperse());
					
					curriculums.add(c);
				}
			}
			modelAndView.addObject("curriculums" + i, curriculums);
		}

		return modelAndView;
	}
	//
	
	@RequestMapping("/QuerySchedule.action")
	public ModelAndView QuerySchedule(Long specialityid) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/QuerySchedule");
		//获取专业id
		if(specialityid == null){
			Subject subject = SecurityUtils.getSubject();
			List<TbUser> tbUsers = tbUserService.selectUserByLoginname((String) subject.getPrincipal());
			if (tbUsers != null && tbUsers.size() > 0) {
				Speciality speciality = specialityService.selectSpecialityByUserId(tbUsers.get(0).getId());
				if (speciality == null) {
					return modelAndView;
				}else{
					specialityid = speciality.getId();
				}
			} else {
				return modelAndView;
			}
		}
		/////////////////////////////////////////////
		if (specialityid == null) {
			return modelAndView;
		}
		////////////////////////////////////////////
		List<Coursetype> coursetypes = coursetypeService.selectAllCoursetype();
		modelAndView.addObject("coursetypes", coursetypes);
		
		List<Map<String, Object>> curriculums = queryCurriculumsByCoursetype(specialityid);
		modelAndView.addObject("allcurriculums", curriculums);

		return modelAndView;
	}
	

	@RequestMapping(value = "/UpdateCurriculum.action")
	@ResponseBody
	public Object UpdateCurriculum(@RequestParam(value = "semester") Integer semester,
			@RequestParam(value = "curriculums") String curriculums) {
		Subject subject = SecurityUtils.getSubject();
		JSONArray jsonArray = JSONArray.fromObject(curriculums);
		Speciality speciality = null;

		List<TbUser> tbUsers = tbUserService.selectUserByLoginname((String) subject.getPrincipal());
		if (tbUsers != null && tbUsers.size() > 0) {
			speciality = specialityService.selectSpecialityByUserId(tbUsers.get(0).getId());
			if (speciality == null) {
				return "failure";
			}
		} else {
			return "failure";
		}

		List<Curriculum> list_curriculum = new ArrayList<Curriculum>();
		for (int i = 0; i < jsonArray.size(); i++) {
			Object o = jsonArray.get(i);
			JSONObject jsonObject2 = JSONObject.fromObject(o);
			Curriculum curriculum = (Curriculum) JSONObject.toBean(jsonObject2, Curriculum.class);
			curriculum.setId(null);
			curriculum.setSort(i);
			curriculum.setSpecialityid(speciality.getId());
			list_curriculum.add(curriculum);
		}

		if (list_curriculum.size() > 0) {
			int cnt = curriculumService.updateCurriculum(speciality.getId(), semester, list_curriculum);
		}
		return "successful";
	}
	
	private Map<String, Object> queryCurriculumsBySemester(Long specialityId, boolean available) {
		Map<String,Object> all_curriculums = new HashMap<String,Object>();
		
		for (int i = 1; i <= 8; i++) {
			//保存每个学期所有的课程和小计信息
			Map<String,Object> semester = new HashMap<String,Object>();
			//保存每个学期所有的课程
			List<Map<String, Object>> curriculums = new ArrayList<Map<String, Object>>();
			//保存小计信息
			Map<String,Object> total = new HashMap<String,Object>();
			double total_score = 0D;//总学分
			double[] total_hour = new double[10];//所有时小计
			double[] total_week = new double[10];//所有周小计
			double[] disperse_hour = new double[10];//开课，分散时小计
			double[] disperse_week = new double[10];//开课，分散周小计
			
			List<Curriculum> curriculums_ = curriculumService.selectAllCurriculumBySpecialityandSemester(specialityId, i);
			if (curriculums_ != null && curriculums_.size() > 0) {
				for (Curriculum curriculum : curriculums_) {
					if(!curriculum.getAvailable()){//课程没有开课，跳过
						continue;
					}
					
					Map<String, Object> c = new HashMap<String, Object>();
					Course course = courseService.selectCourseById(curriculum.getCourseid());
					Boolean isDisperse = curriculum.getDisperse();
					int unit = course.getUnit();
					c.put("id", course.getId());
					c.put("name", course.getName());
					c.put("identifier", course.getIdentifier());
					
					c.put("score", course.getScore());//总学分
					total_score += (course.getScore()==null) ? 0 : course.getScore();
					
					c.put("time", course.getTheoretical() + course.getExperiement()); 
					if(!isDisperse){
						if(course.getUnit() == null || course.getUnit() == 0){//计算学时
							total_hour[1] += (course.getTheoretical() == null || course.getExperiement() == null) ? 0 : course.getTheoretical() + course.getExperiement() ;
						}else{
							total_week[1] += (course.getTheoretical() == null || course.getExperiement() == null) ? 0 : course.getTheoretical() + course.getExperiement() ;
						}
					}else{
						if(course.getUnit() == null || course.getUnit() == 0){//计算学时
							disperse_hour[1] += (course.getTheoretical() == null || course.getExperiement() == null) ? 0 : course.getTheoretical() + course.getExperiement() ;
						}else{
							disperse_week[1] += (course.getTheoretical() == null || course.getExperiement() == null) ? 0 : course.getTheoretical() + course.getExperiement() ;
						}
					}
					
					c.put("theoretical", course.getTheoretical());
					if(!isDisperse){
						if(course.getUnit() == null || course.getUnit() == 0){//计算学时
							total_hour[2] += (course.getTheoretical()==null) ? 0 : course.getTheoretical();
						}else{
							total_week[2] += (course.getTheoretical()==null) ? 0 : course.getTheoretical();
						}
					}else{
						if(course.getUnit() == null || course.getUnit() == 0){//计算学时
							disperse_hour[2] += (course.getTheoretical()==null) ? 0 : course.getTheoretical();
						}else{
							disperse_week[2] += (course.getTheoretical()==null) ? 0 : course.getTheoretical();
						}
					}
						
					c.put("experiement", course.getExperiement());
					if(!isDisperse){
						if(course.getUnit() == null || course.getUnit() == 0){//计算学时
							total_hour[3] += (course.getExperiement()==null) ? 0 : course.getExperiement();
						}else{
							total_week[3] += (course.getExperiement()==null) ? 0 : course.getExperiement();
						}
					}else{
						if(course.getUnit() == null || course.getUnit() == 0){//计算学时
							disperse_hour[3] += (course.getExperiement()==null) ? 0 : course.getExperiement();
						}else{
							disperse_week[3] += (course.getExperiement()==null) ? 0 : course.getExperiement();
						}
					}
					c.put("unit", course.getUnit());
					c.put("hpw", (curriculum.getHpw()==null) ? "" : curriculum.getHpw());
					if(!isDisperse){//不分散
						if(unit == 0){//学时
							total_hour[4] += (curriculum.getHpw()==null) ? 0 : curriculum.getHpw();
						}else{//学周
							total_week[4] += (curriculum.getHpw()==null) ? 0 : curriculum.getHpw();
							c.put("hpw", (course.getTheoretical() == null || course.getExperiement() == null) ? 0 : course.getTheoretical() + course.getExperiement());
						}
					}else{
						if(unit == 0){//学时
							disperse_hour[4] += (curriculum.getHpw()==null) ? 0 : curriculum.getHpw();
						}else{//学周
							disperse_week[4] += (curriculum.getHpw()==null) ? 0 : curriculum.getHpw();
							c.put("hpw", (course.getTheoretical() == null || course.getExperiement() == null) ? 0 : course.getTheoretical() + course.getExperiement());
						}
					}
					c.put("start", curriculum.getStart());
					c.put("end", curriculum.getEnd());
					c.put("available", curriculum.getAvailable());
					c.put("disperse", curriculum.getDisperse());
					
					if(available){//若果只查已开课的
						if(curriculum.getAvailable()){
							curriculums.add(c);
						}
					}else{//所有课程全部查询
						curriculums.add(c);
					}
				}
			}
			//保存学期的小计信息
			total.put("score", total_score);//学分不分是否分散，全部统计
			
			total.put("time", double2String(total_hour[1]));
			if(disperse_hour[1] > 0D){
				total.put("time", double2String(total_hour[1]) + "(" + double2String(disperse_hour[1]) + ")");//
			}
			
			total.put("theoretical", double2String(total_hour[2]));
			if(disperse_hour[2] > 0D){
				total.put("theoretical", double2String(total_hour[2]) + "(" + double2String(disperse_hour[2]) + ")");
			}
			
			total.put("experiement", double2String(total_hour[3]));
			if(disperse_hour[3] > 0D){
				total.put("experiement", double2String(total_hour[3]) + "(" + double2String(disperse_hour[3]) + ")");
			}
			
			if(total_week[4] > 0D || disperse_week[4] > 0D){
				total.put("hpw", double2String(total_hour[4]) + "/");
				if(total_week[4] > 0D){
					total.put("hpw", (String)total.get("hpw") + double2String(total_week[4]));
				}
				
				if(disperse_week[4] > 0D){
					total.put("hpw", (String)total.get("hpw") + "(" +  double2String(total_week[4]) + ")");
				}
				
				total.put("hpw", (String)total.get("hpw") + "周" );
			}else{
				total.put("hpw",double2String(total_hour[4]));
			}
			
			semester.put("course", curriculums);
			semester.put("total", total);
			
			all_curriculums.put("semester" + i, semester);
		}

		return all_curriculums;
	}
	
	private List<Map<String,Object>> queryCurriculumsByCoursetype(Long specialityId) {
		//Map<String,Object> all_curriculums = new HashMap<String,Object>();
		List<Map<String,Object>> all_curriculums = new ArrayList<Map<String,Object>>();
		
		List<Coursetype> coursetypes = coursetypeService.selectAllCoursetype();
		if(coursetypes != null && coursetypes.size() > 0){
			for (Coursetype coursetype : coursetypes) {
				//保存每个课程类型所有的课程和小计信息
				Map<String,Object> type_ = new HashMap<String,Object>();
				//保存每个课程类型所有的课程
				List<Map<String, Object>> curriculums = new ArrayList<Map<String, Object>>();
				//保存小计信息
				Map<String,Object> total = new HashMap<String,Object>();
				double[] t = new double[10];
				
				List<Curriculum> curriculums_ = curriculumService.selectAllCurriculumBySpecialityandCourseType(specialityId, coursetype.getId());
				if (curriculums_ != null && curriculums_.size() > 0) {
					for (Curriculum curriculum : curriculums_) {
						Map<String, Object> c = new HashMap<String, Object>();
						Course course = courseService.selectCourseById(curriculum.getCourseid());
						c.put("id", course.getId());
						c.put("name", course.getName());
						c.put("identifier", course.getIdentifier());
						c.put("score", course.getScore());	
							t[0] += (course.getScore()==null) ? 0 : course.getScore();
						c.put("time", course.getTheoretical() + course.getExperiement()); 
							t[1] += (course.getTheoretical() == null || course.getExperiement() == null) ? 0 : course.getTheoretical() + course.getExperiement() ;
						c.put("theoretical", course.getTheoretical());
							t[2] += (course.getTheoretical()==null) ? 0 : course.getTheoretical();
						c.put("experiement", course.getExperiement());
							t[3] += (course.getExperiement()==null) ? 0 : course.getExperiement();
						c.put("unit", course.getUnit());
						c.put("hpw", curriculum.getHpw());
							t[4] += (curriculum.getHpw()==null) ? 0 : curriculum.getHpw();
						c.put("start", curriculum.getStart());
						c.put("end", curriculum.getEnd());
						c.put("available", curriculum.getAvailable());
						c.put("disperse", curriculum.getDisperse());
						c.put("semester", CharacterUtil.num2roman(curriculum.getSemester()));
	
						curriculums.add(c);
					}
				}
				//保存学期的小计信息
				total.put("score", t[0]);
				total.put("time", t[1]);
				total.put("theoretical", t[2]);
				total.put("experiement", t[3]);
				total.put("hpw", t[4]);
				
				
				type_.put("curriculums", curriculums);
				type_.put("total", total);
				type_.put("coursetypename", coursetype.getName());
				
				all_curriculums.add(type_);
			}
		}

		return all_curriculums;
	}
	
	/*
	 * 功能描述 ： 人才培养方案实施进程表
	 * 	
	 */
	@RequestMapping("/ExportCurriculum")
    public @ResponseBody void ExportCurriculum(HttpServletRequest req, HttpServletResponse resp){
		// 获取专业id
		Speciality speciality = null;
		Subject subject = SecurityUtils.getSubject();
		List<TbUser> tbUsers = tbUserService.selectUserByLoginname((String) subject.getPrincipal());
		if (tbUsers != null && tbUsers.size() > 0) {
			speciality = specialityService.selectSpecialityByUserId(tbUsers.get(0).getId());
			
			Map<String,Object> dataMap = queryCurriculumsBySemester(speciality.getId(),true);
	        
	        try {
	            //WordUtils.exportMillCertificateWord(req, resp, dataMap, "本科专业人才培养方案实施进程表","schedule_12.ftl");
	            WordUtils.exportMillCertificateWord(req, resp, dataMap, "本科专业人才培养方案实施进程表","schedule.ftl");
	            //WordUtils.exportTestWord(req, resp, data, "本科专业人才培养方案实施进程表","test1.ftl");
	        	
	        } catch (IOException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }   
	        
		} 
    }
	
	/*
	 * 功能描述 ： 人才培养方案计划表
	 * 	
	 */
	@RequestMapping("/ExportSchedule")
    public @ResponseBody void ExportSchedule(HttpServletRequest req, HttpServletResponse resp){
		// 获取专业id
		Speciality speciality = null;
		Subject subject = SecurityUtils.getSubject();
		List<TbUser> tbUsers = tbUserService.selectUserByLoginname((String) subject.getPrincipal());
		if (tbUsers != null && tbUsers.size() > 0) {
			speciality = specialityService.selectSpecialityByUserId(tbUsers.get(0).getId());
			
			Map<String, Object> dataMap = querySchedule(speciality.getId(),true);
	        
	       try {
	            WordUtils.exportMillCertificateWord(req, resp, dataMap, "本科专业人才培养方案计划表","sch1.ftl");
	        	
	        } catch (IOException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        } 
	        
		} 
    }
	
	private Map<String, Object> querySchedule(Long specialityId, Boolean is){
		double coursemode_theoretical_hour = 0;//课程模式理论学时
		double coursemode_theoretical_disperse_hour = 0;//课程模式理论分散学时
		double coursemode_experiement_hour = 0;//课程模式实验学时
		double coursemode_experiement_disperse_hour = 0;//课程模式实验分散学时
		double coursemode_practice_week = 0;//课程模式实验学时
		double coursemode_practice_disperse_week = 0;//课程模式实验分散学时
		double coursemode_score = 0;//课程模式学分
		List<Map<String, Object>> platforms = new ArrayList<Map<String,Object>>();
		List<Curriculum> speciality_curriculums = curriculumService.selectAllCurriculumBySpeciality(specialityId, is);
		List<Long> courseids = new ArrayList<Long>();
		for (Curriculum curriculum : speciality_curriculums) {
			courseids.add(curriculum.getCourseid());
		}
		
		//(1) 查询所有的教育平台
		List<Platform> plfs = platformService.selectAllPlatform();
		for (Platform plf : plfs) {			
		//(2) 查询该教育平台下所有课程类型
			List<Coursetype> cts = coursetypeService.selectCoursetypesByPlatform(plf.getId());
			
			Map<String, Object> platform = new HashMap<String,Object>();
			List<Map<String,Object> > coursetypes = new ArrayList<Map<String,Object>>();
			if(cts != null && cts.size() > 0){
				for (Coursetype ct : cts) {
					//(3) 查询所有的课程模式
					List<Coursemode> cms = coursemodeService.selectAllCoursemode();
					
					Map<String,Object> coursetype = new HashMap<String,Object>();
					List<Map<String,Object>> coursemodes = new ArrayList<Map<String,Object>>();
					if(cms != null && cms.size() > 0){
						for (Coursemode cm : cms) {
							coursemode_theoretical_hour = 0D;
							coursemode_theoretical_disperse_hour = 0D;
							coursemode_experiement_hour = 0D;
							coursemode_experiement_disperse_hour = 0D;
							coursemode_practice_week = 0D;
							coursemode_practice_disperse_week = 0D;
							coursemode_score = 0D;
							
							//(4) 查询该模式下所有的课程
							// selectCourseBy
							Course exampleCourse = new Course();
							exampleCourse.setCoursetypeid(ct.getId());//课程类型
							exampleCourse.setCoursemodeid(cm.getId());//课程模式
							
							List<Course> courses = courseService.selectCourseBy(exampleCourse,courseids);
							if(courses != null && courses.size()>0){
								//////////////////////////////////////////////////////////////////
								List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
								for (Course course : courses) {
									Map<String,Object> obj = new HashMap<String,Object>();
									List<Curriculum> curs = curriculumService.selectAllCurriculumBySpecialityandCourse(specialityId, course.getId());
									if(curs != null && curs.size() > 0){
										String rets = "";
										for(int k = 0; k < curs.size() ; k++){
											rets += CharacterUtil.num2roman(curs.get(0).getSemester());
											if(k < curs.size() - 1){
												rets += ",";
											}
											
											if(course.getUnit() == null || course.getUnit() == 0){
												if(curs.get(k).getDisperse()){//分散
													coursemode_theoretical_disperse_hour += ((course.getTheoretical()==null) ? 0 : course.getTheoretical()) ;
													coursemode_experiement_disperse_hour += + ((course.getExperiement()==null) ? 0 : course.getExperiement()) ;
												}else{
													coursemode_theoretical_hour += ((course.getTheoretical()==null) ? 0 : course.getTheoretical()) ;
													coursemode_experiement_hour += + ((course.getExperiement()==null) ? 0 : course.getExperiement()) ;
												}
												obj.put("hour", (((course.getTheoretical()==null) ? 0 : course.getTheoretical()) + ((course.getExperiement()==null) ? 0 : course.getExperiement())));//课程总学时
											}else{
												if(curs.get(k).getDisperse()){//分散
													coursemode_practice_disperse_week += (((course.getTheoretical()==null) ? 0 : course.getTheoretical()) + ((course.getExperiement()==null) ? 0 : course.getExperiement()));
												}else{
													coursemode_practice_disperse_week += (((course.getTheoretical()==null) ? 0 : course.getTheoretical()) + ((course.getExperiement()==null) ? 0 : course.getExperiement()));
												}
												obj.put("hour", (((course.getTheoretical()==null) ? 0 : course.getTheoretical()) + ((course.getExperiement()==null) ? 0 : course.getExperiement())) + "周");//课程总学时
											}
										}
										obj.put("semester", rets);
										obj.put("available", curs.get(0).getAvailable());
									}else{
										obj.put("semester", "");
										obj.put("available", "");
									}
														
									if(course.getUnit() == null || course.getUnit() == 0){
										obj.put("hour", (((course.getTheoretical()==null) ? 0 : course.getTheoretical()) + ((course.getExperiement()==null) ? 0 : course.getExperiement())));//课程总学时
									}else{
										obj.put("hour", (((course.getTheoretical()==null) ? 0 : course.getTheoretical()) + ((course.getExperiement()==null) ? 0 : course.getExperiement())) + "周");//课程总学时
									}
									obj.put("name", course.getName());
									obj.put("score", course.getScore());
									obj.put("theoretical", course.getTheoretical());//课程的理论学时
									obj.put("experiement", course.getExperiement());//课程的时间学时
									
									
									list.add(obj);
									
									/////////////////////////////////////////////////////////////////
									coursemode_score += ((course.getScore()==null) ? 0 : course.getScore()) ;
								}
								
								/////////////////////////////////////////////////////////////////
								//设置查询到的课程模式下的所有课程
								Map<String,Object> coursemode = new HashMap<String,Object>();
								coursemode.put("courses", list);//所有课程 + 其他信息
								//coursemode.put("courses", courses);//所有课程
								coursemode.put("theoretical", coursemode_theoretical_hour);//理论学时
								coursemode.put("dispersetheoretical", coursemode_theoretical_disperse_hour);//理论学时
								coursemode.put("experiement", coursemode_experiement_hour);//实验学时
								coursemode.put("vexperiement", coursemode_experiement_disperse_hour);//实验学时
								coursemode.put("hour", coursemode_theoretical_hour + coursemode_experiement_hour);
								coursemode.put("dispersehour", coursemode_theoretical_disperse_hour + coursemode_experiement_disperse_hour);
								coursemode.put("week", coursemode_practice_week);
								coursemode.put("disperseweek", coursemode_practice_disperse_week);
								coursemode.put("name", cm.getName());//名称
								coursemode.put("score", coursemode_score);//学分
								coursemodes.add(coursemode);
								
								//统计总计信息
								/*total_theoretical_hour += coursemode_theoretical_hour;
								total_experiement_hour += coursemode_experiement_hour;
								total_score += coursemode_score;
								total_hour += (coursemode_theoretical_hour + coursemode_experiement_hour);*/
							}
						}
					}
					coursetype.put("coursemodes", coursemodes);
					coursetype.put("name", ct.getName());
					
					coursetypes.add(coursetype);
				}
			}
			platform.put("coursetypes", coursetypes);
			platform.put("name", plf.getName());
			platforms.add(platform);
			
			System.out.println("===================" + plf.getName() + "===============================");
		}
		
		Map<String,Object> platforms_data = new HashMap<String,Object>();
		platforms_data.put("platforms", platforms);
		
		Map<String,Object> statictics = queryCourseTimeScore(specialityId,is);
		platforms_data.putAll(statictics);
		return platforms_data;
	}
	
	private Map<String,Object> queryCourseTimeScore(Long specialityId, Boolean is){
		Map<String,Object> ts = new HashMap<String,Object>();
		double[][] theoretical_hours = new double[20][10];//理论课学时
		double[][] experiement_hours = new double[20][10];//实验课学时
		double[][] theoretical_weeks = new double[20][10];//理论课学周
		double[][] experiement_weeks = new double[20][10];//实验课学周
		double[][] scores = new double[20][10];//学分
		double[][] theoretical_disperse_hours = new double[20][10];//分散理论课学时
		double[][] experiement_disperse_hours = new double[20][10];//分散实验课学时
		double[][] theoretical_disperse_weeks = new double[20][10];//分散理论课学周
		double[][] experiement_disperse_weeks = new double[20][10];//分散实验课学周
		
		List<Curriculum> curriculums = curriculumService.selectAllCurriculumBySpeciality(specialityId,is);
		if(curriculums != null && curriculums.size() > 0){
			for (Curriculum curriculum : curriculums) {//对该培养方案中所有课程进行统计
				if(is.equals(new Boolean(true))){//是否考虑已开
					if(curriculum.getAvailable().equals(is)){//如果已开，则统计
						Course theCourse = courseService.selectCourseById(curriculum.getCourseid());
						if(theCourse.getUnit() == null || theCourse.getUnit() == 0){//统计学时
							if(curriculum.getDisperse()){//分散
								theoretical_disperse_hours[theCourse.getCoursetypeid().intValue()][theCourse.getCoursemodeid().intValue()] += ((theCourse.getTheoretical()==null)?0:theCourse.getTheoretical());
								experiement_disperse_hours[theCourse.getCoursetypeid().intValue()][theCourse.getCoursemodeid().intValue()] += ((theCourse.getExperiement()==null)?0:theCourse.getExperiement());
							}else{
								theoretical_hours[theCourse.getCoursetypeid().intValue()][theCourse.getCoursemodeid().intValue()] += ((theCourse.getTheoretical()==null)?0:theCourse.getTheoretical());
								experiement_hours[theCourse.getCoursetypeid().intValue()][theCourse.getCoursemodeid().intValue()] += ((theCourse.getExperiement()==null)?0:theCourse.getExperiement());
							}
							
						}else{//统计学周
							if(curriculum.getDisperse()){//分散
								theoretical_disperse_weeks[theCourse.getCoursetypeid().intValue()][theCourse.getCoursemodeid().intValue()] += ((theCourse.getTheoretical()==null)?0:theCourse.getTheoretical());
								experiement_disperse_weeks[theCourse.getCoursetypeid().intValue()][theCourse.getCoursemodeid().intValue()] += ((theCourse.getExperiement()==null)?0:theCourse.getExperiement());
							}else{
								theoretical_weeks[theCourse.getCoursetypeid().intValue()][theCourse.getCoursemodeid().intValue()] += ((theCourse.getTheoretical()==null)?0:theCourse.getTheoretical());
								experiement_weeks[theCourse.getCoursetypeid().intValue()][theCourse.getCoursemodeid().intValue()] += ((theCourse.getExperiement()==null)?0:theCourse.getExperiement());
							}
						}
						//学分全统计
						scores[theCourse.getCoursetypeid().intValue()][theCourse.getCoursemodeid().intValue()] += ((theCourse.getScore()==null)?0:theCourse.getScore());
					}
				}else{
					Course theCourse = courseService.selectCourseById(curriculum.getCourseid());
					if(theCourse.getUnit() == null || theCourse.getUnit() == 0){//统计学时
						if(curriculum.getDisperse()){//分散
							theoretical_disperse_hours[theCourse.getCoursetypeid().intValue()][theCourse.getCoursemodeid().intValue()] += ((theCourse.getTheoretical()==null)?0:theCourse.getTheoretical());
							experiement_disperse_hours[theCourse.getCoursetypeid().intValue()][theCourse.getCoursemodeid().intValue()] += ((theCourse.getExperiement()==null)?0:theCourse.getExperiement());
						}else{
							theoretical_hours[theCourse.getCoursetypeid().intValue()][theCourse.getCoursemodeid().intValue()] += ((theCourse.getTheoretical()==null)?0:theCourse.getTheoretical());
							experiement_hours[theCourse.getCoursetypeid().intValue()][theCourse.getCoursemodeid().intValue()] += ((theCourse.getExperiement()==null)?0:theCourse.getExperiement());
						}
					}else{//统计学周
						if(curriculum.getDisperse()){//分散
							theoretical_disperse_weeks[theCourse.getCoursetypeid().intValue()][theCourse.getCoursemodeid().intValue()] += ((theCourse.getTheoretical()==null)?0:theCourse.getTheoretical());
							experiement_disperse_weeks[theCourse.getCoursetypeid().intValue()][theCourse.getCoursemodeid().intValue()] += ((theCourse.getExperiement()==null)?0:theCourse.getExperiement());
						}else{
							theoretical_weeks[theCourse.getCoursetypeid().intValue()][theCourse.getCoursemodeid().intValue()] += ((theCourse.getTheoretical()==null)?0:theCourse.getTheoretical());
							experiement_weeks[theCourse.getCoursetypeid().intValue()][theCourse.getCoursemodeid().intValue()] += ((theCourse.getExperiement()==null)?0:theCourse.getExperiement());
						}
					}
					scores[theCourse.getCoursetypeid().intValue()][theCourse.getCoursemodeid().intValue()] += ((theCourse.getScore()==null)?0:theCourse.getScore());
				}
			}
		}
		//必修课
		double total_compulsory_theoretical_hour = 0;
		double total_compulsory_experiement_hour = 0;
		double total_compulsory_practice_hour = 0;
		double total_compulsory_theoretical_week = 0;
		double total_compulsory_experiement_week = 0;
		double total_compulsory_practice_week = 0;
		
		double total_compulsory_disperse_theoretical_hour = 0;
		double total_compulsory_disperse_experiement_hour = 0;
		double total_compulsory_disperse_practice_hour = 0;
		double total_compulsory_disperse_theoretical_week = 0;
		double total_compulsory_disperse_experiement_week = 0;
		double total_compulsory_disperse_practice_week = 0;
		
		double total_compulsory_theoretical_score = 0;
		double total_compulsory_practice_score = 0;
		//选修课
		double total_elective_theoretical_hour = 0;
		double total_elective_experiement_hour = 0;
		double total_elective_practice_hour = 0;
		double total_elective_theoretical_week = 0;
		double total_elective_experiement_week = 0;
		double total_elective_practice_week = 0;
		
		double total_elective_theoretical_score = 0;
		double total_elective_practice_score = 0;
		
		double total_elective_disperse_theoretical_hour = 0;
		double total_elective_disperse_experiement_hour = 0;
		double total_elective_disperse_practice_hour = 0;
		double total_elective_disperse_theoretical_week = 0;
		double total_elective_disperse_experiement_week = 0;
		double total_elective_disperse_practice_week = 0;
		
		for(int coursemode = 1 ; coursemode <= 3; coursemode ++){
			if(coursemode == 1){//必修课
				for(int coursetype = 1; coursetype <= 19; coursetype ++){
					if(coursetype == 8 || coursetype == 9){//实践课
						//统计学时
						total_compulsory_practice_hour += theoretical_hours[coursetype][coursemode] + experiement_hours[coursetype][coursemode];
						total_compulsory_disperse_practice_hour += theoretical_disperse_hours[coursetype][coursemode] + experiement_disperse_hours[coursetype][coursemode];
						//统计学分
						total_compulsory_practice_score += scores[coursetype][coursemode] + experiement_hours[coursetype][coursemode];
						//统计学周
						total_compulsory_practice_week += theoretical_weeks[coursetype][coursemode] + experiement_weeks[coursetype][coursemode];
						total_compulsory_disperse_practice_week += theoretical_disperse_weeks[coursetype][coursemode] + experiement_disperse_weeks[coursetype][coursemode];
					}else{//理论课
						//统计学时
						total_compulsory_theoretical_hour += theoretical_hours[coursetype][coursemode];
						total_compulsory_disperse_theoretical_hour += theoretical_disperse_hours[coursetype][coursemode];
						total_compulsory_experiement_hour += experiement_hours[coursetype][coursemode];
						total_compulsory_disperse_experiement_hour += experiement_disperse_hours[coursetype][coursemode];
						//统计学周
						total_compulsory_theoretical_week += theoretical_weeks[coursetype][coursemode];
						total_compulsory_disperse_theoretical_week += theoretical_disperse_weeks[coursetype][coursemode];
						total_compulsory_experiement_week += experiement_weeks[coursetype][coursemode];
						total_compulsory_disperse_experiement_week += experiement_disperse_weeks[coursetype][coursemode];
						//统计学分
						total_compulsory_theoretical_score += scores[coursetype][coursemode];
					}
				}
			}else{//选修课
				for(int coursetype = 1; coursetype <= 19; coursetype ++){
					if(coursetype == 8 || coursetype == 9){//实践课
						//统计学时
						total_elective_practice_hour += theoretical_hours[coursetype][coursemode] + experiement_hours[coursetype][coursemode];
						total_elective_disperse_practice_hour += theoretical_disperse_hours[coursetype][coursemode] + experiement_disperse_hours[coursetype][coursemode];
						total_elective_practice_score += scores[coursetype][coursemode] + experiement_hours[coursetype][coursemode];
						//统计学周
						total_elective_practice_week += theoretical_weeks[coursetype][coursemode] + experiement_weeks[coursetype][coursemode];
						total_elective_disperse_practice_week += theoretical_disperse_weeks[coursetype][coursemode] + experiement_disperse_weeks[coursetype][coursemode];
					}else{//理论课
						//统计学时
						total_elective_theoretical_hour += theoretical_hours[coursetype][coursemode];
						total_elective_disperse_theoretical_hour += theoretical_disperse_hours[coursetype][coursemode];
						total_elective_experiement_hour += experiement_hours[coursetype][coursemode];
						total_elective_disperse_experiement_hour += experiement_disperse_hours[coursetype][coursemode];
						//统计学周
						total_elective_theoretical_week += theoretical_weeks[coursetype][coursemode];
						total_elective_disperse_theoretical_week += theoretical_disperse_weeks[coursetype][coursemode];
						total_elective_experiement_week += experiement_weeks[coursetype][coursemode];
						total_elective_disperse_experiement_week += experiement_disperse_weeks[coursetype][coursemode];
						//统计学分
						total_elective_theoretical_score += scores[coursetype][coursemode];
					}
				}
			}
		}
		//必修课
		ts.put("total_coursetype_compulsory_theoretical_hour", total_compulsory_theoretical_hour);//必修课总理论课的学时
		ts.put("total_coursetype_compulsory_experiement_hour", total_compulsory_experiement_hour);//必修课总实验课的学时
		ts.put("total_coursetype_compulsory_practice_hour", total_compulsory_practice_hour);//选修课总实践课的学时
		ts.put("total_coursetype_compulsory_theoretical_week", total_compulsory_theoretical_week);//必修课总理论课的学周
		ts.put("total_coursetype_compulsory_experiement_week", total_compulsory_experiement_week);//必修课总实验课的学周
		ts.put("total_coursetype_compulsory_practice_week", total_compulsory_practice_week);//必修课总实践课的学周
		ts.put("total_coursetype_compulsory_theoretical_score", total_compulsory_theoretical_score);//必修课总理论课的学分
		ts.put("total_coursetype_compulsory_practice_score", total_compulsory_practice_score);//选修课总实践课的学分
		
		ts.put("total_coursetype_compulsory_disperse_theoretical_hour", total_compulsory_disperse_theoretical_hour);//必修课总理论课的分散学时
		ts.put("total_coursetype_compulsory_disperse_experiement_hour", total_compulsory_disperse_experiement_hour);//必修课总实验课的分散学时
		ts.put("total_coursetype_compulsory_disperse_practice_hour", total_compulsory_disperse_practice_hour);//选修课总实践课的分散学时
		ts.put("total_coursetype_compulsory_disperse_theoretical_week", total_compulsory_disperse_theoretical_week);//必修课总理论课的分散学周
		ts.put("total_coursetype_compulsory_disperse_experiement_week", total_compulsory_disperse_experiement_week);//必修课总实验课的分散学周
		ts.put("total_coursetype_compulsory_disperse_practice_week", total_compulsory_disperse_practice_week);//必修课总实践课的分散学周
		
		//选修课
		ts.put("total_coursetype_elective_theoretical_hour", total_elective_theoretical_hour);//选修课总理论课的学时
		ts.put("total_coursetype_elective_experiement_hour", total_elective_experiement_hour);//选修课总实验课的学时
		ts.put("total_coursetype_elective_practice_hour", total_elective_practice_hour);//选修课总实践课的学时
		ts.put("total_coursetype_elective_theoretical_week", total_elective_theoretical_week);//选修课总理论课的学周
		ts.put("total_coursetype_elective_experiement_week", total_elective_experiement_week);//选修课总实验课的学周
		ts.put("total_coursetype_elective_practice_week", total_elective_practice_week);//选修课总实践课的学周
		ts.put("total_coursemode_elective_theoretical_score", total_elective_theoretical_score);//选修课理论课总的学分
		ts.put("total_coursemode_elective_practice_score", total_elective_practice_score);//选修课实践课总的学分
		
		ts.put("total_coursetype_elective_disperse_theoretical_hour", total_elective_disperse_theoretical_hour);//选修课总理论课的分散学时
		ts.put("total_coursetype_elective_disperse_experiement_hour", total_elective_disperse_experiement_hour);//选修课总实验课的分散学时
		ts.put("total_coursetype_elective_disperse_practice_hour", total_elective_disperse_practice_hour);//选修课总实践课的分散学时
		ts.put("total_coursetype_elective_disperse_theoretical_week", total_elective_disperse_theoretical_week);//选修课总理论课的分散学周
		ts.put("total_coursetype_elective_disperse_experiement_week", total_elective_disperse_experiement_week);//选修课总实验课的分散学周
		ts.put("total_coursetype_elective_disperse_practice_week", total_elective_disperse_practice_week);//选修课总实践课的分散学周
		
		//课堂教学
		ts.put("totaltheoreticalhour", total_compulsory_theoretical_hour + total_elective_theoretical_hour);//总的理论学时
		ts.put("totalexperiementhour", total_compulsory_experiement_hour + total_elective_experiement_hour);//总的实验学时
		ts.put("totaltheoreticalweek", total_compulsory_theoretical_week + total_elective_theoretical_week);//总的理论学周
		ts.put("totalexperiementweek", total_compulsory_experiement_week + total_elective_experiement_week);//总的实验学周
		ts.put("totaltheoreticalscore", total_compulsory_theoretical_score + total_elective_theoretical_score);//总的学分
		
		ts.put("totaltheoreticaldispersehour", total_compulsory_disperse_theoretical_hour + total_elective_disperse_theoretical_hour);//总的理论分散学时
		ts.put("totalexperiementdispersehour", total_compulsory_disperse_experiement_hour + total_elective_disperse_experiement_hour);//总的实验分散学时
		ts.put("totaltheoreticaldisperseweek", total_compulsory_disperse_theoretical_week + total_elective_disperse_theoretical_week);//总的理论分散学周
		ts.put("totalexperiementdisperseweek", total_compulsory_disperse_experiement_week + total_elective_disperse_experiement_week);//总的实验分散学周
		//实践教学
		ts.put("totalpracticehour", total_compulsory_disperse_practice_hour + total_elective_disperse_practice_hour);//总的实践学时
		ts.put("totalpracticeweek", total_compulsory_disperse_practice_week + total_elective_disperse_practice_week);//总的实践学周
		ts.put("totalpracticescore", total_compulsory_practice_score + total_elective_practice_score);//总的实践学分
		ts.put("totalpracticedispersehour", total_compulsory_disperse_practice_hour + total_elective_disperse_practice_hour);//总的实践学时
		ts.put("totalpracticedisperseweek", total_compulsory_disperse_practice_week + total_elective_disperse_practice_week);//总的实践学周
		
		return ts;
	}
	

	private String double2String(double num){
		if(new Double(num).intValue() - num == 0){//判断是否符合取整条件
			return String.valueOf(new Double(num).intValue());
		}else{
			return String.valueOf(new Double(num));
		}
	}
}
