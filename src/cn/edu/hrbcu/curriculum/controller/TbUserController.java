package cn.edu.hrbcu.curriculum.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;

import cn.edu.hrbcu.curriculum.pojo.TbUser;
import cn.edu.hrbcu.curriculum.service.TbUserService;

@Controller
@RequestMapping("/TbUserController")
public class TbUserController {
	
	@Autowired
	TbUserService tbUserService;
	
	@RequestMapping("/QueryTbUser.action")
	public ModelAndView QueryTbUser(@RequestParam(value = "currentPage",required=false, defaultValue="1") Integer currentPage){
		
		List<TbUser> list = tbUserService.selectTbUsers(currentPage);
		
		PageInfo<TbUser> page = new PageInfo<TbUser>(list);
		
		ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/QueryTbUser");
        modelAndView.addObject("pageInfo", page);
        modelAndView.addObject("page_url", "/CurriculumSystemManager/TbUserController/QueryTbUser.action");
        
		return modelAndView;
	}
	
	@RequestMapping("/QueryAllTbUser.action")
	@ResponseBody
	public Object QueryAllTbUser(){
		List<TbUser> list = tbUserService.selectTbUsers(-1);		
		return list;
	}
	
	@RequestMapping("/AddTbUser.action")
	public Object AddTbUser(TbUser user){
		
		tbUserService.insertUser(user);
		
		return "forward:/TbUserController/QueryTbUser.action";
	}
	
	@RequestMapping("/DeleteTbUser.action")
	public Object DeleteTbUser(TbUser tbUser){
		
		tbUserService.deletTbUser(tbUser);
		
		return "forward:/TbUserController/QueryTbUser.action";
	}
	
	@RequestMapping("/toUpdateTbUser.action")
	public Object toUpdateTbUser(TbUser tbUser){
		
		tbUser = tbUserService.selectTbUserById(tbUser);
		ModelAndView modelAndView = new ModelAndView();
		 modelAndView.setViewName("/UpdateTbUser");
        modelAndView.addObject("tbUser", tbUser);
        
		return modelAndView;
	}
	
	@RequestMapping("/UpdateTbUser.action")
	public Object UpdateTbUser(TbUser tbUser){
		int cnt = tbUserService.updateTbUser(tbUser);
		
		return "forward:/TbUserController/QueryTbUser.action";
	}

}
