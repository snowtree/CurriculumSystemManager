package cn.edu.hrbcu.curriculum.controller;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;

import cn.edu.hrbcu.curriculum.pojo.Academy;
import cn.edu.hrbcu.curriculum.pojo.Course;
import cn.edu.hrbcu.curriculum.pojo.Coursemode;
import cn.edu.hrbcu.curriculum.pojo.Coursetype;
import cn.edu.hrbcu.curriculum.pojo.Examinationmethod;
import cn.edu.hrbcu.curriculum.pojo.Speciality;
import cn.edu.hrbcu.curriculum.pojo.TbUser;
import cn.edu.hrbcu.curriculum.pojo.Teachingmethod;
import cn.edu.hrbcu.curriculum.service.AcademyService;
import cn.edu.hrbcu.curriculum.service.CourseService;
import cn.edu.hrbcu.curriculum.service.CoursemodeService;
import cn.edu.hrbcu.curriculum.service.CoursetypeService;
import cn.edu.hrbcu.curriculum.service.ExaminationmethodService;
import cn.edu.hrbcu.curriculum.service.SpecialityService;
import cn.edu.hrbcu.curriculum.service.TbUserService;
import cn.edu.hrbcu.curriculum.service.TeachingmethodService;


@Controller
@RequestMapping("/CourseController")
public class CourseController {
	@Autowired
	CourseService courseService;
	@Autowired
	AcademyService academyService;
	@Autowired
	CoursemodeService coursemodeService;
	@Autowired
	CoursetypeService  coursetypeService;
	@Autowired
	TeachingmethodService  teachingmethodService;
	@Autowired
	ExaminationmethodService examinationmethodService;
	@Autowired
	private SpecialityService specialityService;
	@Autowired
	private TbUserService tbUserService;
	
	@RequestMapping("/QueryCourse.action")
	public ModelAndView QueryCourse(@RequestParam(value = "currentPage",required=false, defaultValue="1")
	Integer currentPage, /* @RequestParam(value= "course",required=false) */Course course){
		System.out.println(course);
		List<Course> list = courseService.selectCourses(currentPage,course);
		List<Academy> academies = academyService.selectAllAcademy();
		List<Speciality> specialities = specialityService.selectAllSpeciality();
		
		PageInfo<Course> page = new PageInfo<Course>(list);
		Long specialityid = 0L;
		Subject subject = SecurityUtils.getSubject();
		List<TbUser> tbUsers = tbUserService.selectUserByLoginname((String) subject.getPrincipal());
		if (tbUsers != null && tbUsers.size() > 0) {
			Speciality speciality = specialityService.selectSpecialityByUserId(tbUsers.get(0).getId());
			if(speciality != null) {
				specialityid = speciality.getId();
			}
		}
		ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/QueryCourse");
        modelAndView.addObject("pageInfo", page);
        modelAndView.addObject("specialityid", specialityid);
        modelAndView.addObject("academies", academies);
        modelAndView.addObject("specialities", specialities);
        modelAndView.addObject("page_url", "/CurriculumSystemManager/CourseController/QueryCourse.action");
        
		return modelAndView;
	}
	
	@RequestMapping("/QueryAllCourse.action")
	@ResponseBody
	public Object QueryAllCourse(){
		List<Course> list = courseService.selectCourses(-1);		
		return list;
	}
	
	@RequestMapping("/QueryCourseBy.action")
	@ResponseBody
	public Object QueryCourseBy(Course course){
		List<Course> list = courseService.selectCourseBy(course);		
		return list;
	}
	
	@RequestMapping("/QueryCourseById.action")
	@ResponseBody
	public Object QueryCourseById(@RequestParam(value = "id",required=true, defaultValue="1") Long courseId){
		Course course_ = new Course();
		course_.setId(courseId);
		
		Course course = courseService.selectCourseById(course_);		
		return course;
	}
	
	@RequestMapping("/AddCourse.action")
	public Object AddCourse(Course course){
		
		courseService.insertCourse(course);
		
		return "forward:/CourseController/QueryCourse.action";
	}
	
	@RequestMapping("/DeleteCourse.action")
	public Object DeleteCourse(Course course){
		
		courseService.deleteCourse(course);
		
		return "forward:/CourseController/QueryCourse.action";
	}
	
	@RequestMapping("/toAddCourse.action")
	public Object toAddCourse(){
		List<Academy> academies = academyService.selectAllAcademy();
		List<Speciality> specialities = specialityService.selectAllSpeciality();
		List<Coursemode> coursemodes = coursemodeService.selectAllCoursemode();
		List<Coursetype> coursetypes= coursetypeService.selectAllCoursetype();
		List<Teachingmethod> teachingmethods = teachingmethodService.selectAllTeachingmethod();
		List<Examinationmethod> examinationmethods = examinationmethodService.selectAllExaminationmethod();
		ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/AddCourse");
        modelAndView.addObject("academies", academies);
        modelAndView.addObject("coursemodes", coursemodes);
        modelAndView.addObject("coursetypes", coursetypes);
        modelAndView.addObject("teachingmethods", teachingmethods);
        modelAndView.addObject("specialities", specialities);
        modelAndView.addObject("examinationmethods", examinationmethods);
		return modelAndView;
		
	}
	
	@RequestMapping("/toUpdateCourse.action")
	public Object toUpdateCourse(Course course){
		List<Academy> academies = academyService.selectAllAcademy();
		List<Coursemode> coursemodes = coursemodeService.selectAllCoursemode();
		List<Coursetype> coursetypes= coursetypeService.selectAllCoursetype();
		List<Teachingmethod> teachingmethods = teachingmethodService.selectAllTeachingmethod();
		List<Examinationmethod> examinationmethods = examinationmethodService.selectAllExaminationmethod();
		List<Speciality> specialities = specialityService.selectAllSpeciality();
		course = courseService.selectCourseById(course);
		ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/UpdateCourse");
        System.out.println(academies.size());
        modelAndView.addObject("course", course);
        modelAndView.addObject("academies", academies);
        modelAndView.addObject("coursemodes", coursemodes);
        modelAndView.addObject("coursetypes", coursetypes);
        modelAndView.addObject("teachingmethods", teachingmethods);
        modelAndView.addObject("examinationmethods", examinationmethods);
        modelAndView.addObject("specialities", specialities);
		return modelAndView;
	}
	
	@RequestMapping("/UpdateCourse.action")
	public Object UpdateCourse(Course course){
		int cnt = courseService.updateCourse(course);
		
		return "forward:/CourseController/QueryCourse.action";
	}
	
	@RequestMapping("/MakeCourseCode.action")
	@ResponseBody
	public Object MakeCourseCode(){
		Boolean ret = false;
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/QueryCourse");
		
		//获取专业id
		Subject subject = SecurityUtils.getSubject();
		List<TbUser> tbUsers = tbUserService.selectUserByLoginname((String) subject.getPrincipal());
		if (tbUsers != null && tbUsers.size() > 0) {
			Speciality speciality = specialityService.selectSpecialityByUserId(tbUsers.get(0).getId());
			if(speciality != null){
				ret = courseService.makeCourseCode(9,speciality.getId(),speciality.getCode());
			}else{
				ret = false;
			}
		}
		if(ret){
			return "successful";
		}else{
			return "failure";
		}
	}
}
