package cn.edu.hrbcu.curriculum.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.github.pagehelper.PageInfo;

import cn.edu.hrbcu.curriculum.pojo.Academy;
import cn.edu.hrbcu.curriculum.pojo.Course;
import cn.edu.hrbcu.curriculum.pojo.Speciality;
import cn.edu.hrbcu.curriculum.pojo.TbRole;
import cn.edu.hrbcu.curriculum.pojo.TbUser;
import cn.edu.hrbcu.curriculum.service.AcademyService;
import cn.edu.hrbcu.curriculum.service.SpecialityService;
import cn.edu.hrbcu.curriculum.service.TbUserService;

@Controller
@RequestMapping("/TbUserRoleController")
public class TbUserRoleController {
	@Autowired
	AcademyService academyService;
	@Autowired
	SpecialityService specialityService;
	@Autowired
	TbUserService tbUserService;
	
	@RequestMapping("/QueryTbUserRole.action")
	public ModelAndView QueryTbUserRole(){
		
		/*List<TbUser> list = tbUserService.selectTbUsers(currentPage);
		
		PageInfo<TbUser> page = new PageInfo<TbUser>(list);
		
		ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/QueryTbUser");
        modelAndView.addObject("pageInfo", page);
        modelAndView.addObject("page_url", "/CurriculumSystemManager/TbUserController/QueryTbUser.action");*/
        
		ModelAndView modelAndView = new ModelAndView();
		List<TbRole> roles1 = new ArrayList<TbRole>();
		TbRole r = new TbRole();
		r.setId(new Long(1));
		r.setName("管理员");
		roles1.add(r);
		
		List<TbRole> roles2 = new ArrayList<TbRole>();
		modelAndView.addObject("roles1", roles1);
		modelAndView.addObject("roles2", roles2);
		modelAndView.setViewName("/QueryUserRole");
		return modelAndView;
	}
	
	@RequestMapping("/AsynQueryUserInfo.action")
	@ResponseBody
	public Object AsynQueryUserInfo(Integer id,String n,Integer lv){
		System.out.println("id = " + id + ";" + "name = " + n + ";" + "level = " + lv);
		List<Academy> academies = null;
		List<Speciality> specialities = null;
		List<TbUser> users = null;
		if(id == null){
			academies = academyService.selectAllAcademy();
			for (Academy academy : academies) {
				academy.setIsParent(true);
				academy.setIcon("../images/academy.png");
			}
		}else{
			if(lv == 0){
				specialities =  specialityService.selectAllSpecialityByAcademyid((long)(id.intValue()));
				for (Speciality speciality : specialities) {
					speciality.setIsParent(true);
					speciality.setIcon("../images/speciality.png");
				}
				return specialities;
			}else if(lv == 1){
				users =  tbUserService.selectAllUserBySpecialityId((long)(id.intValue()));
				for (TbUser tbUser : users) {
					tbUser.setIcon("../images/user.png");
				}
				return users;
			}
		}
		return academies;
	}


}
