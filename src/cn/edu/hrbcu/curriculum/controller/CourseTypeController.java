package cn.edu.hrbcu.curriculum.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.edu.hrbcu.curriculum.pojo.Coursetype;
import cn.edu.hrbcu.curriculum.service.CoursetypeService;


@Controller
@RequestMapping("/CourseTypeController")
public class CourseTypeController {
	@Autowired
	CoursetypeService coursetypeService;
	
	@RequestMapping("/QueryAllCoursetype.action")
	@ResponseBody
	public Object QueryAllCoursetype(){
		List<Coursetype> list = coursetypeService.selectAllCoursetype();		
		return list;
	}
}
