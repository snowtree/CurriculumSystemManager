package cn.edu.hrbcu.curriculum.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import cn.edu.hrbcu.curriculum.pojo.TbPermission;
import cn.edu.hrbcu.curriculum.pojo.TbRole;
import cn.edu.hrbcu.curriculum.service.TbPermissionService;

@Controller
@RequestMapping("/TbPermissionController")
public class TbPermissionController {
	
	@Autowired
	TbPermissionService tbPermissionService;
	
	@RequestMapping("/QueryTbPermission.action")
	public ModelAndView QueryTbPermission(@RequestParam(value = "currentPage",required=false, defaultValue="1") Integer currentPage){
		List<TbPermission> list = tbPermissionService.selectTbPermissions(currentPage);
		
		PageInfo<TbPermission> page = new PageInfo<TbPermission>(list);
		
		ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/QueryTbPermission");
        modelAndView.addObject("pageInfo", page);
        modelAndView.addObject("page_url", "/CurriculumSystemManager/TbPermissionController/QueryTbPermission.action");
        
		return modelAndView;
	}
	
	@RequestMapping("/QueryAllTbPermission.action")
	@ResponseBody
	public Object QueryAllTbPermission(){
		List<TbPermission> list = tbPermissionService.selectAllPermission();		
		return list;
	}
	
	
	@RequestMapping("/QueryTbPermissionById.action")
	@ResponseBody
	public Object QueryTbPermissionById(@RequestParam(value = "id",required=true, defaultValue="1") Long id){
		TbPermission tbPermission_ = new TbPermission();
		tbPermission_.setId(id);
		
		TbPermission tbPermission = tbPermissionService.selectTbPermissionById(tbPermission_);		
		return tbPermission;
	}
	
	@RequestMapping("/AddTbPermission.action")
	public Object AddTbPermission(TbPermission tbPermission){
		
		tbPermissionService.insertPermission(tbPermission);
		
		return "forward:/TbPermissionController/QueryTbPermission.action";
	}
	
	@RequestMapping("/DeleteTbPermission.action")
	public Object DeleteTbPermission(TbPermission tbPermission){
		
		tbPermissionService.deletTbPermission(tbPermission);
		
		return "forward:/TbPermissionController/QueryTbPermission.action";
	}
	

	@RequestMapping("/UpdateTbPermission.action")
	public Object UpdateTbRole(TbPermission tbPermission){
		int cnt = tbPermissionService.updateTbPermission(tbPermission);
		
		return "forward:/TbPermissionController/QueryTbPermission.action";
	}

	@RequestMapping("/toUpdateTbPermission.action")
	public Object toUpdateTbPermission(TbPermission tbPermission){
		tbPermission = tbPermissionService.selectTbPermissionById(tbPermission);
		ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/UpdateTbPermission");
        modelAndView.addObject("tbPermission", tbPermission);
		return modelAndView;
	}
	
	@RequestMapping("/toAddTbPermission.action")
	public Object toAddTbPermission(){
		ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/AddTbPermission");
		return modelAndView;
	}
	
}
