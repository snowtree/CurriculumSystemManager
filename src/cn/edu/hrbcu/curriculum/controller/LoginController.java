package cn.edu.hrbcu.curriculum.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/LoginController")
public class LoginController {
	@RequestMapping(value = "/Login.action", method = RequestMethod.POST)
	public String login(@RequestParam("form-username") String username, 
			@RequestParam("form-password") String password,HttpServletRequest request){
		
		Subject currentUser = SecurityUtils.getSubject();
		
		// let's login the current user so we can check against roles and permissions:
        if (!currentUser.isAuthenticated()) {
            UsernamePasswordToken token = new UsernamePasswordToken(username, password);
            token.setRememberMe(true);
            try {
                currentUser.login(token);
            } 
            catch (AuthenticationException ae) {
            	request.setAttribute("info", "用户名不存在或密码错误");
                return "redirect:/login.jsp";
            }
        }

        return "redirect:/jsp/index.jsp";
	}
}
