package cn.edu.hrbcu.curriculum.pojo;

import java.io.Serializable;

public class Course implements Serializable {
    @Override
	public String toString() {
		return "Course [id=" + id + ", name=" + name + ", identifier=" + identifier + ", theoretical=" + theoretical
				+ ", experiement=" + experiement + ", score=" + score + ", academyid=" + academyid + ", coursemodeid="
				+ coursemodeid + ", coursetypeid=" + coursetypeid + ", specialityid=" + specialityid
				+ ", teachingmethodid=" + teachingmethodid + ", examinationmethodid=" + examinationmethodid + "]";
	}

	private Long id;

    private String name;

    private String identifier;

    private Double theoretical;

    private Double experiement;

    private Integer unit;

    private Double score;

    private Long academyid;

    private Long coursemodeid;

    private Long coursetypeid;

    private Long specialityid;

    private Long teachingmethodid;

    private Long examinationmethodid;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier == null ? null : identifier.trim();
    }

    public Double getTheoretical() {
        return theoretical;
    }

    public void setTheoretical(Double theoretical) {
        this.theoretical = theoretical;
    }

    public Double getExperiement() {
        return experiement;
    }

    public void setExperiement(Double experiement) {
        this.experiement = experiement;
    }

    public Integer getUnit() {
        return unit;
    }

    public void setUnit(Integer unit) {
        this.unit = unit;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Long getAcademyid() {
        return academyid;
    }

    public void setAcademyid(Long academyid) {
        this.academyid = academyid;
    }

    public Long getCoursemodeid() {
        return coursemodeid;
    }

    public void setCoursemodeid(Long coursemodeid) {
        this.coursemodeid = coursemodeid;
    }

    public Long getCoursetypeid() {
        return coursetypeid;
    }

    public void setCoursetypeid(Long coursetypeid) {
        this.coursetypeid = coursetypeid;
    }

    public Long getSpecialityid() {
        return specialityid;
    }

    public void setSpecialityid(Long specialityid) {
        this.specialityid = specialityid;
    }

    public Long getTeachingmethodid() {
        return teachingmethodid;
    }

    public void setTeachingmethodid(Long teachingmethodid) {
        this.teachingmethodid = teachingmethodid;
    }

    public Long getExaminationmethodid() {
        return examinationmethodid;
    }

    public void setExaminationmethodid(Long examinationmethodid) {
        this.examinationmethodid = examinationmethodid;
    }
}