package cn.edu.hrbcu.curriculum.pojo;

import java.io.Serializable;

public class TbUserRole implements Serializable {
    private Long id;

    private Long userid;

    private Long roleid;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public Long getRoleid() {
        return roleid;
    }

    public void setRoleid(Long roleid) {
        this.roleid = roleid;
    }
}