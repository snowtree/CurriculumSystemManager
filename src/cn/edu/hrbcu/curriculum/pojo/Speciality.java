package cn.edu.hrbcu.curriculum.pojo;

import java.io.Serializable;

public class Speciality implements Serializable {
    private Long id;

    private String name;

    private Long tbuserid;

    private Integer code;

    private Long academyid;
    
    private Boolean isParent;
    
    private String icon;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Long getTbuserid() {
        return tbuserid;
    }

    public void setTbuserid(Long tbuserid) {
        this.tbuserid = tbuserid;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Long getAcademyid() {
        return academyid;
    }

    public void setAcademyid(Long academyid) {
        this.academyid = academyid;
    }

	public Boolean getIsParent() {
		return isParent;
	}

	public void setIsParent(Boolean isParent) {
		this.isParent = isParent;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
    
    
}