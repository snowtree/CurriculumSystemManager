package cn.edu.hrbcu.curriculum.pojo;

import java.io.Serializable;

public class Curriculum implements Serializable {
    private Long id;

    private Long specialityid;

    private Long courseid;

    private Integer semester;

    private Integer start;

    private Integer end;

    private Double hpw;

    private Integer sort;

    private Boolean available;

    private Boolean disperse;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSpecialityid() {
        return specialityid;
    }

    public void setSpecialityid(Long specialityid) {
        this.specialityid = specialityid;
    }

    public Long getCourseid() {
        return courseid;
    }

    public void setCourseid(Long courseid) {
        this.courseid = courseid;
    }

    public Integer getSemester() {
        return semester;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getEnd() {
        return end;
    }

    public void setEnd(Integer end) {
        this.end = end;
    }

    public Double getHpw() {
        return hpw;
    }

    public void setHpw(Double hpw) {
        this.hpw = hpw;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Boolean getDisperse() {
        return disperse;
    }

    public void setDisperse(Boolean disperse) {
        this.disperse = disperse;
    }
}