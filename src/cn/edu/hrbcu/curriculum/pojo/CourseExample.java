package cn.edu.hrbcu.curriculum.pojo;

import java.util.ArrayList;
import java.util.List;

public class CourseExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CourseExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andIdentifierIsNull() {
            addCriterion("identifier is null");
            return (Criteria) this;
        }

        public Criteria andIdentifierIsNotNull() {
            addCriterion("identifier is not null");
            return (Criteria) this;
        }

        public Criteria andIdentifierEqualTo(String value) {
            addCriterion("identifier =", value, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierNotEqualTo(String value) {
            addCriterion("identifier <>", value, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierGreaterThan(String value) {
            addCriterion("identifier >", value, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierGreaterThanOrEqualTo(String value) {
            addCriterion("identifier >=", value, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierLessThan(String value) {
            addCriterion("identifier <", value, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierLessThanOrEqualTo(String value) {
            addCriterion("identifier <=", value, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierLike(String value) {
            addCriterion("identifier like", value, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierNotLike(String value) {
            addCriterion("identifier not like", value, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierIn(List<String> values) {
            addCriterion("identifier in", values, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierNotIn(List<String> values) {
            addCriterion("identifier not in", values, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierBetween(String value1, String value2) {
            addCriterion("identifier between", value1, value2, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierNotBetween(String value1, String value2) {
            addCriterion("identifier not between", value1, value2, "identifier");
            return (Criteria) this;
        }

        public Criteria andTheoreticalIsNull() {
            addCriterion("theoretical is null");
            return (Criteria) this;
        }

        public Criteria andTheoreticalIsNotNull() {
            addCriterion("theoretical is not null");
            return (Criteria) this;
        }

        public Criteria andTheoreticalEqualTo(Double value) {
            addCriterion("theoretical =", value, "theoretical");
            return (Criteria) this;
        }

        public Criteria andTheoreticalNotEqualTo(Double value) {
            addCriterion("theoretical <>", value, "theoretical");
            return (Criteria) this;
        }

        public Criteria andTheoreticalGreaterThan(Double value) {
            addCriterion("theoretical >", value, "theoretical");
            return (Criteria) this;
        }

        public Criteria andTheoreticalGreaterThanOrEqualTo(Double value) {
            addCriterion("theoretical >=", value, "theoretical");
            return (Criteria) this;
        }

        public Criteria andTheoreticalLessThan(Double value) {
            addCriterion("theoretical <", value, "theoretical");
            return (Criteria) this;
        }

        public Criteria andTheoreticalLessThanOrEqualTo(Double value) {
            addCriterion("theoretical <=", value, "theoretical");
            return (Criteria) this;
        }

        public Criteria andTheoreticalIn(List<Double> values) {
            addCriterion("theoretical in", values, "theoretical");
            return (Criteria) this;
        }

        public Criteria andTheoreticalNotIn(List<Double> values) {
            addCriterion("theoretical not in", values, "theoretical");
            return (Criteria) this;
        }

        public Criteria andTheoreticalBetween(Double value1, Double value2) {
            addCriterion("theoretical between", value1, value2, "theoretical");
            return (Criteria) this;
        }

        public Criteria andTheoreticalNotBetween(Double value1, Double value2) {
            addCriterion("theoretical not between", value1, value2, "theoretical");
            return (Criteria) this;
        }

        public Criteria andExperiementIsNull() {
            addCriterion("experiement is null");
            return (Criteria) this;
        }

        public Criteria andExperiementIsNotNull() {
            addCriterion("experiement is not null");
            return (Criteria) this;
        }

        public Criteria andExperiementEqualTo(Double value) {
            addCriterion("experiement =", value, "experiement");
            return (Criteria) this;
        }

        public Criteria andExperiementNotEqualTo(Double value) {
            addCriterion("experiement <>", value, "experiement");
            return (Criteria) this;
        }

        public Criteria andExperiementGreaterThan(Double value) {
            addCriterion("experiement >", value, "experiement");
            return (Criteria) this;
        }

        public Criteria andExperiementGreaterThanOrEqualTo(Double value) {
            addCriterion("experiement >=", value, "experiement");
            return (Criteria) this;
        }

        public Criteria andExperiementLessThan(Double value) {
            addCriterion("experiement <", value, "experiement");
            return (Criteria) this;
        }

        public Criteria andExperiementLessThanOrEqualTo(Double value) {
            addCriterion("experiement <=", value, "experiement");
            return (Criteria) this;
        }

        public Criteria andExperiementIn(List<Double> values) {
            addCriterion("experiement in", values, "experiement");
            return (Criteria) this;
        }

        public Criteria andExperiementNotIn(List<Double> values) {
            addCriterion("experiement not in", values, "experiement");
            return (Criteria) this;
        }

        public Criteria andExperiementBetween(Double value1, Double value2) {
            addCriterion("experiement between", value1, value2, "experiement");
            return (Criteria) this;
        }

        public Criteria andExperiementNotBetween(Double value1, Double value2) {
            addCriterion("experiement not between", value1, value2, "experiement");
            return (Criteria) this;
        }

        public Criteria andUnitIsNull() {
            addCriterion("unit is null");
            return (Criteria) this;
        }

        public Criteria andUnitIsNotNull() {
            addCriterion("unit is not null");
            return (Criteria) this;
        }

        public Criteria andUnitEqualTo(Integer value) {
            addCriterion("unit =", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotEqualTo(Integer value) {
            addCriterion("unit <>", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitGreaterThan(Integer value) {
            addCriterion("unit >", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitGreaterThanOrEqualTo(Integer value) {
            addCriterion("unit >=", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitLessThan(Integer value) {
            addCriterion("unit <", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitLessThanOrEqualTo(Integer value) {
            addCriterion("unit <=", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitIn(List<Integer> values) {
            addCriterion("unit in", values, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotIn(List<Integer> values) {
            addCriterion("unit not in", values, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitBetween(Integer value1, Integer value2) {
            addCriterion("unit between", value1, value2, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotBetween(Integer value1, Integer value2) {
            addCriterion("unit not between", value1, value2, "unit");
            return (Criteria) this;
        }

        public Criteria andScoreIsNull() {
            addCriterion("score is null");
            return (Criteria) this;
        }

        public Criteria andScoreIsNotNull() {
            addCriterion("score is not null");
            return (Criteria) this;
        }

        public Criteria andScoreEqualTo(Double value) {
            addCriterion("score =", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotEqualTo(Double value) {
            addCriterion("score <>", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThan(Double value) {
            addCriterion("score >", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThanOrEqualTo(Double value) {
            addCriterion("score >=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThan(Double value) {
            addCriterion("score <", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThanOrEqualTo(Double value) {
            addCriterion("score <=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreIn(List<Double> values) {
            addCriterion("score in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotIn(List<Double> values) {
            addCriterion("score not in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreBetween(Double value1, Double value2) {
            addCriterion("score between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotBetween(Double value1, Double value2) {
            addCriterion("score not between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andAcademyidIsNull() {
            addCriterion("academyid is null");
            return (Criteria) this;
        }

        public Criteria andAcademyidIsNotNull() {
            addCriterion("academyid is not null");
            return (Criteria) this;
        }

        public Criteria andAcademyidEqualTo(Long value) {
            addCriterion("academyid =", value, "academyid");
            return (Criteria) this;
        }

        public Criteria andAcademyidNotEqualTo(Long value) {
            addCriterion("academyid <>", value, "academyid");
            return (Criteria) this;
        }

        public Criteria andAcademyidGreaterThan(Long value) {
            addCriterion("academyid >", value, "academyid");
            return (Criteria) this;
        }

        public Criteria andAcademyidGreaterThanOrEqualTo(Long value) {
            addCriterion("academyid >=", value, "academyid");
            return (Criteria) this;
        }

        public Criteria andAcademyidLessThan(Long value) {
            addCriterion("academyid <", value, "academyid");
            return (Criteria) this;
        }

        public Criteria andAcademyidLessThanOrEqualTo(Long value) {
            addCriterion("academyid <=", value, "academyid");
            return (Criteria) this;
        }

        public Criteria andAcademyidIn(List<Long> values) {
            addCriterion("academyid in", values, "academyid");
            return (Criteria) this;
        }

        public Criteria andAcademyidNotIn(List<Long> values) {
            addCriterion("academyid not in", values, "academyid");
            return (Criteria) this;
        }

        public Criteria andAcademyidBetween(Long value1, Long value2) {
            addCriterion("academyid between", value1, value2, "academyid");
            return (Criteria) this;
        }

        public Criteria andAcademyidNotBetween(Long value1, Long value2) {
            addCriterion("academyid not between", value1, value2, "academyid");
            return (Criteria) this;
        }

        public Criteria andCoursemodeidIsNull() {
            addCriterion("coursemodeid is null");
            return (Criteria) this;
        }

        public Criteria andCoursemodeidIsNotNull() {
            addCriterion("coursemodeid is not null");
            return (Criteria) this;
        }

        public Criteria andCoursemodeidEqualTo(Long value) {
            addCriterion("coursemodeid =", value, "coursemodeid");
            return (Criteria) this;
        }

        public Criteria andCoursemodeidNotEqualTo(Long value) {
            addCriterion("coursemodeid <>", value, "coursemodeid");
            return (Criteria) this;
        }

        public Criteria andCoursemodeidGreaterThan(Long value) {
            addCriterion("coursemodeid >", value, "coursemodeid");
            return (Criteria) this;
        }

        public Criteria andCoursemodeidGreaterThanOrEqualTo(Long value) {
            addCriterion("coursemodeid >=", value, "coursemodeid");
            return (Criteria) this;
        }

        public Criteria andCoursemodeidLessThan(Long value) {
            addCriterion("coursemodeid <", value, "coursemodeid");
            return (Criteria) this;
        }

        public Criteria andCoursemodeidLessThanOrEqualTo(Long value) {
            addCriterion("coursemodeid <=", value, "coursemodeid");
            return (Criteria) this;
        }

        public Criteria andCoursemodeidIn(List<Long> values) {
            addCriterion("coursemodeid in", values, "coursemodeid");
            return (Criteria) this;
        }

        public Criteria andCoursemodeidNotIn(List<Long> values) {
            addCriterion("coursemodeid not in", values, "coursemodeid");
            return (Criteria) this;
        }

        public Criteria andCoursemodeidBetween(Long value1, Long value2) {
            addCriterion("coursemodeid between", value1, value2, "coursemodeid");
            return (Criteria) this;
        }

        public Criteria andCoursemodeidNotBetween(Long value1, Long value2) {
            addCriterion("coursemodeid not between", value1, value2, "coursemodeid");
            return (Criteria) this;
        }

        public Criteria andCoursetypeidIsNull() {
            addCriterion("coursetypeid is null");
            return (Criteria) this;
        }

        public Criteria andCoursetypeidIsNotNull() {
            addCriterion("coursetypeid is not null");
            return (Criteria) this;
        }

        public Criteria andCoursetypeidEqualTo(Long value) {
            addCriterion("coursetypeid =", value, "coursetypeid");
            return (Criteria) this;
        }

        public Criteria andCoursetypeidNotEqualTo(Long value) {
            addCriterion("coursetypeid <>", value, "coursetypeid");
            return (Criteria) this;
        }

        public Criteria andCoursetypeidGreaterThan(Long value) {
            addCriterion("coursetypeid >", value, "coursetypeid");
            return (Criteria) this;
        }

        public Criteria andCoursetypeidGreaterThanOrEqualTo(Long value) {
            addCriterion("coursetypeid >=", value, "coursetypeid");
            return (Criteria) this;
        }

        public Criteria andCoursetypeidLessThan(Long value) {
            addCriterion("coursetypeid <", value, "coursetypeid");
            return (Criteria) this;
        }

        public Criteria andCoursetypeidLessThanOrEqualTo(Long value) {
            addCriterion("coursetypeid <=", value, "coursetypeid");
            return (Criteria) this;
        }

        public Criteria andCoursetypeidIn(List<Long> values) {
            addCriterion("coursetypeid in", values, "coursetypeid");
            return (Criteria) this;
        }

        public Criteria andCoursetypeidNotIn(List<Long> values) {
            addCriterion("coursetypeid not in", values, "coursetypeid");
            return (Criteria) this;
        }

        public Criteria andCoursetypeidBetween(Long value1, Long value2) {
            addCriterion("coursetypeid between", value1, value2, "coursetypeid");
            return (Criteria) this;
        }

        public Criteria andCoursetypeidNotBetween(Long value1, Long value2) {
            addCriterion("coursetypeid not between", value1, value2, "coursetypeid");
            return (Criteria) this;
        }

        public Criteria andSpecialityidIsNull() {
            addCriterion("specialityid is null");
            return (Criteria) this;
        }

        public Criteria andSpecialityidIsNotNull() {
            addCriterion("specialityid is not null");
            return (Criteria) this;
        }

        public Criteria andSpecialityidEqualTo(Long value) {
            addCriterion("specialityid =", value, "specialityid");
            return (Criteria) this;
        }

        public Criteria andSpecialityidNotEqualTo(Long value) {
            addCriterion("specialityid <>", value, "specialityid");
            return (Criteria) this;
        }

        public Criteria andSpecialityidGreaterThan(Long value) {
            addCriterion("specialityid >", value, "specialityid");
            return (Criteria) this;
        }

        public Criteria andSpecialityidGreaterThanOrEqualTo(Long value) {
            addCriterion("specialityid >=", value, "specialityid");
            return (Criteria) this;
        }

        public Criteria andSpecialityidLessThan(Long value) {
            addCriterion("specialityid <", value, "specialityid");
            return (Criteria) this;
        }

        public Criteria andSpecialityidLessThanOrEqualTo(Long value) {
            addCriterion("specialityid <=", value, "specialityid");
            return (Criteria) this;
        }

        public Criteria andSpecialityidIn(List<Long> values) {
            addCriterion("specialityid in", values, "specialityid");
            return (Criteria) this;
        }

        public Criteria andSpecialityidNotIn(List<Long> values) {
            addCriterion("specialityid not in", values, "specialityid");
            return (Criteria) this;
        }

        public Criteria andSpecialityidBetween(Long value1, Long value2) {
            addCriterion("specialityid between", value1, value2, "specialityid");
            return (Criteria) this;
        }

        public Criteria andSpecialityidNotBetween(Long value1, Long value2) {
            addCriterion("specialityid not between", value1, value2, "specialityid");
            return (Criteria) this;
        }

        public Criteria andTeachingmethodidIsNull() {
            addCriterion("teachingmethodid is null");
            return (Criteria) this;
        }

        public Criteria andTeachingmethodidIsNotNull() {
            addCriterion("teachingmethodid is not null");
            return (Criteria) this;
        }

        public Criteria andTeachingmethodidEqualTo(Long value) {
            addCriterion("teachingmethodid =", value, "teachingmethodid");
            return (Criteria) this;
        }

        public Criteria andTeachingmethodidNotEqualTo(Long value) {
            addCriterion("teachingmethodid <>", value, "teachingmethodid");
            return (Criteria) this;
        }

        public Criteria andTeachingmethodidGreaterThan(Long value) {
            addCriterion("teachingmethodid >", value, "teachingmethodid");
            return (Criteria) this;
        }

        public Criteria andTeachingmethodidGreaterThanOrEqualTo(Long value) {
            addCriterion("teachingmethodid >=", value, "teachingmethodid");
            return (Criteria) this;
        }

        public Criteria andTeachingmethodidLessThan(Long value) {
            addCriterion("teachingmethodid <", value, "teachingmethodid");
            return (Criteria) this;
        }

        public Criteria andTeachingmethodidLessThanOrEqualTo(Long value) {
            addCriterion("teachingmethodid <=", value, "teachingmethodid");
            return (Criteria) this;
        }

        public Criteria andTeachingmethodidIn(List<Long> values) {
            addCriterion("teachingmethodid in", values, "teachingmethodid");
            return (Criteria) this;
        }

        public Criteria andTeachingmethodidNotIn(List<Long> values) {
            addCriterion("teachingmethodid not in", values, "teachingmethodid");
            return (Criteria) this;
        }

        public Criteria andTeachingmethodidBetween(Long value1, Long value2) {
            addCriterion("teachingmethodid between", value1, value2, "teachingmethodid");
            return (Criteria) this;
        }

        public Criteria andTeachingmethodidNotBetween(Long value1, Long value2) {
            addCriterion("teachingmethodid not between", value1, value2, "teachingmethodid");
            return (Criteria) this;
        }

        public Criteria andExaminationmethodidIsNull() {
            addCriterion("examinationmethodid is null");
            return (Criteria) this;
        }

        public Criteria andExaminationmethodidIsNotNull() {
            addCriterion("examinationmethodid is not null");
            return (Criteria) this;
        }

        public Criteria andExaminationmethodidEqualTo(Long value) {
            addCriterion("examinationmethodid =", value, "examinationmethodid");
            return (Criteria) this;
        }

        public Criteria andExaminationmethodidNotEqualTo(Long value) {
            addCriterion("examinationmethodid <>", value, "examinationmethodid");
            return (Criteria) this;
        }

        public Criteria andExaminationmethodidGreaterThan(Long value) {
            addCriterion("examinationmethodid >", value, "examinationmethodid");
            return (Criteria) this;
        }

        public Criteria andExaminationmethodidGreaterThanOrEqualTo(Long value) {
            addCriterion("examinationmethodid >=", value, "examinationmethodid");
            return (Criteria) this;
        }

        public Criteria andExaminationmethodidLessThan(Long value) {
            addCriterion("examinationmethodid <", value, "examinationmethodid");
            return (Criteria) this;
        }

        public Criteria andExaminationmethodidLessThanOrEqualTo(Long value) {
            addCriterion("examinationmethodid <=", value, "examinationmethodid");
            return (Criteria) this;
        }

        public Criteria andExaminationmethodidIn(List<Long> values) {
            addCriterion("examinationmethodid in", values, "examinationmethodid");
            return (Criteria) this;
        }

        public Criteria andExaminationmethodidNotIn(List<Long> values) {
            addCriterion("examinationmethodid not in", values, "examinationmethodid");
            return (Criteria) this;
        }

        public Criteria andExaminationmethodidBetween(Long value1, Long value2) {
            addCriterion("examinationmethodid between", value1, value2, "examinationmethodid");
            return (Criteria) this;
        }

        public Criteria andExaminationmethodidNotBetween(Long value1, Long value2) {
            addCriterion("examinationmethodid not between", value1, value2, "examinationmethodid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}