package cn.edu.hrbcu.curriculum.dao;

import cn.edu.hrbcu.curriculum.pojo.Teachingmethod;
import cn.edu.hrbcu.curriculum.pojo.TeachingmethodExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TeachingmethodMapper {
    long countByExample(TeachingmethodExample example);

    int deleteByExample(TeachingmethodExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Teachingmethod record);

    int insertSelective(Teachingmethod record);

    List<Teachingmethod> selectByExample(TeachingmethodExample example);

    Teachingmethod selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Teachingmethod record, @Param("example") TeachingmethodExample example);

    int updateByExample(@Param("record") Teachingmethod record, @Param("example") TeachingmethodExample example);

    int updateByPrimaryKeySelective(Teachingmethod record);

    int updateByPrimaryKey(Teachingmethod record);
}