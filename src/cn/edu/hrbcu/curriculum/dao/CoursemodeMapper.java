package cn.edu.hrbcu.curriculum.dao;

import cn.edu.hrbcu.curriculum.pojo.Coursemode;
import cn.edu.hrbcu.curriculum.pojo.CoursemodeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CoursemodeMapper {
    long countByExample(CoursemodeExample example);

    int deleteByExample(CoursemodeExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Coursemode record);

    int insertSelective(Coursemode record);

    List<Coursemode> selectByExample(CoursemodeExample example);

    Coursemode selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Coursemode record, @Param("example") CoursemodeExample example);

    int updateByExample(@Param("record") Coursemode record, @Param("example") CoursemodeExample example);

    int updateByPrimaryKeySelective(Coursemode record);

    int updateByPrimaryKey(Coursemode record);
}