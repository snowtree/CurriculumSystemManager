package cn.edu.hrbcu.curriculum.dao;

import cn.edu.hrbcu.curriculum.pojo.Coursetype;
import cn.edu.hrbcu.curriculum.pojo.CoursetypeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CoursetypeMapper {
    long countByExample(CoursetypeExample example);

    int deleteByExample(CoursetypeExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Coursetype record);

    int insertSelective(Coursetype record);

    List<Coursetype> selectByExample(CoursetypeExample example);

    Coursetype selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Coursetype record, @Param("example") CoursetypeExample example);

    int updateByExample(@Param("record") Coursetype record, @Param("example") CoursetypeExample example);

    int updateByPrimaryKeySelective(Coursetype record);

    int updateByPrimaryKey(Coursetype record);
}