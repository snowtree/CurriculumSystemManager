package cn.edu.hrbcu.curriculum.dao;

import cn.edu.hrbcu.curriculum.pojo.Speciality;
import cn.edu.hrbcu.curriculum.pojo.SpecialityExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SpecialityMapper {
    long countByExample(SpecialityExample example);

    int deleteByExample(SpecialityExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Speciality record);

    int insertSelective(Speciality record);

    List<Speciality> selectByExample(SpecialityExample example);

    Speciality selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Speciality record, @Param("example") SpecialityExample example);

    int updateByExample(@Param("record") Speciality record, @Param("example") SpecialityExample example);

    int updateByPrimaryKeySelective(Speciality record);

    int updateByPrimaryKey(Speciality record);
}