package cn.edu.hrbcu.curriculum.dao;

import cn.edu.hrbcu.curriculum.pojo.Examinationmethod;
import cn.edu.hrbcu.curriculum.pojo.ExaminationmethodExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ExaminationmethodMapper {
    long countByExample(ExaminationmethodExample example);

    int deleteByExample(ExaminationmethodExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Examinationmethod record);

    int insertSelective(Examinationmethod record);

    List<Examinationmethod> selectByExample(ExaminationmethodExample example);

    Examinationmethod selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Examinationmethod record, @Param("example") ExaminationmethodExample example);

    int updateByExample(@Param("record") Examinationmethod record, @Param("example") ExaminationmethodExample example);

    int updateByPrimaryKeySelective(Examinationmethod record);

    int updateByPrimaryKey(Examinationmethod record);
}