package cn.edu.hrbcu.shiro;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.realm.AuthenticatingRealm;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import cn.edu.hrbcu.curriculum.pojo.TbUser;
import cn.edu.hrbcu.curriculum.service.TbUserService;

public class ShiroRealm  extends AuthenticatingRealm{

	@Autowired
	TbUserService userService;
	
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) 
			throws AuthenticationException {
		//1.强转
		UsernamePasswordToken upToken = (UsernamePasswordToken) token;
		
		//2.获取系统管理员账号
		String username = upToken.getUsername();
        
        //3.根据系统管理员账号获取系统管理员信息
		List<TbUser> users = userService.selectUserByLoginname(username) ;
        
        //4.系统管理员存在则进行密码校验，否则，抛出异常：系统管理员不存在;
        if (users!= null && users.size() > 0){
        	TbUser user = users.get(0);
        	
        	SecurityUtils.getSubject().getSession().setAttribute("id",user.getId());
        	
        	//1)principal：认证的实体信息，可以是username，也可以是数据库表对应的用户的实体对象  
            Object principal = user.getLoginname();  
            
            //2)credentials：数据库中的密码  
            Object credentials = user.getPassword();  
            
            //3)realmName：当前realm对象的name，调用父类的getName()方法即可  
            String realmName = getName();  
            
            //4)credentialsSalt盐值  
            ByteSource credentialsSalt = ByteSource.Util.bytes(username);//使用账号作为盐值 
            
            //根据用户的情况，来构建AuthenticationInfo对象,通常使用的实现类为SimpleAuthenticationInfo
            //5)与数据库中用户名和密码进行比对，密码盐值加密，第4个参数传入realName。
            SimpleAuthenticationInfo info  = new SimpleAuthenticationInfo(principal, credentials, credentialsSalt, realmName); 
            
            return info;  
        }else{
            //6.若用户不存在，可以抛出UnknownAccountException  
            throw new UnknownAccountException("不存在该用户");//没找到帐号
        }
	}

}
