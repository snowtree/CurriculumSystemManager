<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>登录</title>

        <!-- CSS -->
        <!-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500"> -->
        <link rel="stylesheet" href="<%=request.getContextPath() %>/js/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    	<link rel="stylesheet" href="<%=request.getContextPath() %>/js/validform/css/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/js/fontawesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/css/form-elements.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/style.css">
	    
        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="<%=request.getContextPath()%>/images/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<%=request.getContextPath()%>/images/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<%=request.getContextPath()%>/images/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<%=request.getContextPath()%>/images/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<%=request.getContextPath()%>/images/apple-touch-icon-57-precomposed.png">

        <!-- Javascript -->
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.3.3.1.js"></script>
	    <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap/js/bootstrap.min.js"></script>
	    <script type="text/javascript" src="<%=request.getContextPath() %>/js/validform/js/Validform_v5.3.2.js"></script>
        <script src="<%=request.getContextPath()%>/js/jquery.backstretch.min.js"></script>
        <script src="<%=request.getContextPath()%>/js/scripts.js"></script>
    </head>
    <body>
        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>培养方案管理系统</strong></h1>
                            <div class="description">
                            	<!-- <p>
	                            	This is a free responsive login form made with Bootstrap. 
	                            	Download it on <a href="#"><strong>AZMIND</strong></a>, customize and use it as you like!
                            	</p> -->
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>登录培养方案管理网站</h3>
                            		<p>请输入您的用户名与密码:</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-key"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form" action="<%=request.getContextPath()%>/LoginController/Login.action" method="post" class="login-form">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">手机号</label>
			                        	<input type="text" name="form-username" placeholder="用户名..." class="form-username form-control" id="form-username">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">密码</label>
			                        	<input type="password" name="form-password" placeholder="密码..." class="form-password form-control" id="form-password">
			                        </div>
			                        <button type="submit" class="btn">登录!</button>
			                        <div><span>${info}</span></div>
			                    </form>
		                    </div>
                        </div>
                    </div>
                    
                </div><!-- container -->
            </div>
        </div>
    </body>
</html>