<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<w:wordDocument xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" xmlns:wsp="http://schemas.microsoft.com/office/word/2003/wordml/sp2" xmlns:sl="http://schemas.microsoft.com/schemaLibrary/2003/core" w:macrosPresent="no" w:embeddedObjPresent="no" w:ocxPresent="no" xml:space="preserve">
<w:ignoreSubtree w:val="http://schemas.microsoft.com/office/word/2003/wordml/sp2"/>
<o:DocumentProperties>
<o:Author>Administrator</o:Author>
<o:LastAuthor>nsow'notepad</o:LastAuthor>
<o:Revision>3</o:Revision>
<o:TotalTime>0</o:TotalTime>
<o:Created>2019-04-11T13:53:00Z</o:Created>
<o:LastSaved>2019-04-11T13:53:00Z</o:LastSaved>
<o:Pages>1</o:Pages>
<o:Words>107</o:Words>
<o:Characters>611</o:Characters>
<o:Company>china</o:Company>
<o:Lines>5</o:Lines>
<o:Paragraphs>1</o:Paragraphs>
<o:CharactersWithSpaces>717</o:CharactersWithSpaces>
<o:Version>15</o:Version>
</o:DocumentProperties>
<w:fonts>
<w:defaultFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
<w:font w:name="Times New Roman">
<w:panose-1 w:val="02020603050405020304"/>
<w:charset w:val="00"/>
<w:family w:val="Roman"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="E0002AFF" w:usb-1="C0007841" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="000001FF" w:csb-1="00000000"/>
</w:font>
<w:font w:name="Arial">
<w:panose-1 w:val="020B0604020202020204"/>
<w:charset w:val="00"/>
<w:family w:val="Swiss"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="E0002AFF" w:usb-1="C0007843" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="000001FF" w:csb-1="00000000"/>
</w:font>
<w:font w:name="Courier New">
<w:panose-1 w:val="02070309020205020404"/>
<w:charset w:val="00"/>
<w:family w:val="Modern"/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="E0002AFF" w:usb-1="C0007843" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="000001FF" w:csb-1="00000000"/>
</w:font>
<w:font w:name="宋体">
<w:altName w:val="SimSun"/>
<w:panose-1 w:val="02010600030101010101"/>
<w:charset w:val="86"/>
<w:family w:val="auto"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
</w:font>
<w:font w:name="黑体">
<w:altName w:val="SimHei"/>
<w:panose-1 w:val="02010609060101010101"/>
<w:charset w:val="86"/>
<w:family w:val="Modern"/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
</w:font>
<w:font w:name="黑体">
<w:altName w:val="SimHei"/>
<w:panose-1 w:val="02010609060101010101"/>
<w:charset w:val="86"/>
<w:family w:val="Modern"/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
</w:font>
<w:font w:name="Calibri">
<w:panose-1 w:val="020F0502020204030204"/>
<w:charset w:val="00"/>
<w:family w:val="Swiss"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="E10002FF" w:usb-1="4000ACFF" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="0000019F" w:csb-1="00000000"/>
</w:font>
<w:font w:name="Microsoft YaHei UI">
<w:panose-1 w:val="020B0503020204020204"/>
<w:charset w:val="86"/>
<w:family w:val="Swiss"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="80000287" w:usb-1="28CF3C52" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="0004001F" w:csb-1="00000000"/>
</w:font>
<w:font w:name="Cambria">
<w:panose-1 w:val="02040503050406030204"/>
<w:charset w:val="00"/>
<w:family w:val="Roman"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="A00002EF" w:usb-1="4000004B" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="0000019F" w:csb-1="00000000"/>
</w:font>
<w:font w:name="隶书">
<w:panose-1 w:val="02010509060101010101"/>
<w:charset w:val="86"/>
<w:family w:val="Modern"/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
</w:font>
<w:font w:name="Verdana">
<w:panose-1 w:val="020B0604030504040204"/>
<w:charset w:val="00"/>
<w:family w:val="Swiss"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="A10006FF" w:usb-1="4000205B" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="0000019F" w:csb-1="00000000"/>
</w:font>
<w:font w:name="_x000B__x000C_">
<w:altName w:val="Times New Roman"/>
<w:charset w:val="00"/>
<w:family w:val="Roman"/>
<w:pitch w:val="default"/>
<w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
</w:font>
<w:font w:name="仿宋_GB2312">
<w:altName w:val="仿宋"/>
<w:charset w:val="86"/>
<w:family w:val="Modern"/>
<w:pitch w:val="default"/>
<w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
</w:font>
<w:font w:name="楷体_GB2312">
<w:altName w:val="楷体"/>
<w:charset w:val="86"/>
<w:family w:val="Modern"/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
</w:font>
<w:font w:name="仿宋">
<w:panose-1 w:val="02010609060101010101"/>
<w:charset w:val="86"/>
<w:family w:val="Modern"/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
</w:font>
<w:font w:name="@宋体">
<w:panose-1 w:val="02010600030101010101"/>
<w:charset w:val="86"/>
<w:family w:val="auto"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
</w:font>
<w:font w:name="@仿宋">
<w:panose-1 w:val="02010609060101010101"/>
<w:charset w:val="86"/>
<w:family w:val="Modern"/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
</w:font>
<w:font w:name="@黑体">
<w:panose-1 w:val="02010609060101010101"/>
<w:charset w:val="86"/>
<w:family w:val="Modern"/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
</w:font>
<w:font w:name="@隶书">
<w:panose-1 w:val="02010509060101010101"/>
<w:charset w:val="86"/>
<w:family w:val="Modern"/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
</w:font>
<w:font w:name="@Microsoft YaHei UI">
<w:panose-1 w:val="020B0503020204020204"/>
<w:charset w:val="86"/>
<w:family w:val="Swiss"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="80000287" w:usb-1="28CF3C52" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="0004001F" w:csb-1="00000000"/>
</w:font>
<w:font w:name="@楷体_GB2312">
<w:charset w:val="86"/>
<w:family w:val="Modern"/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
</w:font>
<w:font w:name="@仿宋_GB2312">
<w:charset w:val="86"/>
<w:family w:val="Modern"/>
<w:pitch w:val="default"/>
<w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
</w:font>
</w:fonts>
<w:lists>
<w:listDef w:listDefId="0">
<w:lsid w:val="BFD10015"/>
<w:plt w:val="SingleLevel"/>
<w:tmpl w:val="BFD10015"/>
<w:lvl w:ilvl="0">
<w:start w:val="1"/>
<w:nfc w:val="37"/>
<w:suff w:val="Nothing"/>
<w:lvlText w:val="（%1）"/>
<w:lvlJc w:val="left"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="fareast"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="1">
<w:lsid w:val="00277C59"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="0A665360"/>
<w:lvl w:ilvl="0" w:tplc="0409000F">
<w:start w:val="1"/>
<w:lvlText w:val="%1."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="420" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="840" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409000F">
<w:start w:val="1"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="2">
<w:lsid w:val="01680EDF"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="2B26B80A"/>
<w:lvl w:ilvl="0" w:tplc="9836DDD8">
<w:start w:val="1"/>
<w:lvlText w:val="%1—"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="360" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="840" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="3">
<w:lsid w:val="0590072D"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="3F309D2E"/>
<w:lvl w:ilvl="0" w:tplc="933ABE90">
<w:start w:val="6"/>
<w:nfc w:val="11"/>
<w:lvlText w:val="%1、"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1054" w:hanging="600"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1294" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1714" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2134" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2554" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2974" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3394" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3814" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="4234" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="4">
<w:lsid w:val="0595583A"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="7F4E5C22"/>
<w:lvl w:ilvl="0" w:tplc="B7EA0906">
<w:start w:val="3"/>
<w:nfc w:val="11"/>
<w:lvlText w:val="%1、"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="720" w:hanging="720"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="840" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="5">
<w:lsid w:val="092B59BA"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="EBC22DD0"/>
<w:lvl w:ilvl="0" w:tplc="0409000F">
<w:start w:val="1"/>
<w:lvlText w:val="%1."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="874" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1294" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1714" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2134" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2554" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2974" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3394" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3814" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="4234" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="6">
<w:lsid w:val="0C177CFD"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="CF127BC8"/>
<w:lvl w:ilvl="0" w:tplc="23E2ECB8">
<w:start w:val="1"/>
<w:lvlText w:val="（%1）"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1280" w:hanging="720"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="593CADCA">
<w:start w:val="4"/>
<w:nfc w:val="11"/>
<w:lvlText w:val="%2、"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1700" w:hanging="720"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="634A7668">
<w:start w:val="1"/>
<w:nfc w:val="11"/>
<w:lvlText w:val="（%3）"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1430" w:hanging="720"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
<w:b/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2240" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2660" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="3080" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3500" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3920" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="4340" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="7">
<w:lsid w:val="0DFD4882"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="E1BA5136"/>
<w:lvl w:ilvl="0" w:tplc="501E1FFA">
<w:start w:val="1"/>
<w:lvlText w:val="%1"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="420" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="fareast"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="840" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="8">
<w:lsid w:val="14D269B2"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="EB166530"/>
<w:lvl w:ilvl="0" w:tplc="3236A5B6">
<w:start w:val="1"/>
<w:lvlText w:val="%1"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="420" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="fareast"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="840" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="9">
<w:lsid w:val="15841424"/>
<w:plt w:val="SingleLevel"/>
<w:tmpl w:val="15841424"/>
<w:lvl w:ilvl="0">
<w:start w:val="1"/>
<w:suff w:val="Nothing"/>
<w:lvlText w:val="（%1）"/>
<w:lvlJc w:val="left"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="10">
<w:lsid w:val="1CBD3633"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="F1A27974"/>
<w:lvl w:ilvl="0" w:tplc="501E1FFA">
<w:start w:val="1"/>
<w:lvlText w:val="%1"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="420" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="fareast"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="840" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="11">
<w:lsid w:val="1D271240"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="3DA43480"/>
<w:lvl w:ilvl="0" w:tplc="8522E1DA">
<w:start w:val="1"/>
<w:lvlText w:val="%1、"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="675" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
<w:b/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1155" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1575" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1995" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2415" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2835" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3255" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3675" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="4095" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="12">
<w:lsid w:val="20D65C2E"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="5E928E40"/>
<w:lvl w:ilvl="0" w:tplc="3B1E3F9A">
<w:start w:val="1"/>
<w:lvlText w:val="%1．"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="840" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1320" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1740" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2160" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2580" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="3000" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3420" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3840" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="4260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="13">
<w:lsid w:val="246A2371"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="1E6802D8"/>
<w:lvl w:ilvl="0" w:tplc="80442490">
<w:start w:val="1"/>
<w:nfc w:val="11"/>
<w:lvlText w:val="（%1）"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1219" w:hanging="765"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1294" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1714" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2134" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2554" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2974" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3394" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3814" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="4234" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="14">
<w:lsid w:val="2F000000"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="B3DA3FD8"/>
<w:lvl w:ilvl="0" w:tplc="A8F2F4F8">
<w:start w:val="4"/>
<w:suff w:val="Nothing"/>
<w:lvlText w:val="%1、"/>
<w:lvlJc w:val="left"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="92FEB85A">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="F6AE3A2A">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="C65EBB42">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="50FC30BE">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="52B6A206">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="6D3AD7B6">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="D9901006">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="F9E8E710">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="15">
<w:lsid w:val="2F000001"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="D64A6582"/>
<w:lvl w:ilvl="0" w:tplc="85F22764">
<w:start w:val="2"/>
<w:suff w:val="Nothing"/>
<w:lvlText w:val="%1）"/>
<w:lvlJc w:val="left"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="94D4FE3E">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="59266EBC">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="A146A960">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="6C103344">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="355EAEFC">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="71C4CAC8">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="02363BCC">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="90602BE4">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="16">
<w:lsid w:val="2F000002"/>
<w:plt w:val="Multilevel"/>
<w:tmpl w:val="1F000C5F"/>
<w:lvl w:ilvl="0">
<w:start w:val="1"/>
<w:nfc w:val="11"/>
<w:lvlText w:val="%1、"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="720" w:hanging="720"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="840" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="17">
<w:lsid w:val="2F000003"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="38EC380E"/>
<w:lvl w:ilvl="0" w:tplc="C4E87232">
<w:start w:val="1"/>
<w:suff w:val="Nothing"/>
<w:lvlText w:val="（%1）"/>
<w:lvlJc w:val="left"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="59CEC0F0">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="37BC8416">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="A4BEB12E">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="A8901092">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="109A266E">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="AE40595E">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="C53072F8">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0FA47486">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="18">
<w:lsid w:val="2F000004"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="D70C853C"/>
<w:lvl w:ilvl="0" w:tplc="E650230C">
<w:start w:val="3"/>
<w:suff w:val="Nothing"/>
<w:lvlText w:val="%1）"/>
<w:lvlJc w:val="left"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="F118E160">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="B96049AC">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="CDB092C2">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="941ECDF4">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="9BF4677A">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0DFA738A">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="E4B8091A">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="81A07BAE">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="19">
<w:lsid w:val="2F000005"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="46189274"/>
<w:lvl w:ilvl="0" w:tplc="FC8AC276">
<w:start w:val="1"/>
<w:suff w:val="Nothing"/>
<w:lvlText w:val="%1、"/>
<w:lvlJc w:val="left"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="A88EBDD2">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="502"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="B3763044">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="502"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="215895B2">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="502"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="63DC7B70">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="502"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="080CFB94">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="502"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="1D407A56">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="502"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="B7F25178">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="502"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="ED3E065A">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="502"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="20">
<w:lsid w:val="2F000006"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="11845858"/>
<w:lvl w:ilvl="0" w:tplc="87485C40">
<w:start w:val="1"/>
<w:suff w:val="Nothing"/>
<w:lvlText w:val="（%1）"/>
<w:lvlJc w:val="left"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="5A6EBD40">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="B8E257E2">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="D4BE1C9A">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="9926C2BE">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="9DB810B0">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="91F04638">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="3D8C96CA">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="5BA069D8">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="21">
<w:lsid w:val="2F000007"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="0C1C0D46"/>
<w:lvl w:ilvl="0" w:tplc="B5CCFD62">
<w:start w:val="2"/>
<w:suff w:val="Nothing"/>
<w:lvlText w:val="（%1）"/>
<w:lvlJc w:val="left"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="410A785A">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="BA165C58">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="36827D42">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="D44C1D00">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="063A1E78">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="300A5620">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="68FAAE10">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="242AB87A">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="22">
<w:lsid w:val="2F000008"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="B9B28398"/>
<w:lvl w:ilvl="0" w:tplc="04B00BC8">
<w:start w:val="4"/>
<w:suff w:val="Nothing"/>
<w:lvlText w:val="%1．"/>
<w:lvlJc w:val="left"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="6DA8549E">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="A49804DC">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="AA1A388E">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="2346BDFC">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0C36BF48">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="E7E4D2B8">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="7B2260FC">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="2662F378">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="23">
<w:lsid w:val="2F000009"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="FB48C6E0"/>
<w:lvl w:ilvl="0" w:tplc="10D2B192">
<w:start w:val="2"/>
<w:suff w:val="Nothing"/>
<w:lvlText w:val="%1．"/>
<w:lvlJc w:val="left"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="BA62DB9C">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="B3EE2202">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="09E86034">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="07DA98F8">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="DBDE7142">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="675CA09E">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="35DCC098">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="31504DC0">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="24">
<w:lsid w:val="2F00000A"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="55E6B0CE"/>
<w:lvl w:ilvl="0" w:tplc="7A5EFFB8">
<w:start w:val="1"/>
<w:lvlText w:val="%1"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="left" w:pos="397"/>
</w:tabs>
<w:ind w:left="454" w:hanging="454"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="F1028120">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="4B1017AC">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="73E4775E">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="C4382A30">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="ED2A0232">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="AC84E434">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="2144AE18">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="5EF440CE">
<w:nfc w:val="255"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="360"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="25">
<w:lsid w:val="2F00000B"/>
<w:plt w:val="Multilevel"/>
<w:tmpl w:val="1F0036F8"/>
<w:lvl w:ilvl="0">
<w:start w:val="4"/>
<w:nfc w:val="11"/>
<w:lvlText w:val="%1、"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="720" w:hanging="720"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="840" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="26">
<w:lsid w:val="2F00000C"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="1F002D78"/>
<w:lvl w:ilvl="0" w:tplc="0F8602B8">
<w:start w:val="4"/>
<w:nfc w:val="11"/>
<w:lvlText w:val="（%1）"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1238" w:hanging="756"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="BDFAD0D4">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1322" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="F2DC7546">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1742" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="6FDA8E4E">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2162" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="AAE4960E">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2582" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="40A2ECCE">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="3002" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="4448FAF8">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3422" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="8C88B47C">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3842" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="961E8E8E">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="4262" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="27">
<w:lsid w:val="2F00000D"/>
<w:plt w:val="Multilevel"/>
<w:tmpl w:val="1F0020DD"/>
<w:lvl w:ilvl="0">
<w:start w:val="4"/>
<w:lvlText w:val="%1"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="left" w:pos="360"/>
</w:tabs>
<w:ind w:left="360" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="left" w:pos="840"/>
</w:tabs>
<w:ind w:left="840" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:tabs>
<w:tab w:val="left" w:pos="1260"/>
</w:tabs>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="left" w:pos="1680"/>
</w:tabs>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="left" w:pos="2100"/>
</w:tabs>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:tabs>
<w:tab w:val="left" w:pos="2520"/>
</w:tabs>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="left" w:pos="2940"/>
</w:tabs>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="left" w:pos="3360"/>
</w:tabs>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:tabs>
<w:tab w:val="left" w:pos="3780"/>
</w:tabs>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="28">
<w:lsid w:val="31C9A838"/>
<w:plt w:val="SingleLevel"/>
<w:tmpl w:val="31C9A838"/>
<w:lvl w:ilvl="0">
<w:start w:val="1"/>
<w:nfc w:val="37"/>
<w:suff w:val="Nothing"/>
<w:lvlText w:val="（%1）"/>
<w:lvlJc w:val="left"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="fareast"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="29">
<w:lsid w:val="3FD50AA3"/>
<w:plt w:val="SingleLevel"/>
<w:tmpl w:val="7BC8410C"/>
<w:lvl w:ilvl="0">
<w:start w:val="2"/>
<w:lvlText w:val="%1)"/>
<w:legacy w:legacy="on" w:legacySpace="0" w:legacyIndent="360"/>
<w:lvlJc w:val="left"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="30">
<w:lsid w:val="48F236D7"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="08526E40"/>
<w:lvl w:ilvl="0" w:tplc="7DA0062C">
<w:start w:val="3"/>
<w:nfc w:val="11"/>
<w:lvlText w:val="%1、"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1282" w:hanging="720"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1402" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1822" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2242" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2662" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="3082" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3502" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3922" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="4342" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="31">
<w:lsid w:val="4AB67229"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="D8CA7206"/>
<w:lvl w:ilvl="0" w:tplc="0409000F">
<w:start w:val="1"/>
<w:lvlText w:val="%1."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="874" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1294" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1714" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2134" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2554" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2974" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3394" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3814" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="4234" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="32">
<w:lsid w:val="59CE2DCC"/>
<w:plt w:val="SingleLevel"/>
<w:tmpl w:val="59CE2DCC"/>
<w:lvl w:ilvl="0">
<w:start w:val="2"/>
<w:suff w:val="Nothing"/>
<w:lvlText w:val="%1．"/>
<w:lvlJc w:val="left"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="33">
<w:lsid w:val="59CEEACB"/>
<w:plt w:val="SingleLevel"/>
<w:tmpl w:val="59CEEACB"/>
<w:lvl w:ilvl="0">
<w:start w:val="1"/>
<w:lvlText w:val="%1"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="539"/>
</w:tabs>
<w:ind w:left="596" w:hanging="454"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="34">
<w:lsid w:val="5DEB3EE7"/>
<w:plt w:val="SingleLevel"/>
<w:tmpl w:val="7A163E62"/>
<w:lvl w:ilvl="0">
<w:start w:val="1"/>
<w:lvlText w:val="%1"/>
<w:legacy w:legacy="on" w:legacySpace="0" w:legacyIndent="360"/>
<w:lvlJc w:val="left"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="宋体" w:h-ansi="宋体" w:cs="Times New Roman" w:hint="fareast"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="35">
<w:lsid w:val="6BB5421D"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="C1D0FE56"/>
<w:lvl w:ilvl="0" w:tplc="1E5856FE">
<w:start w:val="3"/>
<w:nfc w:val="11"/>
<w:lvlText w:val="%1、"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="720" w:hanging="720"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="840" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="36">
<w:lsid w:val="762C5ACF"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="DAD84984"/>
<w:lvl w:ilvl="0" w:tplc="084CB068">
<w:start w:val="1"/>
<w:nfc w:val="11"/>
<w:lvlText w:val="%1、"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="720" w:hanging="720"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="AF46C3EA">
<w:start w:val="1"/>
<w:nfc w:val="11"/>
<w:lvlText w:val="（%2）"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1006" w:hanging="864"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="A15271DC">
<w:start w:val="1"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1200" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:list w:ilfo="1">
<w:ilst w:val="31"/>
</w:list>
<w:list w:ilfo="2">
<w:ilst w:val="5"/>
</w:list>
<w:list w:ilfo="3">
<w:ilst w:val="13"/>
</w:list>
<w:list w:ilfo="4">
<w:ilst w:val="6"/>
</w:list>
<w:list w:ilfo="5">
<w:ilst w:val="30"/>
</w:list>
<w:list w:ilfo="6">
<w:ilst w:val="4"/>
</w:list>
<w:list w:ilfo="7">
<w:ilst w:val="35"/>
</w:list>
<w:list w:ilfo="8">
<w:ilst w:val="36"/>
</w:list>
<w:list w:ilfo="9">
<w:ilst w:val="1"/>
</w:list>
<w:list w:ilfo="10">
<w:ilst w:val="12"/>
</w:list>
<w:list w:ilfo="11">
<w:ilst w:val="3"/>
</w:list>
<w:list w:ilfo="12">
<w:ilst w:val="11"/>
</w:list>
<w:list w:ilfo="13">
<w:ilst w:val="33"/>
</w:list>
<w:list w:ilfo="14">
<w:ilst w:val="16"/>
</w:list>
<w:list w:ilfo="15">
<w:ilst w:val="22"/>
</w:list>
<w:list w:ilfo="16">
<w:ilst w:val="18"/>
</w:list>
<w:list w:ilfo="17">
<w:ilst w:val="19"/>
</w:list>
<w:list w:ilfo="18">
<w:ilst w:val="25"/>
</w:list>
<w:list w:ilfo="19">
<w:ilst w:val="24"/>
</w:list>
<w:list w:ilfo="20">
<w:ilst w:val="23"/>
</w:list>
<w:list w:ilfo="21">
<w:ilst w:val="20"/>
</w:list>
<w:list w:ilfo="22">
<w:ilst w:val="17"/>
</w:list>
<w:list w:ilfo="23">
<w:ilst w:val="27"/>
</w:list>
<w:list w:ilfo="24">
<w:ilst w:val="21"/>
</w:list>
<w:list w:ilfo="25">
<w:ilst w:val="15"/>
</w:list>
<w:list w:ilfo="26">
<w:ilst w:val="14"/>
</w:list>
<w:list w:ilfo="27">
<w:ilst w:val="26"/>
</w:list>
<w:list w:ilfo="28">
<w:ilst w:val="32"/>
</w:list>
<w:list w:ilfo="29">
<w:ilst w:val="10"/>
</w:list>
<w:list w:ilfo="30">
<w:ilst w:val="8"/>
</w:list>
<w:list w:ilfo="31">
<w:ilst w:val="7"/>
</w:list>
<w:list w:ilfo="32">
<w:ilst w:val="34"/>
</w:list>
<w:list w:ilfo="33">
<w:ilst w:val="29"/>
</w:list>
<w:list w:ilfo="34">
<w:ilst w:val="9"/>
</w:list>
<w:list w:ilfo="35">
<w:ilst w:val="0"/>
</w:list>
<w:list w:ilfo="36">
<w:ilst w:val="28"/>
</w:list>
<w:list w:ilfo="37">
<w:ilst w:val="2"/>
</w:list>
</w:lists>
<w:styles>
<w:versionOfBuiltInStylenames w:val="7"/>
<w:latentStyles w:defLockedState="off" w:latentStyleCount="371">
<w:lsdException w:name="Normal" w:locked="on"/>
<w:lsdException w:name="heading 1" w:locked="on"/>
<w:lsdException w:name="heading 2" w:locked="on"/>
<w:lsdException w:name="heading 3" w:locked="on"/>
<w:lsdException w:name="heading 4" w:locked="on"/>
<w:lsdException w:name="heading 5" w:locked="on"/>
<w:lsdException w:name="heading 6" w:locked="on"/>
<w:lsdException w:name="heading 7" w:locked="on"/>
<w:lsdException w:name="heading 8" w:locked="on"/>
<w:lsdException w:name="heading 9" w:locked="on"/>
<w:lsdException w:name="index 1" w:locked="on"/>
<w:lsdException w:name="index 2" w:locked="on"/>
<w:lsdException w:name="index 3" w:locked="on"/>
<w:lsdException w:name="index 4" w:locked="on"/>
<w:lsdException w:name="index 5" w:locked="on"/>
<w:lsdException w:name="index 6" w:locked="on"/>
<w:lsdException w:name="index 7" w:locked="on"/>
<w:lsdException w:name="index 8" w:locked="on"/>
<w:lsdException w:name="index 9" w:locked="on"/>
<w:lsdException w:name="toc 1" w:locked="on"/>
<w:lsdException w:name="toc 2" w:locked="on"/>
<w:lsdException w:name="toc 3" w:locked="on"/>
<w:lsdException w:name="toc 4" w:locked="on"/>
<w:lsdException w:name="toc 5" w:locked="on"/>
<w:lsdException w:name="toc 6" w:locked="on"/>
<w:lsdException w:name="toc 7" w:locked="on"/>
<w:lsdException w:name="toc 8" w:locked="on"/>
<w:lsdException w:name="toc 9" w:locked="on"/>
<w:lsdException w:name="Normal Indent" w:locked="on"/>
<w:lsdException w:name="footnote text" w:locked="on"/>
<w:lsdException w:name="annotation text" w:locked="on"/>
<w:lsdException w:name="header" w:locked="on"/>
<w:lsdException w:name="footer" w:locked="on"/>
<w:lsdException w:name="index heading" w:locked="on"/>
<w:lsdException w:name="caption" w:locked="on"/>
<w:lsdException w:name="table of figures" w:locked="on"/>
<w:lsdException w:name="envelope address" w:locked="on"/>
<w:lsdException w:name="envelope return" w:locked="on"/>
<w:lsdException w:name="footnote reference" w:locked="on"/>
<w:lsdException w:name="annotation reference" w:locked="on"/>
<w:lsdException w:name="line number" w:locked="on"/>
<w:lsdException w:name="page number" w:locked="on"/>
<w:lsdException w:name="endnote reference" w:locked="on"/>
<w:lsdException w:name="endnote text" w:locked="on"/>
<w:lsdException w:name="table of authorities" w:locked="on"/>
<w:lsdException w:name="macro" w:locked="on"/>
<w:lsdException w:name="toa heading" w:locked="on"/>
<w:lsdException w:name="List" w:locked="on"/>
<w:lsdException w:name="List Bullet" w:locked="on"/>
<w:lsdException w:name="List Number" w:locked="on"/>
<w:lsdException w:name="List 2" w:locked="on"/>
<w:lsdException w:name="List 3" w:locked="on"/>
<w:lsdException w:name="List 4" w:locked="on"/>
<w:lsdException w:name="List 5" w:locked="on"/>
<w:lsdException w:name="List Bullet 2" w:locked="on"/>
<w:lsdException w:name="List Bullet 3" w:locked="on"/>
<w:lsdException w:name="List Bullet 4" w:locked="on"/>
<w:lsdException w:name="List Bullet 5" w:locked="on"/>
<w:lsdException w:name="List Number 2" w:locked="on"/>
<w:lsdException w:name="List Number 3" w:locked="on"/>
<w:lsdException w:name="List Number 4" w:locked="on"/>
<w:lsdException w:name="List Number 5" w:locked="on"/>
<w:lsdException w:name="Title" w:locked="on"/>
<w:lsdException w:name="Closing" w:locked="on"/>
<w:lsdException w:name="Signature" w:locked="on"/>
<w:lsdException w:name="Default Paragraph Font" w:locked="on"/>
<w:lsdException w:name="Body Text" w:locked="on"/>
<w:lsdException w:name="Body Text Indent" w:locked="on"/>
<w:lsdException w:name="List Continue" w:locked="on"/>
<w:lsdException w:name="List Continue 2" w:locked="on"/>
<w:lsdException w:name="List Continue 3" w:locked="on"/>
<w:lsdException w:name="List Continue 4" w:locked="on"/>
<w:lsdException w:name="List Continue 5" w:locked="on"/>
<w:lsdException w:name="Message Header" w:locked="on"/>
<w:lsdException w:name="Subtitle" w:locked="on"/>
<w:lsdException w:name="Salutation" w:locked="on"/>
<w:lsdException w:name="Date" w:locked="on"/>
<w:lsdException w:name="Body Text First Indent" w:locked="on"/>
<w:lsdException w:name="Body Text First Indent 2" w:locked="on"/>
<w:lsdException w:name="Note Heading" w:locked="on"/>
<w:lsdException w:name="Body Text 2" w:locked="on"/>
<w:lsdException w:name="Body Text 3" w:locked="on"/>
<w:lsdException w:name="Body Text Indent 2" w:locked="on"/>
<w:lsdException w:name="Body Text Indent 3" w:locked="on"/>
<w:lsdException w:name="Block Text" w:locked="on"/>
<w:lsdException w:name="Hyperlink" w:locked="on"/>
<w:lsdException w:name="FollowedHyperlink" w:locked="on"/>
<w:lsdException w:name="Strong" w:locked="on"/>
<w:lsdException w:name="Emphasis" w:locked="on"/>
<w:lsdException w:name="Document Map" w:locked="on"/>
<w:lsdException w:name="Plain Text" w:locked="on"/>
<w:lsdException w:name="E-mail Signature" w:locked="on"/>
<w:lsdException w:name="HTML Top of Form" w:locked="on"/>
<w:lsdException w:name="HTML Bottom of Form" w:locked="on"/>
<w:lsdException w:name="Normal (Web)" w:locked="on"/>
<w:lsdException w:name="HTML Acronym" w:locked="on"/>
<w:lsdException w:name="HTML Address" w:locked="on"/>
<w:lsdException w:name="HTML Cite" w:locked="on"/>
<w:lsdException w:name="HTML Code" w:locked="on"/>
<w:lsdException w:name="HTML Definition" w:locked="on"/>
<w:lsdException w:name="HTML Keyboard" w:locked="on"/>
<w:lsdException w:name="HTML Preformatted" w:locked="on"/>
<w:lsdException w:name="HTML Sample" w:locked="on"/>
<w:lsdException w:name="HTML Typewriter" w:locked="on"/>
<w:lsdException w:name="HTML Variable" w:locked="on"/>
<w:lsdException w:name="Normal Table" w:locked="on"/>
<w:lsdException w:name="annotation subject" w:locked="on"/>
<w:lsdException w:name="No List" w:locked="on"/>
<w:lsdException w:name="Outline List 1" w:locked="on"/>
<w:lsdException w:name="Outline List 2" w:locked="on"/>
<w:lsdException w:name="Outline List 3" w:locked="on"/>
<w:lsdException w:name="Table Simple 1" w:locked="on"/>
<w:lsdException w:name="Table Simple 2" w:locked="on"/>
<w:lsdException w:name="Table Simple 3" w:locked="on"/>
<w:lsdException w:name="Table Classic 1" w:locked="on"/>
<w:lsdException w:name="Table Classic 2" w:locked="on"/>
<w:lsdException w:name="Table Classic 3" w:locked="on"/>
<w:lsdException w:name="Table Classic 4" w:locked="on"/>
<w:lsdException w:name="Table Colorful 1" w:locked="on"/>
<w:lsdException w:name="Table Colorful 2" w:locked="on"/>
<w:lsdException w:name="Table Colorful 3" w:locked="on"/>
<w:lsdException w:name="Table Columns 1" w:locked="on"/>
<w:lsdException w:name="Table Columns 2" w:locked="on"/>
<w:lsdException w:name="Table Columns 3" w:locked="on"/>
<w:lsdException w:name="Table Columns 4" w:locked="on"/>
<w:lsdException w:name="Table Columns 5" w:locked="on"/>
<w:lsdException w:name="Table Grid 1" w:locked="on"/>
<w:lsdException w:name="Table Grid 2" w:locked="on"/>
<w:lsdException w:name="Table Grid 3" w:locked="on"/>
<w:lsdException w:name="Table Grid 4" w:locked="on"/>
<w:lsdException w:name="Table Grid 5" w:locked="on"/>
<w:lsdException w:name="Table Grid 6" w:locked="on"/>
<w:lsdException w:name="Table Grid 7" w:locked="on"/>
<w:lsdException w:name="Table Grid 8" w:locked="on"/>
<w:lsdException w:name="Table List 1" w:locked="on"/>
<w:lsdException w:name="Table List 2" w:locked="on"/>
<w:lsdException w:name="Table List 3" w:locked="on"/>
<w:lsdException w:name="Table List 4" w:locked="on"/>
<w:lsdException w:name="Table List 5" w:locked="on"/>
<w:lsdException w:name="Table List 6" w:locked="on"/>
<w:lsdException w:name="Table List 7" w:locked="on"/>
<w:lsdException w:name="Table List 8" w:locked="on"/>
<w:lsdException w:name="Table 3D effects 1" w:locked="on"/>
<w:lsdException w:name="Table 3D effects 2" w:locked="on"/>
<w:lsdException w:name="Table 3D effects 3" w:locked="on"/>
<w:lsdException w:name="Table Contemporary" w:locked="on"/>
<w:lsdException w:name="Table Elegant" w:locked="on"/>
<w:lsdException w:name="Table Professional" w:locked="on"/>
<w:lsdException w:name="Table Subtle 1" w:locked="on"/>
<w:lsdException w:name="Table Subtle 2" w:locked="on"/>
<w:lsdException w:name="Table Web 1" w:locked="on"/>
<w:lsdException w:name="Table Web 2" w:locked="on"/>
<w:lsdException w:name="Table Web 3" w:locked="on"/>
<w:lsdException w:name="Balloon Text" w:locked="on"/>
<w:lsdException w:name="Table Grid" w:locked="on"/>
<w:lsdException w:name="Table Theme" w:locked="on"/>
<w:lsdException w:name="Placeholder Text"/>
<w:lsdException w:name="No Spacing"/>
<w:lsdException w:name="Light Shading"/>
<w:lsdException w:name="Light List"/>
<w:lsdException w:name="Light Grid"/>
<w:lsdException w:name="Medium Shading 1"/>
<w:lsdException w:name="Medium Shading 2"/>
<w:lsdException w:name="Medium List 1"/>
<w:lsdException w:name="Medium List 2"/>
<w:lsdException w:name="Medium Grid 1"/>
<w:lsdException w:name="Medium Grid 2"/>
<w:lsdException w:name="Medium Grid 3"/>
<w:lsdException w:name="Dark List"/>
<w:lsdException w:name="Colorful Shading"/>
<w:lsdException w:name="Colorful List"/>
<w:lsdException w:name="Colorful Grid"/>
<w:lsdException w:name="Light Shading Accent 1"/>
<w:lsdException w:name="Light List Accent 1"/>
<w:lsdException w:name="Light Grid Accent 1"/>
<w:lsdException w:name="Medium Shading 1 Accent 1"/>
<w:lsdException w:name="Medium Shading 2 Accent 1"/>
<w:lsdException w:name="Medium List 1 Accent 1"/>
<w:lsdException w:name="Revision"/>
<w:lsdException w:name="List Paragraph"/>
<w:lsdException w:name="Quote"/>
<w:lsdException w:name="Intense Quote"/>
<w:lsdException w:name="Medium List 2 Accent 1"/>
<w:lsdException w:name="Medium Grid 1 Accent 1"/>
<w:lsdException w:name="Medium Grid 2 Accent 1"/>
<w:lsdException w:name="Medium Grid 3 Accent 1"/>
<w:lsdException w:name="Dark List Accent 1"/>
<w:lsdException w:name="Colorful Shading Accent 1"/>
<w:lsdException w:name="Colorful List Accent 1"/>
<w:lsdException w:name="Colorful Grid Accent 1"/>
<w:lsdException w:name="Light Shading Accent 2"/>
<w:lsdException w:name="Light List Accent 2"/>
<w:lsdException w:name="Light Grid Accent 2"/>
<w:lsdException w:name="Medium Shading 1 Accent 2"/>
<w:lsdException w:name="Medium Shading 2 Accent 2"/>
<w:lsdException w:name="Medium List 1 Accent 2"/>
<w:lsdException w:name="Medium List 2 Accent 2"/>
<w:lsdException w:name="Medium Grid 1 Accent 2"/>
<w:lsdException w:name="Medium Grid 2 Accent 2"/>
<w:lsdException w:name="Medium Grid 3 Accent 2"/>
<w:lsdException w:name="Dark List Accent 2"/>
<w:lsdException w:name="Colorful Shading Accent 2"/>
<w:lsdException w:name="Colorful List Accent 2"/>
<w:lsdException w:name="Colorful Grid Accent 2"/>
<w:lsdException w:name="Light Shading Accent 3"/>
<w:lsdException w:name="Light List Accent 3"/>
<w:lsdException w:name="Light Grid Accent 3"/>
<w:lsdException w:name="Medium Shading 1 Accent 3"/>
<w:lsdException w:name="Medium Shading 2 Accent 3"/>
<w:lsdException w:name="Medium List 1 Accent 3"/>
<w:lsdException w:name="Medium List 2 Accent 3"/>
<w:lsdException w:name="Medium Grid 1 Accent 3"/>
<w:lsdException w:name="Medium Grid 2 Accent 3"/>
<w:lsdException w:name="Medium Grid 3 Accent 3"/>
<w:lsdException w:name="Dark List Accent 3"/>
<w:lsdException w:name="Colorful Shading Accent 3"/>
<w:lsdException w:name="Colorful List Accent 3"/>
<w:lsdException w:name="Colorful Grid Accent 3"/>
<w:lsdException w:name="Light Shading Accent 4"/>
<w:lsdException w:name="Light List Accent 4"/>
<w:lsdException w:name="Light Grid Accent 4"/>
<w:lsdException w:name="Medium Shading 1 Accent 4"/>
<w:lsdException w:name="Medium Shading 2 Accent 4"/>
<w:lsdException w:name="Medium List 1 Accent 4"/>
<w:lsdException w:name="Medium List 2 Accent 4"/>
<w:lsdException w:name="Medium Grid 1 Accent 4"/>
<w:lsdException w:name="Medium Grid 2 Accent 4"/>
<w:lsdException w:name="Medium Grid 3 Accent 4"/>
<w:lsdException w:name="Dark List Accent 4"/>
<w:lsdException w:name="Colorful Shading Accent 4"/>
<w:lsdException w:name="Colorful List Accent 4"/>
<w:lsdException w:name="Colorful Grid Accent 4"/>
<w:lsdException w:name="Light Shading Accent 5"/>
<w:lsdException w:name="Light List Accent 5"/>
<w:lsdException w:name="Light Grid Accent 5"/>
<w:lsdException w:name="Medium Shading 1 Accent 5"/>
<w:lsdException w:name="Medium Shading 2 Accent 5"/>
<w:lsdException w:name="Medium List 1 Accent 5"/>
<w:lsdException w:name="Medium List 2 Accent 5"/>
<w:lsdException w:name="Medium Grid 1 Accent 5"/>
<w:lsdException w:name="Medium Grid 2 Accent 5"/>
<w:lsdException w:name="Medium Grid 3 Accent 5"/>
<w:lsdException w:name="Dark List Accent 5"/>
<w:lsdException w:name="Colorful Shading Accent 5"/>
<w:lsdException w:name="Colorful List Accent 5"/>
<w:lsdException w:name="Colorful Grid Accent 5"/>
<w:lsdException w:name="Light Shading Accent 6"/>
<w:lsdException w:name="Light List Accent 6"/>
<w:lsdException w:name="Light Grid Accent 6"/>
<w:lsdException w:name="Medium Shading 1 Accent 6"/>
<w:lsdException w:name="Medium Shading 2 Accent 6"/>
<w:lsdException w:name="Medium List 1 Accent 6"/>
<w:lsdException w:name="Medium List 2 Accent 6"/>
<w:lsdException w:name="Medium Grid 1 Accent 6"/>
<w:lsdException w:name="Medium Grid 2 Accent 6"/>
<w:lsdException w:name="Medium Grid 3 Accent 6"/>
<w:lsdException w:name="Dark List Accent 6"/>
<w:lsdException w:name="Colorful Shading Accent 6"/>
<w:lsdException w:name="Colorful List Accent 6"/>
<w:lsdException w:name="Colorful Grid Accent 6"/>
<w:lsdException w:name="Subtle Emphasis"/>
<w:lsdException w:name="Intense Emphasis"/>
<w:lsdException w:name="Subtle Reference"/>
<w:lsdException w:name="Intense Reference"/>
<w:lsdException w:name="Book Title"/>
<w:lsdException w:name="Bibliography"/>
<w:lsdException w:name="TOC Heading"/>
<w:lsdException w:name="Plain Table 1"/>
<w:lsdException w:name="Plain Table 2"/>
<w:lsdException w:name="Plain Table 3"/>
<w:lsdException w:name="Plain Table 4"/>
<w:lsdException w:name="Plain Table 5"/>
<w:lsdException w:name="Grid Table Light"/>
<w:lsdException w:name="Grid Table 1 Light"/>
<w:lsdException w:name="Grid Table 2"/>
<w:lsdException w:name="Grid Table 3"/>
<w:lsdException w:name="Grid Table 4"/>
<w:lsdException w:name="Grid Table 5 Dark"/>
<w:lsdException w:name="Grid Table 6 Colorful"/>
<w:lsdException w:name="Grid Table 7 Colorful"/>
<w:lsdException w:name="Grid Table 1 Light Accent 1"/>
<w:lsdException w:name="Grid Table 2 Accent 1"/>
<w:lsdException w:name="Grid Table 3 Accent 1"/>
<w:lsdException w:name="Grid Table 4 Accent 1"/>
<w:lsdException w:name="Grid Table 5 Dark Accent 1"/>
<w:lsdException w:name="Grid Table 6 Colorful Accent 1"/>
<w:lsdException w:name="Grid Table 7 Colorful Accent 1"/>
<w:lsdException w:name="Grid Table 1 Light Accent 2"/>
<w:lsdException w:name="Grid Table 2 Accent 2"/>
<w:lsdException w:name="Grid Table 3 Accent 2"/>
<w:lsdException w:name="Grid Table 4 Accent 2"/>
<w:lsdException w:name="Grid Table 5 Dark Accent 2"/>
<w:lsdException w:name="Grid Table 6 Colorful Accent 2"/>
<w:lsdException w:name="Grid Table 7 Colorful Accent 2"/>
<w:lsdException w:name="Grid Table 1 Light Accent 3"/>
<w:lsdException w:name="Grid Table 2 Accent 3"/>
<w:lsdException w:name="Grid Table 3 Accent 3"/>
<w:lsdException w:name="Grid Table 4 Accent 3"/>
<w:lsdException w:name="Grid Table 5 Dark Accent 3"/>
<w:lsdException w:name="Grid Table 6 Colorful Accent 3"/>
<w:lsdException w:name="Grid Table 7 Colorful Accent 3"/>
<w:lsdException w:name="Grid Table 1 Light Accent 4"/>
<w:lsdException w:name="Grid Table 2 Accent 4"/>
<w:lsdException w:name="Grid Table 3 Accent 4"/>
<w:lsdException w:name="Grid Table 4 Accent 4"/>
<w:lsdException w:name="Grid Table 5 Dark Accent 4"/>
<w:lsdException w:name="Grid Table 6 Colorful Accent 4"/>
<w:lsdException w:name="Grid Table 7 Colorful Accent 4"/>
<w:lsdException w:name="Grid Table 1 Light Accent 5"/>
<w:lsdException w:name="Grid Table 2 Accent 5"/>
<w:lsdException w:name="Grid Table 3 Accent 5"/>
<w:lsdException w:name="Grid Table 4 Accent 5"/>
<w:lsdException w:name="Grid Table 5 Dark Accent 5"/>
<w:lsdException w:name="Grid Table 6 Colorful Accent 5"/>
<w:lsdException w:name="Grid Table 7 Colorful Accent 5"/>
<w:lsdException w:name="Grid Table 1 Light Accent 6"/>
<w:lsdException w:name="Grid Table 2 Accent 6"/>
<w:lsdException w:name="Grid Table 3 Accent 6"/>
<w:lsdException w:name="Grid Table 4 Accent 6"/>
<w:lsdException w:name="Grid Table 5 Dark Accent 6"/>
<w:lsdException w:name="Grid Table 6 Colorful Accent 6"/>
<w:lsdException w:name="Grid Table 7 Colorful Accent 6"/>
<w:lsdException w:name="List Table 1 Light"/>
<w:lsdException w:name="List Table 2"/>
<w:lsdException w:name="List Table 3"/>
<w:lsdException w:name="List Table 4"/>
<w:lsdException w:name="List Table 5 Dark"/>
<w:lsdException w:name="List Table 6 Colorful"/>
<w:lsdException w:name="List Table 7 Colorful"/>
<w:lsdException w:name="List Table 1 Light Accent 1"/>
<w:lsdException w:name="List Table 2 Accent 1"/>
<w:lsdException w:name="List Table 3 Accent 1"/>
<w:lsdException w:name="List Table 4 Accent 1"/>
<w:lsdException w:name="List Table 5 Dark Accent 1"/>
<w:lsdException w:name="List Table 6 Colorful Accent 1"/>
<w:lsdException w:name="List Table 7 Colorful Accent 1"/>
<w:lsdException w:name="List Table 1 Light Accent 2"/>
<w:lsdException w:name="List Table 2 Accent 2"/>
<w:lsdException w:name="List Table 3 Accent 2"/>
<w:lsdException w:name="List Table 4 Accent 2"/>
<w:lsdException w:name="List Table 5 Dark Accent 2"/>
<w:lsdException w:name="List Table 6 Colorful Accent 2"/>
<w:lsdException w:name="List Table 7 Colorful Accent 2"/>
<w:lsdException w:name="List Table 1 Light Accent 3"/>
<w:lsdException w:name="List Table 2 Accent 3"/>
<w:lsdException w:name="List Table 3 Accent 3"/>
<w:lsdException w:name="List Table 4 Accent 3"/>
<w:lsdException w:name="List Table 5 Dark Accent 3"/>
<w:lsdException w:name="List Table 6 Colorful Accent 3"/>
<w:lsdException w:name="List Table 7 Colorful Accent 3"/>
<w:lsdException w:name="List Table 1 Light Accent 4"/>
<w:lsdException w:name="List Table 2 Accent 4"/>
<w:lsdException w:name="List Table 3 Accent 4"/>
<w:lsdException w:name="List Table 4 Accent 4"/>
<w:lsdException w:name="List Table 5 Dark Accent 4"/>
<w:lsdException w:name="List Table 6 Colorful Accent 4"/>
<w:lsdException w:name="List Table 7 Colorful Accent 4"/>
<w:lsdException w:name="List Table 1 Light Accent 5"/>
<w:lsdException w:name="List Table 2 Accent 5"/>
<w:lsdException w:name="List Table 3 Accent 5"/>
<w:lsdException w:name="List Table 4 Accent 5"/>
<w:lsdException w:name="List Table 5 Dark Accent 5"/>
<w:lsdException w:name="List Table 6 Colorful Accent 5"/>
<w:lsdException w:name="List Table 7 Colorful Accent 5"/>
<w:lsdException w:name="List Table 1 Light Accent 6"/>
<w:lsdException w:name="List Table 2 Accent 6"/>
<w:lsdException w:name="List Table 3 Accent 6"/>
<w:lsdException w:name="List Table 4 Accent 6"/>
<w:lsdException w:name="List Table 5 Dark Accent 6"/>
<w:lsdException w:name="List Table 6 Colorful Accent 6"/>
<w:lsdException w:name="List Table 7 Colorful Accent 6"/>
</w:latentStyles>
<w:style w:type="paragraph" w:default="on" w:styleId="a">
<w:name w:val="Normal"/>
<wx:uiName wx:val="正文"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:widowControl w:val="off"/>
<w:jc w:val="both"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:kern w:val="2"/>
<w:sz w:val="21"/>
<w:sz-cs w:val="22"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="1">
<w:name w:val="heading 1"/>
<wx:uiName wx:val="标题 1"/>
<w:basedOn w:val="a"/>
<w:next w:val="a"/>
<w:link w:val="1Char"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:keepNext/>
<w:tabs>
<w:tab w:val="left" w:pos="1890"/>
</w:tabs>
<w:jc w:val="center"/>
<w:outlineLvl w:val="0"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:b-cs/>
<w:sz w:val="18"/>
<w:sz-cs w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="2">
<w:name w:val="heading 2"/>
<wx:uiName wx:val="标题 2"/>
<w:basedOn w:val="a"/>
<w:next w:val="a"/>
<w:link w:val="2Char"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:keepNext/>
<w:keepLines/>
<w:spacing w:before="260" w:after="260" w:line="416" w:line-rule="auto"/>
<w:outlineLvl w:val="1"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial" w:fareast="黑体" w:h-ansi="Arial"/>
<wx:font wx:val="Arial"/>
<w:b/>
<w:b-cs/>
<w:sz w:val="32"/>
<w:sz-cs w:val="32"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="3">
<w:name w:val="heading 3"/>
<wx:uiName wx:val="标题 3"/>
<w:basedOn w:val="a"/>
<w:next w:val="a"/>
<w:link w:val="3Char"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:keepNext/>
<w:keepLines/>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:outlineLvl w:val="2"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b-cs/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="4">
<w:name w:val="heading 4"/>
<wx:uiName wx:val="标题 4"/>
<w:basedOn w:val="a"/>
<w:next w:val="a"/>
<w:link w:val="4Char"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:keepNext/>
<w:keepLines/>
<w:widowControl/>
<w:outlineLvl w:val="3"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
<wx:font wx:val="Cambria"/>
<w:b/>
<w:kern w:val="0"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="5">
<w:name w:val="heading 5"/>
<wx:uiName wx:val="标题 5"/>
<w:basedOn w:val="a"/>
<w:link w:val="5Char"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:ind w:left="1400" w:hanging="400"/>
<w:outlineLvl w:val="4"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="6">
<w:name w:val="heading 6"/>
<wx:uiName wx:val="标题 6"/>
<w:basedOn w:val="a"/>
<w:link w:val="6Char"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:ind w:left="1600" w:hanging="400"/>
<w:outlineLvl w:val="5"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:b/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="7">
<w:name w:val="heading 7"/>
<wx:uiName wx:val="标题 7"/>
<w:basedOn w:val="a"/>
<w:link w:val="7Char"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:ind w:left="1800" w:hanging="400"/>
<w:outlineLvl w:val="6"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="8">
<w:name w:val="heading 8"/>
<wx:uiName wx:val="标题 8"/>
<w:basedOn w:val="a"/>
<w:link w:val="8Char"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:ind w:left="2000" w:hanging="400"/>
<w:outlineLvl w:val="7"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="9">
<w:name w:val="heading 9"/>
<wx:uiName wx:val="标题 9"/>
<w:basedOn w:val="a"/>
<w:link w:val="9Char"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:ind w:left="2200" w:hanging="400"/>
<w:outlineLvl w:val="8"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:default="on" w:styleId="a0">
<w:name w:val="Default Paragraph Font"/>
<wx:uiName wx:val="默认段落字体"/>
</w:style>
<w:style w:type="table" w:default="on" w:styleId="a1">
<w:name w:val="Normal Table"/>
<wx:uiName wx:val="普通表格"/>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
<w:tblPr>
<w:tblInd w:w="0" w:type="dxa"/>
<w:tblCellMar>
<w:top w:w="0" w:type="dxa"/>
<w:left w:w="108" w:type="dxa"/>
<w:bottom w:w="0" w:type="dxa"/>
<w:right w:w="108" w:type="dxa"/>
</w:tblCellMar>
</w:tblPr>
</w:style>
<w:style w:type="list" w:default="on" w:styleId="a2">
<w:name w:val="No List"/>
<wx:uiName wx:val="无列表"/>
</w:style>
<w:style w:type="character" w:styleId="1Char">
<w:name w:val="标题 1 Char"/>
<w:link w:val="1"/>
<w:locked/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:fareast="宋体" w:h-ansi="Times New Roman" w:cs="Times New Roman"/>
<w:b/>
<w:b-cs/>
<w:sz w:val="24"/>
<w:sz-cs w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="2Char">
<w:name w:val="标题 2 Char"/>
<w:link w:val="2"/>
<w:locked/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Arial" w:fareast="黑体" w:h-ansi="Arial" w:cs="Times New Roman"/>
<w:b/>
<w:b-cs/>
<w:sz w:val="32"/>
<w:sz-cs w:val="32"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="3Char">
<w:name w:val="标题 3 Char"/>
<w:link w:val="3"/>
<w:locked/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="Times New Roman" w:cs="Times New Roman"/>
<w:b-cs/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Heading4Char">
<w:name w:val="Heading 4 Char"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria" w:cs="Times New Roman"/>
<w:b/>
<w:w w:val="100"/>
<w:sz w:val="28"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="5Char">
<w:name w:val="标题 5 Char"/>
<w:link w:val="5"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
<w:sz w:val="21"/>
<w:sz-cs w:val="21"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="6Char">
<w:name w:val="标题 6 Char"/>
<w:link w:val="6"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
<w:b/>
<w:sz w:val="21"/>
<w:sz-cs w:val="21"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="7Char">
<w:name w:val="标题 7 Char"/>
<w:link w:val="7"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
<w:sz w:val="21"/>
<w:sz-cs w:val="21"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="8Char">
<w:name w:val="标题 8 Char"/>
<w:link w:val="8"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
<w:sz w:val="21"/>
<w:sz-cs w:val="21"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="9Char">
<w:name w:val="标题 9 Char"/>
<w:link w:val="9"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
<w:sz w:val="21"/>
<w:sz-cs w:val="21"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="BodyTextChar">
<w:name w:val="Body Text Char"/>
<w:locked/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:fareast="黑体" w:h-ansi="Times New Roman"/>
<w:sz w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="BalloonTextChar">
<w:name w:val="Balloon Text Char"/>
<w:locked/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<w:sz w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="BodyTextIndent3Char">
<w:name w:val="Body Text Indent 3 Char"/>
<w:locked/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<w:sz w:val="16"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="a3">
<w:name w:val="样式"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:widowControl w:val="off"/>
<w:jc w:val="both"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:kern w:val="2"/>
<w:sz w:val="21"/>
<w:sz-cs w:val="22"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="a4">
<w:name w:val="Strong"/>
<wx:uiName wx:val="要点"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:b/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="a5">
<w:name w:val="Hyperlink"/>
<wx:uiName wx:val="超链接"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:color w:val="0000FF"/>
<w:u w:val="single"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="a6">
<w:name w:val="page number"/>
<wx:uiName wx:val="页码"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="a7">
<w:name w:val="annotation reference"/>
<wx:uiName wx:val="批注引用"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:sz w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="clsparagraph11">
<w:name w:val="clsparagraph11"/>
<w:rsid w:val="001C4BCA"/>
</w:style>
<w:style w:type="character" w:styleId="BodyTextIndentChar">
<w:name w:val="Body Text Indent Char"/>
<w:locked/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<w:b/>
<w:sz w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="style161">
<w:name w:val="style161"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:sz w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="PlainTextChar">
<w:name w:val="Plain Text Char"/>
<w:locked/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="Courier New"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="FooterChar">
<w:name w:val="Footer Char"/>
<w:locked/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<w:sz w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="HeaderChar">
<w:name w:val="Header Char"/>
<w:locked/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:fareast="黑体" w:h-ansi="Times New Roman"/>
<w:sz w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="DateChar">
<w:name w:val="Date Char"/>
<w:locked/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<w:sz w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="BodyTextIndent2Char">
<w:name w:val="Body Text Indent 2 Char"/>
<w:locked/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<w:sz w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="DocumentMapChar">
<w:name w:val="Document Map Char"/>
<w:locked/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<w:sz w:val="24"/>
<w:shd w:val="clear" w:color="auto" w:fill="000080"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="BodyText2Char">
<w:name w:val="Body Text 2 Char"/>
<w:locked/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<w:sz w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="BodyText3Char">
<w:name w:val="Body Text 3 Char"/>
<w:locked/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:fareast="隶书" w:h-ansi="Times New Roman"/>
<w:sz w:val="15"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="bodytext1">
<w:name w:val="bodytext1"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Arial" w:h-ansi="Arial"/>
<w:color w:val="000000"/>
<w:sz w:val="14"/>
<w:u w:val="none"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="ourfont1">
<w:name w:val="ourfont1"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="宋体" w:h-ansi="宋体"/>
<w:sz w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="unnamed31">
<w:name w:val="unnamed31"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:sz w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="HTMLPreformattedChar">
<w:name w:val="HTML Preformatted Char"/>
<w:locked/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Courier New" w:h-ansi="Courier New"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="BodyTextFirstIndentChar">
<w:name w:val="Body Text First Indent Char"/>
<w:locked/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:fareast="黑体" w:h-ansi="Times New Roman"/>
<w:sz w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char1">
<w:name w:val="日期 Char1"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:fareast="宋体" w:h-ansi="Times New Roman"/>
<w:sz w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char">
<w:name w:val="批注文字 Char"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:sz w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char10">
<w:name w:val="批注文字 Char1"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:kern w:val="2"/>
<w:sz w:val="22"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CommentSubjectChar">
<w:name w:val="Comment Subject Char"/>
<w:locked/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:b/>
<w:sz w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char11">
<w:name w:val="批注主题 Char1"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:b/>
<w:kern w:val="2"/>
<w:sz w:val="22"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar">
<w:name w:val="Char Char"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:fareast="宋体"/>
<w:kern w:val="2"/>
<w:sz w:val="18"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar1">
<w:name w:val="Char Char1"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:fareast="宋体"/>
<w:kern w:val="2"/>
<w:sz w:val="18"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="a8">
<w:name w:val="Normal Indent"/>
<wx:uiName wx:val="正文缩进"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:ind w:first-line-chars="200" w:first-line="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:sz-cs w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="a9">
<w:name w:val="Document Map"/>
<wx:uiName wx:val="文档结构图"/>
<w:basedOn w:val="a"/>
<w:link w:val="Char0"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:shd w:val="clear" w:color="auto" w:fill="000080"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char0">
<w:name w:val="文档结构图 Char"/>
<w:link w:val="a9"/>
<w:locked/>
<w:rsid w:val="00B7502A"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:cs="Times New Roman"/>
<w:sz w:val="2"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char12">
<w:name w:val="文档结构图 Char1"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Microsoft YaHei UI" w:fareast="Microsoft YaHei UI" w:h-ansi="Calibri" w:cs="Times New Roman"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="aa">
<w:name w:val="annotation text"/>
<wx:uiName wx:val="批注文字"/>
<w:basedOn w:val="a"/>
<w:link w:val="Char2"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:jc w:val="left"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char2">
<w:name w:val="批注文字 Char2"/>
<w:link w:val="aa"/>
<w:locked/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="ab">
<w:name w:val="annotation subject"/>
<wx:uiName wx:val="批注主题"/>
<w:basedOn w:val="aa"/>
<w:next w:val="aa"/>
<w:link w:val="Char3"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:b/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char3">
<w:name w:val="批注主题 Char"/>
<w:link w:val="ab"/>
<w:locked/>
<w:rsid w:val="00B7502A"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
<w:b/>
<w:b-cs/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char20">
<w:name w:val="批注主题 Char2"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
<w:b/>
<w:b-cs/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="ac">
<w:name w:val="Body Text"/>
<wx:uiName wx:val="正文文本"/>
<w:basedOn w:val="a"/>
<w:link w:val="Char4"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:fareast="黑体" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char4">
<w:name w:val="正文文本 Char"/>
<w:link w:val="ac"/>
<w:locked/>
<w:rsid w:val="00B7502A"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char13">
<w:name w:val="正文文本 Char1"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="ad">
<w:name w:val="Body Text First Indent"/>
<wx:uiName wx:val="正文首行缩进"/>
<w:basedOn w:val="ac"/>
<w:link w:val="Char5"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:spacing w:after="120" w:line="240" w:line-rule="auto"/>
<w:ind w:first-line-chars="100" w:first-line="420"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char5">
<w:name w:val="正文首行缩进 Char"/>
<w:link w:val="ad"/>
<w:locked/>
<w:rsid w:val="00B7502A"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:fareast="黑体" w:h-ansi="Times New Roman" w:cs="Times New Roman"/>
<w:sz w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char14">
<w:name w:val="正文首行缩进 Char1"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="30">
<w:name w:val="Body Text 3"/>
<wx:uiName wx:val="正文文本 3"/>
<w:basedOn w:val="a"/>
<w:link w:val="3Char0"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:spacing w:line="160" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:fareast="隶书" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="0"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="3Char0">
<w:name w:val="正文文本 3 Char"/>
<w:link w:val="30"/>
<w:locked/>
<w:rsid w:val="00B7502A"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:sz w:val="16"/>
<w:sz-cs w:val="16"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="3Char1">
<w:name w:val="正文文本 3 Char1"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
<w:sz w:val="16"/>
<w:sz-cs w:val="16"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="ae">
<w:name w:val="Body Text Indent"/>
<wx:uiName wx:val="正文文本缩进"/>
<w:basedOn w:val="a"/>
<w:link w:val="Char6"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:spacing w:line="400" w:line-rule="at-least"/>
<w:ind w:first-line-chars="200" w:first-line="482"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char6">
<w:name w:val="正文文本缩进 Char"/>
<w:link w:val="ae"/>
<w:locked/>
<w:rsid w:val="00B7502A"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char15">
<w:name w:val="正文文本缩进 Char1"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="af">
<w:name w:val="Block Text"/>
<wx:uiName wx:val="文本块"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:spacing w:line="288" w:line-rule="auto"/>
<w:ind w:left="2" w:right="-96" w:hanging="2"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="HTML">
<w:name w:val="HTML Preformatted"/>
<wx:uiName wx:val="HTML 预设格式"/>
<w:basedOn w:val="a"/>
<w:link w:val="HTMLChar"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:tabs>
<w:tab w:val="left" w:pos="916"/>
<w:tab w:val="left" w:pos="1832"/>
<w:tab w:val="left" w:pos="2748"/>
<w:tab w:val="left" w:pos="3664"/>
<w:tab w:val="left" w:pos="4580"/>
<w:tab w:val="left" w:pos="5496"/>
<w:tab w:val="left" w:pos="6412"/>
<w:tab w:val="left" w:pos="7328"/>
<w:tab w:val="left" w:pos="8244"/>
<w:tab w:val="left" w:pos="9160"/>
<w:tab w:val="left" w:pos="10076"/>
<w:tab w:val="left" w:pos="10992"/>
<w:tab w:val="left" w:pos="11908"/>
<w:tab w:val="left" w:pos="12824"/>
<w:tab w:val="left" w:pos="13740"/>
<w:tab w:val="left" w:pos="14656"/>
</w:tabs>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Courier New" w:h-ansi="Courier New"/>
<wx:font wx:val="Courier New"/>
<w:kern w:val="0"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="HTMLChar">
<w:name w:val="HTML 预设格式 Char"/>
<w:link w:val="HTML"/>
<w:locked/>
<w:rsid w:val="00B7502A"/>
<w:rPr>
<w:rFonts w:ascii="Courier New" w:h-ansi="Courier New" w:cs="Courier New"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="HTMLChar1">
<w:name w:val="HTML 预设格式 Char1"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Courier New" w:fareast="宋体" w:h-ansi="Courier New" w:cs="Courier New"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="31">
<w:name w:val="toc 3"/>
<wx:uiName wx:val="目录 3"/>
<w:basedOn w:val="a"/>
<w:next w:val="a"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:spacing w:line="400" w:line-rule="exact"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:sz w:val="24"/>
<w:sz-cs w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="20">
<w:name w:val="Body Text 2"/>
<wx:uiName wx:val="正文文本 2"/>
<w:basedOn w:val="a"/>
<w:link w:val="2Char0"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="2Char0">
<w:name w:val="正文文本 2 Char"/>
<w:link w:val="20"/>
<w:locked/>
<w:rsid w:val="00B7502A"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="2Char1">
<w:name w:val="正文文本 2 Char1"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="10">
<w:name w:val="toc 1"/>
<wx:uiName wx:val="目录 1"/>
<w:basedOn w:val="a"/>
<w:next w:val="a"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:spacing w:before="120"/>
<w:jc w:val="left"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:b-cs/>
<w:i/>
<w:i-cs/>
<w:sz w:val="24"/>
<w:sz-cs w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="af0">
<w:name w:val="Normal (Web)"/>
<wx:uiName wx:val="普通(网站)"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:widowControl/>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="left"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
<w:sz-cs w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="af1">
<w:name w:val="Plain Text"/>
<wx:uiName wx:val="纯文本"/>
<w:basedOn w:val="a"/>
<w:link w:val="Char7"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="Courier New"/>
<wx:font wx:val="Courier New"/>
<w:kern w:val="0"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char7">
<w:name w:val="纯文本 Char"/>
<w:link w:val="af1"/>
<w:locked/>
<w:rsid w:val="00B7502A"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="Courier New" w:cs="Courier New"/>
<w:sz w:val="21"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char16">
<w:name w:val="纯文本 Char1"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="宋体" w:h-ansi="Courier New" w:cs="Courier New"/>
<w:sz w:val="21"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="af2">
<w:name w:val="Balloon Text"/>
<wx:uiName wx:val="批注框文本"/>
<w:basedOn w:val="a"/>
<w:link w:val="Char8"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char8">
<w:name w:val="批注框文本 Char"/>
<w:link w:val="af2"/>
<w:locked/>
<w:rsid w:val="00B7502A"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:sz w:val="2"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char17">
<w:name w:val="批注框文本 Char1"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="21">
<w:name w:val="toc 2"/>
<wx:uiName wx:val="目录 2"/>
<w:basedOn w:val="a"/>
<w:next w:val="a"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:spacing w:before="120"/>
<w:ind w:left="210"/>
<w:jc w:val="left"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:b-cs/>
<w:sz w:val="22"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="32">
<w:name w:val="Body Text Indent 3"/>
<wx:uiName wx:val="正文文本缩进 3"/>
<w:basedOn w:val="a"/>
<w:link w:val="3Char2"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:spacing w:after="120"/>
<w:ind w:left-chars="200" w:left="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="0"/>
<w:sz w:val="16"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="3Char2">
<w:name w:val="正文文本缩进 3 Char"/>
<w:link w:val="32"/>
<w:locked/>
<w:rsid w:val="00B7502A"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:sz w:val="16"/>
<w:sz-cs w:val="16"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="3Char10">
<w:name w:val="正文文本缩进 3 Char1"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
<w:sz w:val="16"/>
<w:sz-cs w:val="16"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="af3">
<w:name w:val="header"/>
<wx:uiName wx:val="页眉"/>
<w:basedOn w:val="a"/>
<w:link w:val="Char9"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:pBdr>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="1" w:color="auto"/>
</w:pBdr>
<w:tabs>
<w:tab w:val="center" w:pos="4153"/>
<w:tab w:val="right" w:pos="8306"/>
</w:tabs>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="360" w:line-rule="at-least"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:fareast="黑体" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char9">
<w:name w:val="页眉 Char"/>
<w:link w:val="af3"/>
<w:locked/>
<w:rsid w:val="00B7502A"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char18">
<w:name w:val="页眉 Char1"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="af4">
<w:name w:val="Date"/>
<wx:uiName wx:val="日期"/>
<w:basedOn w:val="a"/>
<w:next w:val="a"/>
<w:link w:val="Chara"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:ind w:left-chars="2500" w:left="100"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Chara">
<w:name w:val="日期 Char"/>
<w:link w:val="af4"/>
<w:locked/>
<w:rsid w:val="00B7502A"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char21">
<w:name w:val="日期 Char2"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="af5">
<w:name w:val="footer"/>
<wx:uiName wx:val="页脚"/>
<w:basedOn w:val="a"/>
<w:link w:val="Charb"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:tabs>
<w:tab w:val="center" w:pos="4153"/>
<w:tab w:val="right" w:pos="8306"/>
</w:tabs>
<w:snapToGrid w:val="off"/>
<w:jc w:val="left"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Charb">
<w:name w:val="页脚 Char"/>
<w:link w:val="af5"/>
<w:locked/>
<w:rsid w:val="00B7502A"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char19">
<w:name w:val="页脚 Char1"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="22">
<w:name w:val="Body Text Indent 2"/>
<wx:uiName wx:val="正文文本缩进 2"/>
<w:basedOn w:val="a"/>
<w:link w:val="2Char2"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:spacing w:line="400" w:line-rule="at-least"/>
<w:ind w:first-line="480"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="2Char2">
<w:name w:val="正文文本缩进 2 Char"/>
<w:link w:val="22"/>
<w:locked/>
<w:rsid w:val="00B7502A"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="2Char10">
<w:name w:val="正文文本缩进 2 Char1"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="af6">
<w:name w:val="标准"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:spacing w:before="120" w:after="120" w:line="312" w:line-rule="at-least"/>
<w:textAlignment w:val="baseline"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="33">
<w:name w:val="样式3"/>
<w:basedOn w:val="23"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:pBdr>
<w:top w:val="single" w:sz="8" wx:bdrwidth="20" w:space="2" w:color="auto"/>
</w:pBdr>
</w:pPr>
<w:rPr>
<wx:font wx:val="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="Charc">
<w:name w:val="Char"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:widowControl/>
<w:spacing w:after="160" w:line="240" w:line-rule="exact"/>
<w:jc w:val="left"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Verdana"/>
<wx:font wx:val="Arial"/>
<w:b/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
<w:sz-cs w:val="24"/>
<w:lang w:fareast="EN-US"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="11">
<w:name w:val="1"/>
<w:basedOn w:val="a"/>
<w:next w:val="ac"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:fareast="黑体" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:sz w:val="16"/>
<w:sz-cs w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl24">
<w:name w:val="xl24"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:widowControl/>
<w:pBdr>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="CharCharCharCharCharChar1Char">
<w:name w:val="Char Char Char Char Char Char1 Char"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:widowControl/>
<w:spacing w:after="160" w:line="240" w:line-rule="exact"/>
<w:jc w:val="left"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Verdana"/>
<wx:font wx:val="Arial"/>
<w:b/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
<w:sz-cs w:val="24"/>
<w:lang w:fareast="EN-US"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="50">
<w:name w:val="样式5"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:spacing w:before="50" w:after="50" w:line="360" w:line-rule="exact"/>
<w:ind w:first-line-chars="200" w:first-line="200"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:sz w:val="24"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="24">
<w:name w:val="2"/>
<w:basedOn w:val="a"/>
<w:next w:val="ac"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="90">
<w:name w:val="样式9"/>
<w:basedOn w:val="3"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<wx:font wx:val="Times New Roman"/>
<w:sz-cs w:val="32"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="60">
<w:name w:val="样式6"/>
<w:basedOn w:val="3"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<wx:font wx:val="Times New Roman"/>
<w:b/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl25">
<w:name w:val="xl25"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:widowControl/>
<w:pBdr>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
<w:textAlignment w:val="top"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="23">
<w:name w:val="样式2"/>
<w:basedOn w:val="af5"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="1" w:color="auto"/>
</w:pBdr>
<w:ind w:right="360"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="80">
<w:name w:val="样式8"/>
<w:basedOn w:val="2"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on" w:line="240" w:line-rule="auto"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="黑体"/>
<wx:font wx:val="Arial"/>
<w:b w:val="off"/>
<w:sz w:val="30"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="100">
<w:name w:val="样式10"/>
<w:basedOn w:val="2"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on" w:line="240" w:line-rule="auto"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="黑体"/>
<wx:font wx:val="Arial"/>
<w:b w:val="off"/>
<w:sz w:val="30"/>
</w:rPr>
</w:style>
<w:style w:type="table" w:styleId="51">
<w:name w:val="Table Grid 5"/>
<wx:uiName wx:val="网格型 5"/>
<w:basedOn w:val="a1"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:widowControl w:val="off"/>
<w:jc w:val="both"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
</w:rPr>
<w:tblPr>
<w:tblInd w:w="0" w:type="dxa"/>
<w:tblBorders>
<w:top w:val="single" w:sz="12" wx:bdrwidth="30" w:space="0" w:color="000000"/>
<w:left w:val="single" w:sz="12" wx:bdrwidth="30" w:space="0" w:color="000000"/>
<w:bottom w:val="single" w:sz="12" wx:bdrwidth="30" w:space="0" w:color="000000"/>
<w:right w:val="single" w:sz="12" wx:bdrwidth="30" w:space="0" w:color="000000"/>
<w:insideH w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="000000"/>
<w:insideV w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="000000"/>
</w:tblBorders>
<w:tblCellMar>
<w:top w:w="0" w:type="dxa"/>
<w:left w:w="108" w:type="dxa"/>
<w:bottom w:w="0" w:type="dxa"/>
<w:right w:w="108" w:type="dxa"/>
</w:tblCellMar>
</w:tblPr>
<w:tblStylePr w:type="firstRow">
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
</w:rPr>
<w:tblPr/>
<w:tcPr>
<w:tcBorders>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="12" wx:bdrwidth="30" w:space="0" w:color="000000"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:insideH w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:insideV w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:tl2br w:val="nil"/>
<w:tr2bl w:val="nil"/>
</w:tcBorders>
</w:tcPr>
</w:tblStylePr>
<w:tblStylePr w:type="lastRow">
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:b-cs/>
</w:rPr>
<w:tblPr/>
<w:tcPr>
<w:tcBorders>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:insideH w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:insideV w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:tl2br w:val="nil"/>
<w:tr2bl w:val="nil"/>
</w:tcBorders>
</w:tcPr>
</w:tblStylePr>
<w:tblStylePr w:type="lastCol">
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:b-cs/>
</w:rPr>
<w:tblPr/>
<w:tcPr>
<w:tcBorders>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:insideH w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:insideV w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:tl2br w:val="nil"/>
<w:tr2bl w:val="nil"/>
</w:tcBorders>
</w:tcPr>
</w:tblStylePr>
<w:tblStylePr w:type="nwCell">
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
</w:rPr>
<w:tblPr/>
<w:tcPr>
<w:tcBorders>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:insideH w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:insideV w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:tl2br w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="000000"/>
<w:tr2bl w:val="nil"/>
</w:tcBorders>
</w:tcPr>
</w:tblStylePr>
</w:style>
<w:style w:type="table" w:styleId="af7">
<w:name w:val="Table Grid"/>
<wx:uiName wx:val="网格型"/>
<w:basedOn w:val="a1"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:widowControl w:val="off"/>
<w:jc w:val="both"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
</w:rPr>
<w:tblPr>
<w:tblInd w:w="0" w:type="dxa"/>
<w:tblBorders>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:tblBorders>
<w:tblCellMar>
<w:top w:w="0" w:type="dxa"/>
<w:left w:w="108" w:type="dxa"/>
<w:bottom w:w="0" w:type="dxa"/>
<w:right w:w="108" w:type="dxa"/>
</w:tblCellMar>
</w:tblPr>
</w:style>
<w:style w:type="paragraph" w:styleId="af8">
<w:name w:val="List Paragraph"/>
<wx:uiName wx:val="列出段落"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="001C4BCA"/>
<w:pPr>
<w:ind w:first-line-chars="200" w:first-line="420"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="af9">
<w:name w:val="FollowedHyperlink"/>
<wx:uiName wx:val="访问过的超链接"/>
<w:rsid w:val="001C4BCA"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:color w:val="954F72"/>
<w:u w:val="single"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Heading2Char1">
<w:name w:val="Heading 2 Char1"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria" w:cs="Times New Roman"/>
<w:b/>
<w:kern w:val="0"/>
<w:sz w:val="32"/>
<w:sz-cs w:val="32"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="4Char">
<w:name w:val="标题 4 Char"/>
<w:link w:val="4"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria" w:cs="Times New Roman"/>
<w:b/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="vsbcontentend">
<w:name w:val="vsbcontent_end"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="left"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体"/>
<wx:font wx:val="宋体"/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
<w:sz-cs w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="afa">
<w:name w:val="Emphasis"/>
<wx:uiName wx:val="强调"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:i/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="afb">
<w:name w:val="No Spacing"/>
<wx:uiName wx:val="无间隔"/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:jc w:val="both"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:sz w:val="21"/>
<w:sz-cs w:val="21"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="afc">
<w:name w:val="Title"/>
<wx:uiName wx:val="标题"/>
<w:basedOn w:val="a"/>
<w:next w:val="a"/>
<w:link w:val="Chard"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="黑体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:kern w:val="0"/>
<w:sz w:val="32"/>
<w:sz-cs w:val="32"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Chard">
<w:name w:val="标题 Char"/>
<w:link w:val="afc"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="黑体" w:h-ansi="宋体" w:cs="Times New Roman"/>
<w:sz w:val="32"/>
<w:sz-cs w:val="32"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="afd">
<w:name w:val="Subtitle"/>
<wx:uiName wx:val="副标题"/>
<w:basedOn w:val="a"/>
<w:next w:val="a"/>
<w:link w:val="Chare"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Cambria" w:fareast="黑体" w:h-ansi="Cambria"/>
<wx:font wx:val="Cambria"/>
<w:b/>
<w:kern w:val="0"/>
<w:sz w:val="32"/>
<w:sz-cs w:val="32"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Chare">
<w:name w:val="副标题 Char"/>
<w:link w:val="afd"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="Cambria" w:fareast="黑体" w:h-ansi="Cambria" w:cs="Times New Roman"/>
<w:b/>
<w:sz w:val="32"/>
<w:sz-cs w:val="32"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="afe">
<w:name w:val="Subtle Emphasis"/>
<wx:uiName wx:val="不明显强调"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:i/>
<w:color w:val="404040"/>
<w:w w:val="100"/>
<w:sz w:val="21"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="aff">
<w:name w:val="Intense Emphasis"/>
<wx:uiName wx:val="明显强调"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:i/>
<w:color w:val="5B9BD5"/>
<w:w w:val="100"/>
<w:sz w:val="21"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="aff0">
<w:name w:val="Quote"/>
<wx:uiName wx:val="引用"/>
<w:basedOn w:val="a"/>
<w:link w:val="Charf"/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:ind w:left="864" w:right="864"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:i/>
<w:color w:val="404040"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Charf">
<w:name w:val="引用 Char"/>
<w:link w:val="aff0"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
<w:i/>
<w:color w:val="404040"/>
<w:sz w:val="21"/>
<w:sz-cs w:val="21"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="aff1">
<w:name w:val="Intense Quote"/>
<wx:uiName wx:val="明显引用"/>
<w:basedOn w:val="a"/>
<w:link w:val="Charf0"/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:ind w:left="950" w:right="950"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:i/>
<w:color w:val="5B9BD5"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Charf0">
<w:name w:val="明显引用 Char"/>
<w:link w:val="aff1"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
<w:i/>
<w:color w:val="5B9BD5"/>
<w:sz w:val="21"/>
<w:sz-cs w:val="21"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="aff2">
<w:name w:val="Subtle Reference"/>
<wx:uiName wx:val="不明显参考"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:smallCaps/>
<w:color w:val="5A5A5A"/>
<w:w w:val="100"/>
<w:sz w:val="21"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="aff3">
<w:name w:val="Intense Reference"/>
<wx:uiName wx:val="明显参考"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:b/>
<w:smallCaps/>
<w:color w:val="5B9BD5"/>
<w:w w:val="100"/>
<w:sz w:val="21"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="aff4">
<w:name w:val="Book Title"/>
<wx:uiName wx:val="书籍标题"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:b/>
<w:i/>
<w:w w:val="100"/>
<w:sz w:val="21"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="TOC">
<w:name w:val="TOC Heading"/>
<wx:uiName wx:val="TOC 标题"/>
<w:basedOn w:val="1"/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:keepNext w:val="off"/>
<w:widowControl/>
<w:tabs>
<w:tab w:val="clear" w:pos="1890"/>
</w:tabs>
<w:jc w:val="left"/>
<w:outlineLvl w:val="9"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:h-ansi="Calibri"/>
<wx:font wx:val="Calibri"/>
<w:b w:val="off"/>
<w:b-cs w:val="off"/>
<w:color w:val="2E74B5"/>
<w:kern w:val="0"/>
<w:sz w:val="32"/>
<w:sz-cs w:val="32"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="40">
<w:name w:val="toc 4"/>
<wx:uiName wx:val="目录 4"/>
<w:basedOn w:val="a"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:ind w:left="1275"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="52">
<w:name w:val="toc 5"/>
<wx:uiName wx:val="目录 5"/>
<w:basedOn w:val="a"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:ind w:left="1700"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="61">
<w:name w:val="toc 6"/>
<wx:uiName wx:val="目录 6"/>
<w:basedOn w:val="a"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:ind w:left="2125"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="70">
<w:name w:val="toc 7"/>
<wx:uiName wx:val="目录 7"/>
<w:basedOn w:val="a"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:ind w:left="2550"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="81">
<w:name w:val="toc 8"/>
<wx:uiName wx:val="目录 8"/>
<w:basedOn w:val="a"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:ind w:left="2975"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="91">
<w:name w:val="toc 9"/>
<wx:uiName wx:val="目录 9"/>
<w:basedOn w:val="a"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:ind w:left="3400"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="3Char11">
<w:name w:val="标题 3 Char1"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<w:b/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="font01">
<w:name w:val="font01"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="宋体" w:h-ansi="宋体"/>
<w:color w:val="000000"/>
<w:w w:val="100"/>
<w:sz w:val="21"/>
<w:u w:val="none"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="HTML0">
<w:name w:val="HTML Cite"/>
<wx:uiName wx:val="HTML 引文"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="宋体" w:cs="Times New Roman"/>
<w:w w:val="100"/>
<w:sz w:val="24"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="font11">
<w:name w:val="font11"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="宋体" w:h-ansi="宋体"/>
<w:color w:val="000000"/>
<w:w w:val="100"/>
<w:sz w:val="18"/>
<w:u w:val="none"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="a161">
<w:name w:val="a161"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:color w:val="000000"/>
<w:w w:val="100"/>
<w:sz w:val="21"/>
<w:u w:val="none"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="12">
<w:name w:val="纯文本1"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="0"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="content">
<w:name w:val="content"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="_x000B__x000C_" w:h-ansi="_x000B__x000C_"/>
<wx:font wx:val="_x000B__x000C_"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="13">
<w:name w:val="列出段落1"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:ind w:first-line="420"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="Style1">
<w:name w:val="_Style 1"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:ind w:first-line="420"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:kern w:val="0"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="PlainText1">
<w:name w:val="Plain Text1"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="0"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="aff5">
<w:name w:val="一览表格"/>
<w:basedOn w:val="a"/>
<w:next w:val="a"/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="110">
<w:name w:val="列出段落11"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:ind w:first-line="420"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="Style2">
<w:name w:val="_Style 2"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:ind w:first-line="420"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:kern w:val="0"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="111">
<w:name w:val="纯文本11"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="0"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="ListParagraph1">
<w:name w:val="List Paragraph1"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:ind w:first-line="420"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Calibri"/>
<w:kern w:val="0"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="PlainText2">
<w:name w:val="Plain Text2"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="0"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="Style4">
<w:name w:val="_Style 4"/>
<w:basedOn w:val="1"/>
<w:next w:val="a"/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:keepLines/>
<w:widowControl/>
<w:tabs>
<w:tab w:val="clear" w:pos="1890"/>
</w:tabs>
<w:jc w:val="both"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
<wx:font wx:val="Cambria"/>
<w:b-cs w:val="off"/>
<w:color w:val="365F91"/>
<w:kern w:val="0"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar7">
<w:name w:val="Char Char7"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:fareast="宋体" w:h-ansi="Times New Roman"/>
<w:b/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar5">
<w:name w:val="Char Char5"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="宋体"/>
<w:w w:val="100"/>
<w:sz w:val="18"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar4">
<w:name w:val="Char Char4"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="宋体"/>
<w:w w:val="100"/>
<w:sz w:val="18"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar9">
<w:name w:val="Char Char9"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="宋体"/>
<w:b/>
<w:w w:val="100"/>
<w:sz w:val="48"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar3">
<w:name w:val="Char Char3"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="宋体"/>
<w:w w:val="100"/>
<w:sz w:val="20"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="SubtitleChar1">
<w:name w:val="Subtitle Char1"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="Cambria" w:fareast="黑体" w:h-ansi="Cambria"/>
<w:b/>
<w:w w:val="100"/>
<w:sz w:val="32"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="TitleChar1">
<w:name w:val="Title Char1"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="黑体" w:h-ansi="宋体"/>
<w:w w:val="100"/>
<w:sz w:val="32"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="NormalWebCharChar">
<w:name w:val="Normal (Web) Char Char"/>
<w:link w:val="NormalWeb1"/>
<w:locked/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="宋体"/>
<w:color w:val="000000"/>
<w:sz w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="NormalWeb1">
<w:name w:val="Normal (Web)1"/>
<w:basedOn w:val="a"/>
<w:link w:val="NormalWebCharChar"/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="Calibri"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char1a">
<w:name w:val="副标题 Char1"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
<w:b/>
<w:w w:val="100"/>
<w:sz w:val="32"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char1b">
<w:name w:val="标题 Char1"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
<w:b/>
<w:w w:val="100"/>
<w:sz w:val="32"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="aff6">
<w:name w:val="纯文本 字符"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="Courier New"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="high-light-bg4">
<w:name w:val="high-light-bg4"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="25">
<w:name w:val="纯文本2"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="000D0237"/>
<w:pPr>
<w:widowControl/>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:after="200"/>
<w:jc w:val="left"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="Courier New"/>
<wx:font wx:val="Courier New"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="PlainText11">
<w:name w:val="Plain Text11"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="Courier New" w:cs="宋体"/>
<wx:font wx:val="Courier New"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="aff7">
<w:name w:val="正文文本缩进 字符"/>
<w:rsid w:val="000D0237"/>
<w:rPr>
<w:rFonts w:fareast="仿宋_GB2312"/>
<w:kern w:val="2"/>
<w:sz w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar0">
<w:name w:val="Char Char"/>
<w:rsid w:val="00E8665A"/>
<w:rPr>
<w:rFonts w:fareast="宋体"/>
<w:kern w:val="2"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar10">
<w:name w:val="Char Char1"/>
<w:rsid w:val="00E8665A"/>
<w:rPr>
<w:rFonts w:fareast="宋体"/>
<w:kern w:val="2"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="Charf1">
<w:name w:val="Char"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00E8665A"/>
<w:pPr>
<w:widowControl/>
<w:spacing w:after="160" w:line="240" w:line-rule="exact"/>
<w:jc w:val="left"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial" w:fareast="Times New Roman" w:h-ansi="Arial" w:cs="Verdana"/>
<wx:font wx:val="Arial"/>
<w:b/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
<w:sz-cs w:val="24"/>
<w:lang w:fareast="EN-US"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="CharCharCharCharCharChar1Char0">
<w:name w:val="Char Char Char Char Char Char1 Char"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00E8665A"/>
<w:pPr>
<w:widowControl/>
<w:spacing w:after="160" w:line="240" w:line-rule="exact"/>
<w:jc w:val="left"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial" w:fareast="Times New Roman" w:h-ansi="Arial" w:cs="Verdana"/>
<wx:font wx:val="Arial"/>
<w:b/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
<w:sz-cs w:val="24"/>
<w:lang w:fareast="EN-US"/>
</w:rPr>
</w:style>
</w:styles>
<w:shapeDefaults>
<o:shapedefaults v:ext="edit" spidmax="2049"/>
<o:shapelayout v:ext="edit">
<o:idmap v:ext="edit" data="1"/>
</o:shapelayout>
</w:shapeDefaults>
<w:docPr>
<w:view w:val="print"/>
<w:zoom w:percent="190"/>
<w:doNotEmbedSystemFonts/>
<w:bordersDontSurroundHeader/>
<w:bordersDontSurroundFooter/>
<w:defaultTabStop w:val="420"/>
<w:drawingGridHorizontalSpacing w:val="105"/>
<w:drawingGridVerticalSpacing w:val="156"/>
<w:displayHorizontalDrawingGridEvery w:val="0"/>
<w:displayVerticalDrawingGridEvery w:val="2"/>
<w:punctuationKerning/>
<w:characterSpacingControl w:val="CompressPunctuation"/>
<w:noLineBreaksAfter w:lang="ZH-CN" w:val="$([{£¥·‘“〈《「『【〔〖〝﹙﹛﹝＄（．［｛￡￥"/>
<w:noLineBreaksBefore w:lang="ZH-CN" w:val="!%),.:;&gt;?]}¢¨°·ˇˉ―‖’”…‰′″›℃∶、。〃〉》」』】〕〗〞︶︺︾﹀﹄﹚﹜﹞！＂％＇），．：；？］｀｜｝～￠"/>
<w:optimizeForBrowser/>
<w:relyOnVML/>
<w:allowPNG/>
<w:validateAgainstSchema/>
<w:saveInvalidXML w:val="off"/>
<w:ignoreMixedContent w:val="off"/>
<w:alwaysShowPlaceholderText w:val="off"/>
<w:hdrShapeDefaults>
<o:shapedefaults v:ext="edit" spidmax="2049"/>
</w:hdrShapeDefaults>
<w:footnotePr>
<w:footnote w:type="separator">
<w:p wsp:rsidR="00364AF9" wsp:rsidRDefault="00364AF9">
<w:r>
<w:separator/>
</w:r>
</w:p>
</w:footnote>
<w:footnote w:type="continuation-separator">
<w:p wsp:rsidR="00364AF9" wsp:rsidRDefault="00364AF9">
<w:r>
<w:continuationSeparator/>
</w:r>
</w:p>
</w:footnote>
</w:footnotePr>
<w:endnotePr>
<w:endnote w:type="separator">
<w:p wsp:rsidR="00364AF9" wsp:rsidRDefault="00364AF9">
<w:r>
<w:separator/>
</w:r>
</w:p>
</w:endnote>
<w:endnote w:type="continuation-separator">
<w:p wsp:rsidR="00364AF9" wsp:rsidRDefault="00364AF9">
<w:r>
<w:continuationSeparator/>
</w:r>
</w:p>
</w:endnote>
</w:endnotePr>
<w:compat>
<w:spaceForUL/>
<w:balanceSingleByteDoubleByteWidth/>
<w:doNotLeaveBackslashAlone/>
<w:ulTrailSpace/>
<w:doNotExpandShiftReturn/>
<w:adjustLineHeightInTable/>
<w:breakWrappedTables/>
<w:snapToGridInCell/>
<w:wrapTextWithPunct/>
<w:useAsianBreakRules/>
<w:dontGrowAutofit/>
<w:useFELayout/>
</w:compat>
<wsp:rsids>
<wsp:rsidRoot wsp:val="001C4BCA"/>
<wsp:rsid wsp:val="00001300"/>
<wsp:rsid wsp:val="00004E26"/>
<wsp:rsid wsp:val="00006E7F"/>
<wsp:rsid wsp:val="00007C68"/>
<wsp:rsid wsp:val="00010C04"/>
<wsp:rsid wsp:val="00012EF8"/>
<wsp:rsid wsp:val="000167BC"/>
<wsp:rsid wsp:val="00016AFA"/>
<wsp:rsid wsp:val="0002053C"/>
<wsp:rsid wsp:val="00021EF3"/>
<wsp:rsid wsp:val="00024F04"/>
<wsp:rsid wsp:val="000352C6"/>
<wsp:rsid wsp:val="00036FC5"/>
<wsp:rsid wsp:val="00042A40"/>
<wsp:rsid wsp:val="00060F6B"/>
<wsp:rsid wsp:val="0006128A"/>
<wsp:rsid wsp:val="0006265D"/>
<wsp:rsid wsp:val="00062974"/>
<wsp:rsid wsp:val="000722A3"/>
<wsp:rsid wsp:val="000723E9"/>
<wsp:rsid wsp:val="00082D69"/>
<wsp:rsid wsp:val="000979E7"/>
<wsp:rsid wsp:val="000A10C6"/>
<wsp:rsid wsp:val="000C7596"/>
<wsp:rsid wsp:val="000D0237"/>
<wsp:rsid wsp:val="000D3380"/>
<wsp:rsid wsp:val="000D3C1B"/>
<wsp:rsid wsp:val="000E3177"/>
<wsp:rsid wsp:val="000E4D45"/>
<wsp:rsid wsp:val="000E5386"/>
<wsp:rsid wsp:val="000F0676"/>
<wsp:rsid wsp:val="000F069D"/>
<wsp:rsid wsp:val="000F31B3"/>
<wsp:rsid wsp:val="00101B58"/>
<wsp:rsid wsp:val="00102892"/>
<wsp:rsid wsp:val="001105AF"/>
<wsp:rsid wsp:val="00123256"/>
<wsp:rsid wsp:val="001264B9"/>
<wsp:rsid wsp:val="00127F74"/>
<wsp:rsid wsp:val="00137B98"/>
<wsp:rsid wsp:val="00137DD6"/>
<wsp:rsid wsp:val="00144386"/>
<wsp:rsid wsp:val="0014469E"/>
<wsp:rsid wsp:val="001448BB"/>
<wsp:rsid wsp:val="00156642"/>
<wsp:rsid wsp:val="00161CDF"/>
<wsp:rsid wsp:val="00171624"/>
<wsp:rsid wsp:val="00180B4C"/>
<wsp:rsid wsp:val="00184DB2"/>
<wsp:rsid wsp:val="00191EFF"/>
<wsp:rsid wsp:val="00193FEA"/>
<wsp:rsid wsp:val="001A0D4E"/>
<wsp:rsid wsp:val="001A2615"/>
<wsp:rsid wsp:val="001B0AB6"/>
<wsp:rsid wsp:val="001B3FA6"/>
<wsp:rsid wsp:val="001B7B65"/>
<wsp:rsid wsp:val="001C4BCA"/>
<wsp:rsid wsp:val="001D7799"/>
<wsp:rsid wsp:val="001E7D12"/>
<wsp:rsid wsp:val="001F473C"/>
<wsp:rsid wsp:val="001F58A9"/>
<wsp:rsid wsp:val="001F70E4"/>
<wsp:rsid wsp:val="002241D1"/>
<wsp:rsid wsp:val="00226FC8"/>
<wsp:rsid wsp:val="002348BA"/>
<wsp:rsid wsp:val="002369C9"/>
<wsp:rsid wsp:val="00265E03"/>
<wsp:rsid wsp:val="00270883"/>
<wsp:rsid wsp:val="00270AFC"/>
<wsp:rsid wsp:val="002727CF"/>
<wsp:rsid wsp:val="00281464"/>
<wsp:rsid wsp:val="00292C4E"/>
<wsp:rsid wsp:val="002955CB"/>
<wsp:rsid wsp:val="00295A7A"/>
<wsp:rsid wsp:val="002A156F"/>
<wsp:rsid wsp:val="002C393A"/>
<wsp:rsid wsp:val="002F43C6"/>
<wsp:rsid wsp:val="00301C4B"/>
<wsp:rsid wsp:val="003028B4"/>
<wsp:rsid wsp:val="003128AF"/>
<wsp:rsid wsp:val="00315A7F"/>
<wsp:rsid wsp:val="00320651"/>
<wsp:rsid wsp:val="003213C0"/>
<wsp:rsid wsp:val="00333906"/>
<wsp:rsid wsp:val="003341E4"/>
<wsp:rsid wsp:val="00340982"/>
<wsp:rsid wsp:val="00341097"/>
<wsp:rsid wsp:val="003440E1"/>
<wsp:rsid wsp:val="0035015A"/>
<wsp:rsid wsp:val="00352C6B"/>
<wsp:rsid wsp:val="00364AF9"/>
<wsp:rsid wsp:val="00367244"/>
<wsp:rsid wsp:val="0037165F"/>
<wsp:rsid wsp:val="00381BF7"/>
<wsp:rsid wsp:val="00386DC5"/>
<wsp:rsid wsp:val="003879F2"/>
<wsp:rsid wsp:val="003A57DE"/>
<wsp:rsid wsp:val="003A6600"/>
<wsp:rsid wsp:val="003B5BC2"/>
<wsp:rsid wsp:val="003B79B9"/>
<wsp:rsid wsp:val="003B7C64"/>
<wsp:rsid wsp:val="003D4D2A"/>
<wsp:rsid wsp:val="003D4E02"/>
<wsp:rsid wsp:val="003D63AA"/>
<wsp:rsid wsp:val="003D7E85"/>
<wsp:rsid wsp:val="003E0FF0"/>
<wsp:rsid wsp:val="00401478"/>
<wsp:rsid wsp:val="00401833"/>
<wsp:rsid wsp:val="004030B7"/>
<wsp:rsid wsp:val="00413DBE"/>
<wsp:rsid wsp:val="00416081"/>
<wsp:rsid wsp:val="00423AAB"/>
<wsp:rsid wsp:val="00424E76"/>
<wsp:rsid wsp:val="004253FC"/>
<wsp:rsid wsp:val="0043397C"/>
<wsp:rsid wsp:val="00441A83"/>
<wsp:rsid wsp:val="00442123"/>
<wsp:rsid wsp:val="00442F70"/>
<wsp:rsid wsp:val="00447864"/>
<wsp:rsid wsp:val="00450C5D"/>
<wsp:rsid wsp:val="00456E86"/>
<wsp:rsid wsp:val="00465CBA"/>
<wsp:rsid wsp:val="0046661C"/>
<wsp:rsid wsp:val="004719FF"/>
<wsp:rsid wsp:val="00480A11"/>
<wsp:rsid wsp:val="004822C5"/>
<wsp:rsid wsp:val="0048613E"/>
<wsp:rsid wsp:val="00492DCF"/>
<wsp:rsid wsp:val="00495967"/>
<wsp:rsid wsp:val="00495DEE"/>
<wsp:rsid wsp:val="004A46E6"/>
<wsp:rsid wsp:val="004A603F"/>
<wsp:rsid wsp:val="004B733D"/>
<wsp:rsid wsp:val="004D090A"/>
<wsp:rsid wsp:val="004E0B31"/>
<wsp:rsid wsp:val="004E3865"/>
<wsp:rsid wsp:val="004E4FD7"/>
<wsp:rsid wsp:val="00514EB2"/>
<wsp:rsid wsp:val="00514EF0"/>
<wsp:rsid wsp:val="0052011D"/>
<wsp:rsid wsp:val="0052268F"/>
<wsp:rsid wsp:val="005407CA"/>
<wsp:rsid wsp:val="00550E61"/>
<wsp:rsid wsp:val="005824E8"/>
<wsp:rsid wsp:val="005A23E3"/>
<wsp:rsid wsp:val="005A3EC5"/>
<wsp:rsid wsp:val="005B3651"/>
<wsp:rsid wsp:val="005B6F7D"/>
<wsp:rsid wsp:val="005C4BE6"/>
<wsp:rsid wsp:val="005C7858"/>
<wsp:rsid wsp:val="005D04A5"/>
<wsp:rsid wsp:val="005D67F1"/>
<wsp:rsid wsp:val="005D6DC9"/>
<wsp:rsid wsp:val="005E0D6E"/>
<wsp:rsid wsp:val="005E27F8"/>
<wsp:rsid wsp:val="005E4B1A"/>
<wsp:rsid wsp:val="005F43E2"/>
<wsp:rsid wsp:val="005F7312"/>
<wsp:rsid wsp:val="00602701"/>
<wsp:rsid wsp:val="00602F18"/>
<wsp:rsid wsp:val="00613AE1"/>
<wsp:rsid wsp:val="00615FFE"/>
<wsp:rsid wsp:val="00620858"/>
<wsp:rsid wsp:val="00620AF4"/>
<wsp:rsid wsp:val="006217FB"/>
<wsp:rsid wsp:val="00621AF8"/>
<wsp:rsid wsp:val="0062585D"/>
<wsp:rsid wsp:val="00630458"/>
<wsp:rsid wsp:val="00635985"/>
<wsp:rsid wsp:val="00637AF8"/>
<wsp:rsid wsp:val="00647786"/>
<wsp:rsid wsp:val="00647B4B"/>
<wsp:rsid wsp:val="00667504"/>
<wsp:rsid wsp:val="00667573"/>
<wsp:rsid wsp:val="00680963"/>
<wsp:rsid wsp:val="00681D70"/>
<wsp:rsid wsp:val="00682F4C"/>
<wsp:rsid wsp:val="00684C91"/>
<wsp:rsid wsp:val="006B121D"/>
<wsp:rsid wsp:val="006C35A5"/>
<wsp:rsid wsp:val="006D26C4"/>
<wsp:rsid wsp:val="006E22F4"/>
<wsp:rsid wsp:val="006E23ED"/>
<wsp:rsid wsp:val="007154DC"/>
<wsp:rsid wsp:val="00715C5A"/>
<wsp:rsid wsp:val="00716234"/>
<wsp:rsid wsp:val="0072730D"/>
<wsp:rsid wsp:val="00733B7F"/>
<wsp:rsid wsp:val="00735463"/>
<wsp:rsid wsp:val="0073697B"/>
<wsp:rsid wsp:val="00744A29"/>
<wsp:rsid wsp:val="00762FA4"/>
<wsp:rsid wsp:val="007635E9"/>
<wsp:rsid wsp:val="007667E9"/>
<wsp:rsid wsp:val="00770E7A"/>
<wsp:rsid wsp:val="00771F58"/>
<wsp:rsid wsp:val="007746C3"/>
<wsp:rsid wsp:val="00777F61"/>
<wsp:rsid wsp:val="007967C8"/>
<wsp:rsid wsp:val="007A1273"/>
<wsp:rsid wsp:val="007A4833"/>
<wsp:rsid wsp:val="007A601F"/>
<wsp:rsid wsp:val="007B66E1"/>
<wsp:rsid wsp:val="007C3692"/>
<wsp:rsid wsp:val="007C4C8E"/>
<wsp:rsid wsp:val="007C611A"/>
<wsp:rsid wsp:val="007C7EAA"/>
<wsp:rsid wsp:val="007D1F8B"/>
<wsp:rsid wsp:val="007D5B8C"/>
<wsp:rsid wsp:val="007D6BF4"/>
<wsp:rsid wsp:val="007F1ADD"/>
<wsp:rsid wsp:val="00802DE9"/>
<wsp:rsid wsp:val="008057D4"/>
<wsp:rsid wsp:val="00807EED"/>
<wsp:rsid wsp:val="00824601"/>
<wsp:rsid wsp:val="00831551"/>
<wsp:rsid wsp:val="00831B10"/>
<wsp:rsid wsp:val="00836108"/>
<wsp:rsid wsp:val="00845F65"/>
<wsp:rsid wsp:val="00846EE3"/>
<wsp:rsid wsp:val="00847934"/>
<wsp:rsid wsp:val="00852EB2"/>
<wsp:rsid wsp:val="008704C6"/>
<wsp:rsid wsp:val="00871902"/>
<wsp:rsid wsp:val="00874A34"/>
<wsp:rsid wsp:val="00874D55"/>
<wsp:rsid wsp:val="00877F43"/>
<wsp:rsid wsp:val="00895036"/>
<wsp:rsid wsp:val="008A3A71"/>
<wsp:rsid wsp:val="008A7DFD"/>
<wsp:rsid wsp:val="008C0003"/>
<wsp:rsid wsp:val="008C3686"/>
<wsp:rsid wsp:val="008C49EC"/>
<wsp:rsid wsp:val="008D150E"/>
<wsp:rsid wsp:val="008D4176"/>
<wsp:rsid wsp:val="008D55FD"/>
<wsp:rsid wsp:val="008E2C24"/>
<wsp:rsid wsp:val="008F0334"/>
<wsp:rsid wsp:val="00906005"/>
<wsp:rsid wsp:val="00911EEF"/>
<wsp:rsid wsp:val="00914F68"/>
<wsp:rsid wsp:val="009218B2"/>
<wsp:rsid wsp:val="00921D5E"/>
<wsp:rsid wsp:val="00925206"/>
<wsp:rsid wsp:val="0093039C"/>
<wsp:rsid wsp:val="009365B9"/>
<wsp:rsid wsp:val="00936A56"/>
<wsp:rsid wsp:val="00940499"/>
<wsp:rsid wsp:val="0094235A"/>
<wsp:rsid wsp:val="0095271C"/>
<wsp:rsid wsp:val="00961504"/>
<wsp:rsid wsp:val="00961674"/>
<wsp:rsid wsp:val="00967D1A"/>
<wsp:rsid wsp:val="009777A4"/>
<wsp:rsid wsp:val="00987137"/>
<wsp:rsid wsp:val="00990595"/>
<wsp:rsid wsp:val="00991CB4"/>
<wsp:rsid wsp:val="00992EE1"/>
<wsp:rsid wsp:val="00995446"/>
<wsp:rsid wsp:val="009A1133"/>
<wsp:rsid wsp:val="009A2CC0"/>
<wsp:rsid wsp:val="009A451A"/>
<wsp:rsid wsp:val="009A7D90"/>
<wsp:rsid wsp:val="009C476E"/>
<wsp:rsid wsp:val="009D0824"/>
<wsp:rsid wsp:val="009D72B1"/>
<wsp:rsid wsp:val="009F3DFE"/>
<wsp:rsid wsp:val="009F515D"/>
<wsp:rsid wsp:val="009F5A9B"/>
<wsp:rsid wsp:val="009F6843"/>
<wsp:rsid wsp:val="00A03554"/>
<wsp:rsid wsp:val="00A05B0D"/>
<wsp:rsid wsp:val="00A11A99"/>
<wsp:rsid wsp:val="00A217F9"/>
<wsp:rsid wsp:val="00A22789"/>
<wsp:rsid wsp:val="00A23BC0"/>
<wsp:rsid wsp:val="00A27278"/>
<wsp:rsid wsp:val="00A3419D"/>
<wsp:rsid wsp:val="00A41317"/>
<wsp:rsid wsp:val="00A46EF1"/>
<wsp:rsid wsp:val="00A524F1"/>
<wsp:rsid wsp:val="00A61368"/>
<wsp:rsid wsp:val="00A644AE"/>
<wsp:rsid wsp:val="00A70A8A"/>
<wsp:rsid wsp:val="00A73DF7"/>
<wsp:rsid wsp:val="00A935D6"/>
<wsp:rsid wsp:val="00AA1BE1"/>
<wsp:rsid wsp:val="00AA6B54"/>
<wsp:rsid wsp:val="00AB0C87"/>
<wsp:rsid wsp:val="00AB320E"/>
<wsp:rsid wsp:val="00AB367B"/>
<wsp:rsid wsp:val="00AC495D"/>
<wsp:rsid wsp:val="00AD08D6"/>
<wsp:rsid wsp:val="00AE78A5"/>
<wsp:rsid wsp:val="00B0446B"/>
<wsp:rsid wsp:val="00B171CA"/>
<wsp:rsid wsp:val="00B20CCE"/>
<wsp:rsid wsp:val="00B22B03"/>
<wsp:rsid wsp:val="00B2428C"/>
<wsp:rsid wsp:val="00B335FE"/>
<wsp:rsid wsp:val="00B42EB0"/>
<wsp:rsid wsp:val="00B556D1"/>
<wsp:rsid wsp:val="00B570DB"/>
<wsp:rsid wsp:val="00B6143F"/>
<wsp:rsid wsp:val="00B7502A"/>
<wsp:rsid wsp:val="00B90FDE"/>
<wsp:rsid wsp:val="00BA6529"/>
<wsp:rsid wsp:val="00BB27B0"/>
<wsp:rsid wsp:val="00BC6338"/>
<wsp:rsid wsp:val="00BD42E1"/>
<wsp:rsid wsp:val="00BD589B"/>
<wsp:rsid wsp:val="00BE1807"/>
<wsp:rsid wsp:val="00BF106F"/>
<wsp:rsid wsp:val="00C000F9"/>
<wsp:rsid wsp:val="00C04C26"/>
<wsp:rsid wsp:val="00C07510"/>
<wsp:rsid wsp:val="00C124C3"/>
<wsp:rsid wsp:val="00C24332"/>
<wsp:rsid wsp:val="00C36582"/>
<wsp:rsid wsp:val="00C36F22"/>
<wsp:rsid wsp:val="00C37B12"/>
<wsp:rsid wsp:val="00C42D52"/>
<wsp:rsid wsp:val="00C47325"/>
<wsp:rsid wsp:val="00C6255C"/>
<wsp:rsid wsp:val="00C64010"/>
<wsp:rsid wsp:val="00C72B8E"/>
<wsp:rsid wsp:val="00C85338"/>
<wsp:rsid wsp:val="00C86C6E"/>
<wsp:rsid wsp:val="00CA5C47"/>
<wsp:rsid wsp:val="00CD08E3"/>
<wsp:rsid wsp:val="00CD3B29"/>
<wsp:rsid wsp:val="00CD4508"/>
<wsp:rsid wsp:val="00CE2CB1"/>
<wsp:rsid wsp:val="00CE58EB"/>
<wsp:rsid wsp:val="00CE687D"/>
<wsp:rsid wsp:val="00CF0364"/>
<wsp:rsid wsp:val="00CF14E1"/>
<wsp:rsid wsp:val="00CF3CEB"/>
<wsp:rsid wsp:val="00CF3DC6"/>
<wsp:rsid wsp:val="00D04F44"/>
<wsp:rsid wsp:val="00D07D14"/>
<wsp:rsid wsp:val="00D12023"/>
<wsp:rsid wsp:val="00D16123"/>
<wsp:rsid wsp:val="00D234F6"/>
<wsp:rsid wsp:val="00D34A7E"/>
<wsp:rsid wsp:val="00D356BB"/>
<wsp:rsid wsp:val="00D44002"/>
<wsp:rsid wsp:val="00D453F2"/>
<wsp:rsid wsp:val="00D45F90"/>
<wsp:rsid wsp:val="00D6598B"/>
<wsp:rsid wsp:val="00D70FBC"/>
<wsp:rsid wsp:val="00D75EBC"/>
<wsp:rsid wsp:val="00D76B44"/>
<wsp:rsid wsp:val="00D82CC2"/>
<wsp:rsid wsp:val="00D90039"/>
<wsp:rsid wsp:val="00D9343A"/>
<wsp:rsid wsp:val="00DA33D0"/>
<wsp:rsid wsp:val="00DB4582"/>
<wsp:rsid wsp:val="00DC2DC4"/>
<wsp:rsid wsp:val="00DC492A"/>
<wsp:rsid wsp:val="00DC66EF"/>
<wsp:rsid wsp:val="00DD03B4"/>
<wsp:rsid wsp:val="00DD2949"/>
<wsp:rsid wsp:val="00DE35CA"/>
<wsp:rsid wsp:val="00DE6174"/>
<wsp:rsid wsp:val="00DE740E"/>
<wsp:rsid wsp:val="00E05FA3"/>
<wsp:rsid wsp:val="00E2717D"/>
<wsp:rsid wsp:val="00E3107C"/>
<wsp:rsid wsp:val="00E409FF"/>
<wsp:rsid wsp:val="00E40E92"/>
<wsp:rsid wsp:val="00E5330A"/>
<wsp:rsid wsp:val="00E548D5"/>
<wsp:rsid wsp:val="00E60D9F"/>
<wsp:rsid wsp:val="00E61C4F"/>
<wsp:rsid wsp:val="00E6318F"/>
<wsp:rsid wsp:val="00E7715F"/>
<wsp:rsid wsp:val="00E81248"/>
<wsp:rsid wsp:val="00E8486E"/>
<wsp:rsid wsp:val="00E8665A"/>
<wsp:rsid wsp:val="00E875C9"/>
<wsp:rsid wsp:val="00E930FB"/>
<wsp:rsid wsp:val="00E93C55"/>
<wsp:rsid wsp:val="00EA2D70"/>
<wsp:rsid wsp:val="00EA4BE2"/>
<wsp:rsid wsp:val="00EA6D38"/>
<wsp:rsid wsp:val="00EB04D1"/>
<wsp:rsid wsp:val="00EC02FD"/>
<wsp:rsid wsp:val="00EC13FC"/>
<wsp:rsid wsp:val="00EC1C80"/>
<wsp:rsid wsp:val="00EC4414"/>
<wsp:rsid wsp:val="00EC6BC3"/>
<wsp:rsid wsp:val="00EC6E68"/>
<wsp:rsid wsp:val="00EE405D"/>
<wsp:rsid wsp:val="00EE69B4"/>
<wsp:rsid wsp:val="00EF0596"/>
<wsp:rsid wsp:val="00EF162F"/>
<wsp:rsid wsp:val="00EF26DA"/>
<wsp:rsid wsp:val="00EF2CE4"/>
<wsp:rsid wsp:val="00F025AB"/>
<wsp:rsid wsp:val="00F025DC"/>
<wsp:rsid wsp:val="00F0298B"/>
<wsp:rsid wsp:val="00F142C4"/>
<wsp:rsid wsp:val="00F34A86"/>
<wsp:rsid wsp:val="00F36231"/>
<wsp:rsid wsp:val="00F43798"/>
<wsp:rsid wsp:val="00F5597C"/>
<wsp:rsid wsp:val="00F56A61"/>
<wsp:rsid wsp:val="00F60669"/>
<wsp:rsid wsp:val="00F63686"/>
<wsp:rsid wsp:val="00F63B27"/>
<wsp:rsid wsp:val="00F72072"/>
<wsp:rsid wsp:val="00F81E0D"/>
<wsp:rsid wsp:val="00F83CF4"/>
<wsp:rsid wsp:val="00F8413B"/>
<wsp:rsid wsp:val="00F9178E"/>
<wsp:rsid wsp:val="00F92455"/>
<wsp:rsid wsp:val="00F92E8B"/>
<wsp:rsid wsp:val="00F93A7A"/>
<wsp:rsid wsp:val="00F9746F"/>
<wsp:rsid wsp:val="00FB5B0D"/>
<wsp:rsid wsp:val="00FC5615"/>
<wsp:rsid wsp:val="00FD43B6"/>
<wsp:rsid wsp:val="00FF1E8C"/>
</wsp:rsids>
</w:docPr>
<w:body>
<wx:sect>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00456E86" wsp:rsidRDefault="00E8665A" wsp:rsidP="00456E86">
<w:pPr>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:ind w:right="68"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:fareast="楷体_GB2312" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:b-cs/>
<w:color w:val="000000"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:fareast="楷体_GB2312" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="楷体_GB2312"/>
<w:b/>
<w:b-cs/>
<w:color w:val="000000"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>软件工程</w:t>
</w:r>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:fareast="楷体_GB2312" w:h-ansi="Times New Roman"/>
<wx:font wx:val="楷体_GB2312"/>
<w:b/>
<w:b-cs/>
<w:color w:val="000000"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>本科专业</w:t>
</w:r>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:fareast="楷体_GB2312" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="楷体_GB2312"/>
<w:b/>
<w:b-cs/>
<w:color w:val="000000"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>人才</w:t>
</w:r>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:fareast="楷体_GB2312" w:h-ansi="Times New Roman"/>
<wx:font wx:val="楷体_GB2312"/>
<w:b/>
<w:b-cs/>
<w:color w:val="000000"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>培养方案实施进程表</w:t>
</w:r>
</w:p>
<w:tbl>
<w:tblPr>
<w:tblW w:w="9698" w:type="dxa"/>
<w:jc w:val="center"/>
<w:tblBorders>
<w:top w:val="single" w:sz="12" wx:bdrwidth="30" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="12" wx:bdrwidth="30" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="12" wx:bdrwidth="30" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="12" wx:bdrwidth="30" w:space="0" w:color="auto"/>
<w:insideH w:val="single" w:sz="2" wx:bdrwidth="5" w:space="0" w:color="auto"/>
<w:insideV w:val="single" w:sz="2" wx:bdrwidth="5" w:space="0" w:color="auto"/>
</w:tblBorders>
<w:tblLayout w:type="Fixed"/>
</w:tblPr>
<w:tblGrid>
<w:gridCol w:w="403"/>
<w:gridCol w:w="379"/>
<w:gridCol w:w="880"/>
<w:gridCol w:w="2390"/>
<w:gridCol w:w="595"/>
<w:gridCol w:w="622"/>
<w:gridCol w:w="670"/>
<w:gridCol w:w="709"/>
<w:gridCol w:w="708"/>
<w:gridCol w:w="764"/>
<w:gridCol w:w="653"/>
<w:gridCol w:w="925"/>
</w:tblGrid>
<w:tr wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidTr="00D16123">
<w:trPr>
<w:cantSplit/>
<w:trHeight w:val="280"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="403" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>年</w:t>
</w:r>
</w:p>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>级</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="379" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>学期</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="880" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>课程</w:t>
</w:r>
</w:p>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>代码</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2390" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>课程名称</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="595" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>是否</w:t>
</w:r>
</w:p>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>核心</w:t>
</w:r>
</w:p>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>课程</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="622" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>总</w:t>
</w:r>
</w:p>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>学分</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2087" w:type="dxa"/>
<w:gridSpan w:val="3"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>学时</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="764" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:w w:val="90"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:w w:val="90"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>周学时</w:t>
</w:r>
</w:p>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:spacing w:val="-12"/>
<w:w w:val="90"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:spacing w:val="-12"/>
<w:w w:val="90"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>（课内）</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="653" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>考核</w:t>
</w:r>
</w:p>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>方式</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="925" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>备注</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidTr="00D16123">
<w:trPr>
<w:cantSplit/>
<w:trHeight w:val="153"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="403" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="379" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="880" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2390" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="595" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="622" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="670" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>总学时</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="709" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>讲课</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>实践</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="764" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="653" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="925" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<#list semester5.course as course>
<#if course_index==0>
<w:tr wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidTr="00D16123">
<w:trPr>
<w:cantSplit/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="403" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00C37B12" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>三</w:t>
</w:r>
</w:p>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>年</w:t>
</w:r>
</w:p>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>级</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="379" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>秋季学期</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="880" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00D6598B" wsp:rsidP="00831551">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.identifier}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2390" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00D6598B" wsp:rsidP="00831551">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.name}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="595" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="622" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00D6598B" wsp:rsidP="001105AF">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.score}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="670" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00D6598B" wsp:rsidP="001105AF">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.time}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="709" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00D6598B" wsp:rsidP="001105AF">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.theoretical}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00416081" wsp:rsidP="001105AF">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.experiement}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="764" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00416081" wsp:rsidP="001105AF">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.hpw}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="653" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00E8665A" wsp:rsidP="00831551">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="925" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00E8665A" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00123256" wsp:rsidP="001105AF">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.start}-${course.end}</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>周</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<#elseif course_index!=0>
<w:tr wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidTr="00D16123">
<w:trPr>
<w:cantSplit/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="403" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="000E5386" wsp:rsidP="000E5386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="379" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="000E5386" wsp:rsidP="000E5386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="880" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRDefault="000E5386" wsp:rsidP="000E5386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.identifier}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2390" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRDefault="000E5386" wsp:rsidP="005D6DC9">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.name}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="595" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="000E5386" wsp:rsidP="000E5386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="622" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRDefault="000E5386" wsp:rsidP="001105AF">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.score}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="670" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRDefault="000E5386" wsp:rsidP="001105AF">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.time}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="709" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="000E5386" wsp:rsidP="001105AF">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.theoretical}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="000E5386" wsp:rsidP="001105AF">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.experiement}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="764" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="000E5386" wsp:rsidP="001105AF">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.hpw}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="653" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="000E5386" wsp:rsidP="000E5386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="925" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="000E5386" wsp:rsidP="001105AF">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.start}-${course.end}</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>周</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
</#if>
</#list>
<w:tr wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidTr="00D16123">
<w:trPr>
<w:cantSplit/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="403" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="000E5386" wsp:rsidP="000E5386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="379" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="000E5386" wsp:rsidP="000E5386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="3270" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="000E5386" wsp:rsidP="000E5386">
<w:pPr>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>小</w:t>
</w:r>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>    </w:t>
</w:r>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>计</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="595" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="000E5386" wsp:rsidP="000E5386">
<w:pPr>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="622" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="000E5386" wsp:rsidP="001105AF">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${semester5.total.score}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="670" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="000E5386" wsp:rsidP="001105AF">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${semester5.total.time}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="709" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="003D7E85" wsp:rsidP="001105AF">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${semester5.total.theoretical}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="003D7E85" wsp:rsidP="001105AF">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${semester5.total.experiement}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="764" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="003D7E85" wsp:rsidP="001105AF">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${semester5.total.hpw}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="653" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="000E5386" wsp:rsidP="000E5386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="925" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="000E5386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="000E5386" wsp:rsidP="000E5386">
<w:pPr>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<#list semester6.course as course>
<#if course_index==0>
<w:tr wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidTr="00D16123">
<w:trPr>
<w:cantSplit/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="403" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="379" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>春季学期</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="880" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.identifier}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2390" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.name}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="595" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="622" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.score}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="670" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.time}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="709" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.theoretical}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.experiement}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="764" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.hpw}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="653" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="925" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.start}-${course.end}</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>周</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<#elseif course_index!=0>
<w:tr wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidTr="00D16123">
<w:trPr>
<w:cantSplit/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="403" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="379" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="880" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.identifier}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2390" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.name}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="595" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="622" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.score}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="670" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.time}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="709" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.theoretical}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.experiement}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="764" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.hpw}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="653" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="925" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.start}-${course.end}</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>周</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
</#if>
</#list>
<w:tr wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidTr="00D16123">
<w:trPr>
<w:cantSplit/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="403" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="379" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="3270" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>小</w:t>
</w:r>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>    </w:t>
</w:r>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>计</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="595" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="622" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${semester6.total.score}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="670" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${semester6.total.time}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="709" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${semester6.total.theoretical}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${semester6.total.experiement}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="764" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${semester6.total.hpw}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="653" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="925" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<#list semester7.course as course>
<#if course_index==0>
<w:tr wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidTr="00D16123">
<w:trPr>
<w:cantSplit/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="403" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00C37B12" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>四</w:t>
</w:r>
<w:r wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>  </w:t>
</w:r>
<w:r wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>年</w:t>
</w:r>
<w:r wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>   </w:t>
</w:r>
<w:r wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>级</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="379" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>秋季学期</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="880" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.identifier}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2390" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.name}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="595" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="622" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.score}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="670" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.time}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="709" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.theoretical}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.experiement}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="764" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.hpw}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="653" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="925" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.start}-${course.end}</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>周</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<#elseif course_index!=0>
<w:tr wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidTr="00D16123">
<w:trPr>
<w:cantSplit/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="403" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="379" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="880" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.identifier}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2390" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.name}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="595" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="622" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.score}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="670" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.time}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="709" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.theoretical}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.experiement}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="764" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.hpw}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="653" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="925" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.start}-${course.end}</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>周</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
</#if>
</#list>
<w:tr wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidTr="00D16123">
<w:trPr>
<w:cantSplit/>
<w:trHeight w:val="151"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="403" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="379" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="3270" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>小</w:t>
</w:r>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>    </w:t>
</w:r>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>计</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="595" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="622" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${semester7.total.score}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="670" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${semester7.total.time}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="709" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${semester7.total.theoretical}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${semester7.total.experiement}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="764" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${semester7.total.hpw}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="653" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="925" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<#list semester8.course as course>
<#if course_index==0>
<w:tr wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidTr="00D16123">
<w:trPr>
<w:cantSplit/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="403" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="379" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>春季学期</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="880" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.identifier}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2390" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.name}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="595" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="622" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.score}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="670" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.time}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="709" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.theoretical}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.experiement}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="764" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.hpw}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="653" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="925" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.start}-${course.end}</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>周</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<#elseif course_index!=0>
<w:tr wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidTr="00D16123">
<w:trPr>
<w:cantSplit/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="403" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="379" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="880" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.identifier}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2390" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.name}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="595" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="622" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.score}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="670" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.time}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="709" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.theoretical}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.experiement}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="764" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.hpw}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="653" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="925" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${course.start}-${course.end}</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>周</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
</#if>
</#list>
<w:tr wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidTr="00D16123">
<w:trPr>
<w:cantSplit/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="403" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="379" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="240" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="3270" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>小</w:t>
</w:r>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>    </w:t>
</w:r>
<w:r wsp:rsidRPr="00C44C9B">
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>计</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="595" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="622" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${semester8.total.score}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="670" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${semester8.total.time}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="709" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${semester8.total.theoretical}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${semester8.total.experiement}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="764" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>${semester8.total.hpw}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="653" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:widowControl/>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="925" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p wsp:rsidR="00144386" wsp:rsidRPr="00C44C9B" wsp:rsidRDefault="00144386" wsp:rsidP="00144386">
<w:pPr>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:color w:val="000000"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
</w:tbl>
<w:p wsp:rsidR="00715C5A" wsp:rsidRPr="001C4BCA" wsp:rsidRDefault="00715C5A" wsp:rsidP="00456E86">
<w:pPr>
<w:spacing w:line="0" w:line-rule="at-least"/>
<w:ind w:right="68"/>
<w:rPr>
<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
<wx:font wx:val="仿宋"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
</w:p>
<w:sectPr wsp:rsidR="00715C5A" wsp:rsidRPr="001C4BCA" wsp:rsidSect="00456E86">
<w:hdr w:type="odd">
<w:p wsp:rsidR="00895036" wsp:rsidRDefault="00895036">
<w:pPr>
<w:pStyle w:val="af3"/>
<w:pBdr>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:pBdr>
</w:pPr>
</w:p>
</w:hdr>
<w:ftr w:type="even">
<wx:pBdrGroup>
<wx:apo>
<wx:jc wx:val="center"/>
</wx:apo>
<w:p wsp:rsidR="00895036" wsp:rsidRDefault="00895036">
<w:pPr>
<w:pStyle w:val="af5"/>
<w:framePr w:wrap="around" w:vanchor="text" w:hanchor="margin" w:x-align="center" w:y="1"/>
<w:rPr>
<w:rStyle w:val="a6"/>
</w:rPr>
</w:pPr>
<w:r>
<w:fldChar w:fldCharType="begin"/>
</w:r>
<w:r>
<w:rPr>
<w:rStyle w:val="a6"/>
</w:rPr>
<w:instrText>PAGE  </w:instrText>
</w:r>
<w:r>
<w:fldChar w:fldCharType="separate"/>
</w:r>
<w:r>
<w:rPr>
<w:rStyle w:val="a6"/>
</w:rPr>
<w:t>1</w:t>
</w:r>
<w:r>
<w:fldChar w:fldCharType="end"/>
</w:r>
</w:p>
</wx:pBdrGroup>
<w:p wsp:rsidR="00895036" wsp:rsidRDefault="00895036">
<w:pPr>
<w:pStyle w:val="af5"/>
</w:pPr>
</w:p>
</w:ftr>
<w:ftr w:type="odd">
<wx:pBdrGroup>
<wx:apo>
<wx:jc wx:val="center"/>
</wx:apo>
<w:p wsp:rsidR="00895036" wsp:rsidRDefault="00895036">
<w:pPr>
<w:pStyle w:val="af5"/>
<w:framePr w:wrap="around" w:vanchor="text" w:hanchor="margin" w:x-align="center" w:y="1"/>
<w:rPr>
<w:rStyle w:val="a6"/>
</w:rPr>
</w:pPr>
<w:r>
<w:fldChar w:fldCharType="begin"/>
</w:r>
<w:r>
<w:rPr>
<w:rStyle w:val="a6"/>
</w:rPr>
<w:instrText>PAGE  </w:instrText>
</w:r>
<w:r>
<w:fldChar w:fldCharType="separate"/>
</w:r>
<w:r wsp:rsidR="00C37B12">
<w:rPr>
<w:rStyle w:val="a6"/>
<w:noProof/>
</w:rPr>
<w:t>1</w:t>
</w:r>
<w:r>
<w:fldChar w:fldCharType="end"/>
</w:r>
</w:p>
</wx:pBdrGroup>
<w:p wsp:rsidR="00895036" wsp:rsidRDefault="00895036">
<w:pPr>
<w:pStyle w:val="af5"/>
</w:pPr>
</w:p>
</w:ftr>
<w:pgSz w:w="11906" w:h="16838"/>
<w:pgMar w:top="851" w:right="1418" w:bottom="851" w:left="1418" w:header="851" w:footer="992" w:gutter="0"/>
<w:cols w:space="720"/>
<w:docGrid w:type="lines" w:line-pitch="312"/>
</w:sectPr>
</wx:sect>
</w:body>
</w:wordDocument>
