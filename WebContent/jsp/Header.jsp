<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>
<link href="<%=request.getContextPath()%>/css/common.css"
	rel="stylesheet" type="text/css">

<div class="container">

	<!-- 巨幕 -->
	<div class="jumbotron bg_img">
		<h2>求真至善,修德允能</h2>
		<p>IT精英从这里出发</p>
		<p></p>
		<div id="code"></div>
	</div>

	<!--导航条-->
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#myCollapse1">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a href="#" class="navbar-brand">当前用户：<shiro:guest>未登录</shiro:guest>
					<shiro:principal /></a>
			</div>
			<!--/.navbar-header-->

			<div class="collapse navbar-collapse" id="myCollapse1">
				<ul class="nav navbar-nav" role="navigation">
					<li><a href="<%=request.getContextPath()%>/jsp/index.jsp">主页</a></li>
					<li><a href="<%=request.getContextPath()%>/jsp/SystemManager.jsp">系统管理</a></li>
					<!-- <li><a href="#">课程管理</a></li> -->
					<shiro:authenticated>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"> 系统管理 <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a
									href="<%=request.getContextPath()%>/TbUserController/QueryTbUser.action"><span
										class="glyphicon glyphicon-th-list"></span>&nbsp;&nbsp;用户管理</a></li>
										
								<li><a
									href="<%=request.getContextPath()%>/TbRoleController/QueryTbRole.action"><span
										class="glyphicon glyphicon-th-list"></span>&nbsp;&nbsp;角色管理</a></li>
								<li><a
									href="<%=request.getContextPath()%>/TbPermissionController/QueryTbPermission.action"><span
										class="glyphicon glyphicon-th-list"></span>&nbsp;&nbsp;权限管理</a></li>
										
								<li><a
									href="<%=request.getContextPath()%>/TbUserRoleController/QueryTbUserRole.action"><span
										class="glyphicon glyphicon-th-list"></span>&nbsp;&nbsp;角色分配</a></li>
							</ul>
						</li>
						
						<!-- 课程管理 -->
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"> 课程管理 <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a
									href="<%=request.getContextPath()%>/CourseController/QueryCourse.action"><span
										class="glyphicon glyphicon-th-list"></span>&nbsp;&nbsp;课程管理</a>
								</li>
							</ul>
						</li>
					
						<!-- 培养方案管理 -->
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"> 培养方案管理 <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a
									href="<%=request.getContextPath()%>/CurriculumController/QueryCurriculums.action"><span
										class="glyphicon glyphicon-th-list"></span>&nbsp;&nbsp;实施进程管理</a></li>
								<li><a
									href="<%=request.getContextPath()%>/CurriculumController/CheckCurriculums.action"><span
										class="glyphicon glyphicon-th-list"></span>&nbsp;&nbsp;实施进程审核</a></li>
								<li>
									<a
									href="<%=request.getContextPath()%>/CurriculumController/QuerySchedule.action"><span
										class="glyphicon glyphicon-th-list"></span>&nbsp;&nbsp;计划表查看</a></li>
								<li>
									<a
									href="<%=request.getContextPath()%>/CurriculumController/CheckSchedule.action"><span
										class="glyphicon glyphicon-th-list"></span>&nbsp;&nbsp;计划表审核</a></li>
							</ul></li>

					</shiro:authenticated>

					<shiro:notAuthenticated>
						<li><a href="<%=request.getContextPath()%>/jsp/login.jsp">登陆系统</a></li>
					</shiro:notAuthenticated>
					<shiro:authenticated>
						<li><a href="<%=request.getContextPath()%>/logout">退出登陆</a></li>
					</shiro:authenticated>
				</ul>
			</div>
			<!--/.collapse-->
		</div>
		<!--/.container-fluid-->
	</nav>
	<!--/导航条-->
</div>