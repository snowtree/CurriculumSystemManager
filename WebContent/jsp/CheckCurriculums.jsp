<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <title>审核进程表</title>
	<link rel='icon' href='<%=request.getContextPath()%>/images/tm.ico ' type=‘image/x-ico’ />
    <link href="<%=request.getContextPath() %>/js/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<%=request.getContextPath() %>/js/validform/css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/validform/js/Validform_v5.3.2.js"></script>
    
    <style>
		.total{
			color:#F00;
		}
    </style>
    
	<script type="text/javascript">
		$(function(){
			 $('.right').each(function(){
				 compute_total($(this));
			 });
			 
			 $.ajax({
					url : '<%=request.getContextPath() %>/SpecialityController/QueryAllSpeciality.action',
					datatype : 'json',
					success : function(specialitys){
						//空白选项
						specialitys.forEach(function(speciality){
							var url = "<%=request.getContextPath()%>/CurriculumController/CheckCurriculums.action?specialityid=";
							url += speciality.id;
							$('#speciality_list').append('<li><a href="' + url + '">' + speciality.name + '</a></li>');
						})
					}
			});
			 
		})
		
		function compute_total($whichbody){
			var total = new Array()
			var $dest_trs = $whichbody.children('tr');
			
			for(var i = 0; i < $($dest_trs[0]).children().length - 1 ; i++){
				total[i] = 0;
			}
			
			for(var i = 0;i < $dest_trs.length; i++){
				var $dest_tds = $($dest_trs[i]).children();
				
				if(i < $dest_trs.length - 1){//前面的行
					
					$dest_tds[4].innerHTML = parseInt($dest_tds[5].innerHTML) + parseInt($dest_tds[6].innerHTML); + "";
					
					for(var j = 0; j < $dest_tds.length ; j ++ ){
						if(j >= 3 && j <= 7){
							total[j] += parseInt($dest_tds[j].innerHTML);
						}
					}
				
					
				}else{//total 行
					for(var j = 0; j < $dest_tds.length ; j ++ ){
						if(j >= 3 && j <= 7){
							$dest_tds[j].innerHTML = total[j] + "";
						}
					}
				}
			}
		}
    </script>
</head>
<body>

    <!-- 包含网页头部 -->
    <jsp:include page="Header.jsp"></jsp:include>

    <!-- 主体内容 -->
    <div class="container" style="min-height: 400px;">
        <div class="row">

            <!-- 左侧 -->
			<div class="col-md-3">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">查询条件</h3>
					</div>
					<div class="panel-body">
						<div class="btn-group">
						    <button type="button" class="btn btn-default btn-block dropdown-toggle" data-toggle="dropdown" >选择一个专业
						        <span class="caret"></span>
						    </button>
						    <ul class="dropdown-menu" role="menu" id="speciality_list">
						       
						    </ul>
						</div>
					</div>
				</div>
			</div>

			<!-- 右侧 -->
            <div class="col-md-9">

            <div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">实施进程表</h3>
					</div>
					<div class="panel-body">
			            <!-- 导航区 -->			
						<ul class="nav nav-tabs" role="tablist">
						  <li role="presentation" class="active"><a href="#semester1" role="tab" data-toggle="tab">第一学期</a></li>
						  <li role="presentation"><a href="#semester2" role="tab" data-toggle="tab">第二学期</a></li>
						  <li role="presentation"><a href="#semester3" role="tab" data-toggle="tab">第三学期</a></li>
						  <li role="presentation"><a href="#semester4" role="tab" data-toggle="tab">第四学期</a></li>
						  <li role="presentation"><a href="#semester5" role="tab" data-toggle="tab">第五学期</a></li>
						  <li role="presentation"><a href="#semester6" role="tab" data-toggle="tab">第六学期</a></li>
						  <li role="presentation"><a href="#semester7" role="tab" data-toggle="tab">第七学期</a></li>
						  <li role="presentation"><a href="#semester8" role="tab" data-toggle="tab">第八学期</a></li>
						</ul>
						 
						<!-- 面板区 -->
						<div class="tab-content">
						  <div role="tabpanel" class="tab-pane active" id="semester1">
						  	<div class="table-responsive">
								<table class="table table-striped table-bordered">
					              <thead>
					                <tr>
					                  <th style="width: 5%">ID</th>
					                  <th style="width: 15%">课程名称</th>                                    
					                  <th style="width: 6%">课程编号</th>                                    
					                  <th style="width: 6%">学分</th>                                    
					                  <th style="width: 6%">学期学时</th>
					                  <th style="width: 6%">理论学时</th>
					                  <th style="width: 6%">实验学时</th>
					                  <th style="width: 5%">周学时</th>
					                  <th style="width: 5%">起始周</th>
					                  <th style="width: 5%">结束周</th>
					                </tr>
					              </thead>
					              <tbody>
					              	<c:forEach items="${curriculums1 }" var="curriculum">
						                <tr class="course">
						                  <td>${curriculum.id }</td>
						                  <td>${curriculum.name }</td>
						                  <td>${curriculum.identifier }</td>
						                  <td>${curriculum.score }</td>
						                  <td>${curriculum.time }</td>
						                  <td>${curriculum.theoretical }</td>
						                  <td>${curriculum.experiement }</td>
						                  <td class='editable'>${curriculum.hpw }</td>                                
						                  <td class='editable'>${curriculum.start }</td>                                
						                  <td class='editable'>${curriculum.end }</td>                                
						                </tr>
					                </c:forEach>	
						                <tr class="total">
						              		<td>##</td>
							              	<td>小计</td>
							              	<td>----</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>----</td>
							              	<td>----</td>
						              	</tr>                
					              </tbody>
					            </table>			
			                </div>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="semester2">
						  
							  <div class="table-responsive">
									<table class="table table-striped table-bordered">
						              <thead>
						                <tr>
						                  <th style="width: 5%">ID</th>
						                  <th style="width: 15%">课程名称</th>                                    
						                  <th style="width: 6%">课程编号</th>                                    
						                  <th style="width: 6%">学分</th>                                    
						                  <th style="width: 6%">学期学时</th>
						                  <th style="width: 6%">理论学时</th>
						                  <th style="width: 6%">实验学时</th>
						                  <th style="width: 5%">周学时</th>
						                  <th style="width: 5%">起始周</th>
						                  <th style="width: 5%">结束周</th>
						                </tr>
						              </thead>
						              <tbody>
						              	<c:forEach items="${curriculums2 }" var="curriculum">
							                <tr class="course">
							                  <td>${curriculum.id }</td>
							                  <td>${curriculum.name }</td>
							                  <td>${curriculum.identifier }</td>
							                  <td>${curriculum.score }</td>
							                  <td>${curriculum.time }</td>
							                  <td>${curriculum.theoretical }</td>
							                  <td>${curriculum.experiement }</td>
							                  <td>${curriculum.hpw }</td>                                
							                  <td>${curriculum.start }</td>                                
							                  <td>${curriculum.end }</td>                                
							                </tr>
							          </c:forEach>	
							          <tr class="total">
							          		<td>##</td>
							              	<td>小计</td>
							              	<td>----</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>----</td>
							              	<td>----</td>
							          </tr>                
						              </tbody>
						            </table>			
				                </div>
						  
						  </div>
						  <div role="tabpanel" class="tab-pane" id="semester3">
						  	<%-- <jsp:include page="semester.jsp"></jsp:include> --%>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="semester4">
						  	<%-- <jsp:include page="semester.jsp"></jsp:include> --%>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="semester5">
						  	<%-- <jsp:include page="semester.jsp"></jsp:include> --%>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="semester6">
						  	<%-- <jsp:include page="semester.jsp"></jsp:include> --%>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="semester7">
						  	<%-- <jsp:include page="semester.jsp"></jsp:include> --%>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="semester8">
						  	<%-- <jsp:include page="semester.jsp"></jsp:include> --%>						  						  
						  </div>
						</div>
                
                </div><!-- panel-body -->
            </div>

        </div>
    </div>
    
    <!-- 包含网页底部文件 -->
    <jsp:include page="Footer.jsp"></jsp:include>
</body>
</html>