<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="application/x-www-form-urlencoded; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <title>增加课程</title>
	<link rel='icon' href='<%=request.getContextPath()%>/image/tm.ico ' type=‘image/x-ico’ />
    <link href="<%=request.getContextPath() %>/js/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<%=request.getContextPath() %>/js/validform/css/style.css" rel="stylesheet" type="text/css">
    <link href="<%=request.getContextPath() %>/js/datetimepicker/jquery.datetimepicker.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/validform/js/Validform_v5.3.2.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/datetimepicker/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		$(document).ready(function(e) {
	        
			//一行代码搞定，提交表单的时候会自动触发验证程序
			$('#form1').Validform({
				tiptype:3	
			});
			
			$('#academyid').change(function (academyid){
				var academyid = $('#academyid option:selected').val();

				$.ajax({
					url : '<%=request.getContextPath() %>/SpecialityController/QueryAllSpecialityByAcademyid.action',
					datatype : 'json',
					data: { "academyid":academyid}, 
					success : function(specialities){
						$('#specialityid').empty();
						specialities.forEach(function(speciality){
							$('#specialityid').append('<option value=' + speciality.id + '>' + speciality.name + '</option>');
						})
					}
				});
			});
			
	    });
	</script>
</head>
<body>

    <!-- 包含网页头部 -->
    <jsp:include page="Header.jsp"></jsp:include>

    <!-- 主体内容 -->
    <div class="container" style="min-height: 400px;">
        <!-- 导航路径 -->
        <ol class="breadcrumb">
            <li>
                <a href="#">系统管理</a>
            </li>
            <li class="active">课程管理</li> 
        </ol>

        <div class="row">

            <!-- 左侧 -->
            <div class="col-md-3">
            
            </div>

            <!-- 右侧 -->
            <div class="col-md-9">

                <form id="form1" class="form-horizontal" action="<%=request.getContextPath() %>/CourseController/AddCourse.action" method="post" role="form">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">课程名</label>
                        <div class="col-sm-10">
                            <input type="text" id="name" name="name" class="form-control" placeholder="请输入课程名" datatype="*"></div>
                    </div>
                    
                    <div class="form-group">
                        <label for="identifier" class="col-sm-2 control-label">课程编码</label>
                        <div class="col-sm-10">
                            <input id="identifier" name="identifier" class="form-control" placeholder="请输入课程编码" datatype="*"></input>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="theoretical" class="col-sm-2 control-label">理论学时</label>
                        <div class="col-sm-10">
                            <input id="theoretical" name="theoretical" class="form-control" placeholder="请输入理论学时" datatype="n"></input>
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="score" class="col-sm-2 control-label">学分</label>
                        <div class="col-sm-10">
                            <input id="score" name="score" class="form-control" placeholder="请输入学分"  datatype="n"></input>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="experiement" class="col-sm-2 control-label">实验学时</label>
                        <div class="col-sm-10">
                            <input id="experiement" name="experiement" class="form-control" placeholder="请输入实验学时" datatype="n"></input>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="unit" class="col-sm-2 control-label">实验学时单位</label>
                        <div class="col-sm-4"> 
								<select id="unit" name="unit"  class="selectpicker show-tick form-control"> 
								     	<option value="0" selected="selected">时</option> 
								     	<option value="1">周</option> 
								</select>	     
						</div>
                    </div>
                    
                   

                    
                      <div class="form-group">	 
                     	<label for="academyid" class="col-sm-2 control-label">所属学院</label>                     
						 <div class="col-sm-4"> 
								<select id="academyid" name="academyid"  class="selectpicker show-tick form-control">
								    <c:forEach items="${academies}" var="academy">	 
								     	<option value="${academy.id}">${academy.name}</option>  
								    </c:forEach>
								</select>	     
						</div>
						       
                    </div>
                    
                      <div class="form-group">	 
                     	<label for="specialityid" class="col-sm-2 control-label">所属专业</label>                     
						 <div class="col-sm-4"> 
								<select id=specialityid name="specialityid"  class="selectpicker show-tick form-control">
								    <c:forEach items="${specialities}" var="speciality">	 
								     	<option value="${speciality.id}">${speciality.name}</option>  
								    </c:forEach>
								</select>	     
						</div>
						       
                    </div>
                    
                    
                    
                    <div class="form-group">	
                         <label for="coursemodeid" class="col-sm-2 control-label">课程模式</label> 
                          <div class="col-sm-4">     
	                                  
							<select id="coursemodeId" name="coursemodeId" class="selectpicker show-tick form-control" title="课程模式"  >
							      <c:forEach items="${coursemodes}" var="coursemode">	
							      			<option value="${coursemode.id }">${coursemode.name}</option>                          	
							      </c:forEach>
						      </select>       
						  </div>               
                    </div>
                    
                     <div class="form-group">	    
                      <label for="coursetypeid" class="col-sm-2 control-label">课程类型</label>                 
					    <div class="col-sm-4">     
	                                  
							<select id="coursetypeid" name="coursetypeid" class="selectpicker show-tick form-control" title="课程模式"  >
							      <c:forEach items="${coursetypes}" var="coursetype">	
							      			<option value="${coursetype.id }">${coursetype.name}</option>                          	
							      </c:forEach>
						      </select>       
						  </div>                    
                    </div>
                    
                    <div class="form-group">	 
                     	<label for="teachingmethodid" class="col-sm-2 control-label">授课方式</label>                     
						 <div class="col-sm-4">     
									<select id="teachingmethodid" name="teachingmethodid" class="selectpicker show-tick form-control" title="课程模式" >
									    <c:forEach items="${teachingmethods}" var="teachingmethod">	                  	
							                  	<option value="${teachingmethod.id}">${teachingmethod.name}</option>         
					                 	 </c:forEach>
								      </select>       
						</div>       
                    </div>
                   <div class="form-group">	     
                      	<label for="examinationmethodid" class="col-sm-2 control-label">考核方式</label>                     
						 <div class="col-sm-4">     
									<select id=examinationmethodid name="examinationmethodid" class="selectpicker show-tick form-control" title="课程模式"  >
									      <c:forEach items="${examinationmethods}" var="examinationmethod">
									      			<option value="${examinationmethod.id }">${examinationmethod.name}</option>                          	
									      </c:forEach>
								      </select>       
						</div>          
                    </div>
                   
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <button type="submit" class="btn btn-primary">增加课程1</button>
                            <button type="button" class="btn btn-success" onclick="history.back();">返回</button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>
    
    <!-- 包含网页底部文件 -->
    <jsp:include page="Footer.jsp"></jsp:include>
</body>
</html>