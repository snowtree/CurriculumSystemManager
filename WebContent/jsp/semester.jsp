<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div role="tabpanel" class="tab-pane active">
	<div class="table-responsive">
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th style="width: 5%">ID</th>
					<th style="width: 15%">课程名称</th>
					<th style="width: 6%">课程编号</th>
					<th style="width: 6%">学分</th>
					<th style="width: 6%">学期学时</th>
					<th style="width: 6%">理论学时</th>
					<th style="width: 6%">实验学时</th>
					<th style="width: 5%">周学时</th>
					<th style="width: 5%">起始周</th>
					<th style="width: 5%">结束周</th>
				</tr>
			</thead>
			<tbody class="dnd right">
				<c:forEach items="${curriculums2 }" var="curriculum">
					<tr draggable="true" class="course">
						<td>${curriculum.id }</td>
						<td>${curriculum.name }</td>
						<td>${curriculum.identifier }</td>
						<td>${curriculum.score }</td>
						<td>${curriculum.time }</td>
						<td>${curriculum.theoretical }</td>
						<td>${curriculum.experiement }</td>
						<td>${curriculum.hpw }</td>
						<td>${curriculum.start }</td>
						<td>${curriculum.end }</td>
					</tr>
				</c:forEach>
				<tr>
					<td>##</td>
					<td>小计</td>
					<td>----</td>
					<td>0</td>
					<td>0</td>
					<td>0</td>
					<td>0</td>
					<td>0</td>
					<td>----</td>
					<td>----</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
