<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <title>用户列表</title>
	<link rel='icon' href='<%=request.getContextPath()%>/image/tm.ico ' type=‘image/x-ico’ />
    <link href="<%=request.getContextPath() %>/js/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<%=request.getContextPath() %>/js/validform/css/style.css" rel="stylesheet" type="text/css">
    <link href="<%=request.getContextPath() %>/js/zTree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/validform/js/Validform_v5.3.2.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/zTree/js/jquery.ztree.core.min.js"></script> 
    <style>
    	.role{
    	 	list-style-image: url("../images/role.png");
    	}
    	
    	.over{
			border: 2px dashed #F00;
		}
    </style>
	<script type="text/javascript">	
		var zTreeObj;
	   // zTree 的参数配置，深入使用请参考 API 文档（setting 配置详解）
	   var setting = {
			view: {
				selectedMulti: false
			},
			async: {
				enable: true,
				url:"<%=request.getContextPath()%>/TbUserRoleController/AsynQueryUserInfo.action",
				autoParam:["id","name=n", "level=lv"],
				otherParam:{"otherParam":"zTreeAsyncTest"},
				dataFilter: filter
			},
			callback: {
				onClick: onClick,
			}
		};
	   
	   function filter(treeId, parentNode, childNodes) {
			if (!childNodes) return null;
			for (var i=0, l=childNodes.length; i<l; i++) {
				if(childNodes[i].username!=undefined){
					childNodes[i].name = childNodes[i].username;
				}
			}
			return childNodes;
		}
	   
		function onClick(event, treeId, treeNode, clickFlag) {
			if(treeNode.level == 2){
				$.ajax({
					url : '<%=request.getContextPath()%>/TbRoleController/QueryRolesUser.action',
					data : {userid : treeNode.id, isAssgned : true},
					success : function(data){
						$('#assigned').children().remove();
						for(var i = 0;i < data.length; i++){
							$('#assigned').append('<li class="role" draggable="true">'+ data[i].name +'</li>');
						}

						$.ajax({
						url : '<%=request.getContextPath()%>/TbRoleController/QueryRolesUser.action',
						data : {userid : treeNode.id, isAssgned : false},
						success : function(data){
							$('#unassigne').children().remove();
								for(var i = 0;i < data.length; i++){
									$('#unassigne').append('<li class="role" draggable="true">'+ data[i].name +'</li>');
								}
								set_dnd();
							}
						});
					}
				});
			}
		}	

	   
		/*删除确认*/
		function del(courseId) {
			var flag = window.confirm("确定要删除此数据吗");
			if (flag) {
				debugger;
				location.href = '<%=request.getContextPath()%>/CourseController/DeleteCourse.action?id=' + courseId;
			}
		}
		
		$(function(){
			zTreeObj = $.fn.zTree.init($("#treeUsers"), setting);
		})
		
		function set_dnd(){
			$dnds_li = $('ul.dnd').children();
			$dnds_panel = $('.panel-body');
						
			[].forEach.call($dnds_li,function(li){
				li.addEventListener('dragstart',handleDragStart,false);
				li.addEventListener('dragenter',handleDragEnter,false);
				li.addEventListener('dragend',handleDragEnd,false);
			});
			
			[].forEach.call($dnds_panel,function(body){
				body.addEventListener('dragend',handleDragEndBody,false);
				body.addEventListener('dragleave',handleDragLeaveBody,false);
				body.addEventListener('dragover',handleDargOverBody,false);
				body.addEventListener('drop',handleDropBody,false);
			});
		}
		
		var dragSrcEl = null;
		var $dragSrcEl = null;
		var dragSrcUL = null;
		var $dragSrcUL = null;
		var dragSrcBody = null;
		var $dragSrcBody = null;
		
		var dropDestUL = null;
		var $dropDestUL = null;
		var dropDestBody = null;
		var $dropDestBody = null;
		
		function handleDragStart(evt){
			dragSrcEl = this;
			$dragSrcEl = $(this);
			dragSrcUL = this.parentNode;
			$dragSrcUL = $(this.parentNode);
			dragSrcBody = this.parentNode.parentNode;
			$dragSrcBody = $(this.parentNode.parentNode);
			
			evt.dataTransfer.effectAllowed = 'all';
			evt.dataTransfer.setData('text/html',$(this).prop("outerHTML"));
		}
	
		function handleDargOver(evt){
			if(evt.preventDefault){
				evt.preventDefault();
			}
	
			evt.dataTransfer.dropEffect = 'move';
		}
		
		function handleDargOverBody(evt){
			if(evt.preventDefault){
				evt.preventDefault();
			}
	
			evt.dataTransfer.dropEffect = 'move';
			this.classList.add('over');
		}
	
		function handleDragEnter(evt){
			dropDestUL = this.parentNode;
			$dropDestUL = $(this.parentNode);
			
		}
		
		function handleDragEnterBody(evt){
			dropDestBody = this.parentNode;
			$dropDestBody = $(this.parentNode);
			this.classList.add('over');
		}
	
		function handleDragLeave(evt){
		}
		
		function handleDragLeaveBody(evt){
			this.classList.remove('over');
		}
	
		/* function handleDrop(evt){
			//在同一张表中移动，不改变颜色
			if(dragSrcUL == dropDestUL){
				
			}else{//不在同一在表中
				var tr_context = evt.dataTransfer.getData('text/html');
			
				console.log(tr_context);
				
				var $tr_context = $(tr_context);
				$dropDestUL.prepend($tr_context);
			}
			
			set_dnd();
		} */
		
		function handleDropBody(evt){
			if(evt.stopPropagation){
				evt.stopPropagation();
			}
			//在同一张表中移动，不改变颜色
			if(dragSrcBody == dropDestBody){
				
			}else{//不在同一在表中
				var tr_context = evt.dataTransfer.getData('text/html');
				var $tr_context = $(tr_context);
				$dropDestUL = $(this).find('ul:first-child');
				$dropDestUL.prepend($tr_context);
				
				$dragSrcEl.remove();
			}
			
			this.classList.remove('over');
			set_dnd();
		}
	
		function handleDragEnd(evt){		
		}
	   
		function handleDragEndBody(evt){		
			this.classList.remove('over');
		}
  
	</script>
</head>
<body>

    <!-- 包含网页头部 -->
    <jsp:include page="Header.jsp"></jsp:include>

    <!-- 主体内容 -->
    <div class="container" style="min-height: 400px;">
		<div class="row">
 			<!-- 左侧 -->
            <div class="col-md-4">
	            <div class="panel panel-success">
						<div class="panel-heading">
							<h3 class="panel-title">用户名称</h3>
						</div>
						<div class="panel-body">
							<div>
							   <ul id="treeUsers" class="ztree"></ul>
							</div>
						</div>
            	</div>
            </div>
            
            <!-- 中侧 -->
            <div class="col-md-4">
	            <div class="panel panel-success">
						<div class="panel-heading">
							<h3 class="panel-title">已分配角色</h3>
						</div>
						<div class="panel-body" draggable="true">
			            	<div class="table-responsive">
								<ul class="dnd" id="assigned" draggable="true"></ul>	
			                </div>
		          	</div>
            	</div>
            </div>

            <!-- 右侧 -->
            <div class="col-md-4">
            	<div class="panel panel-success">
						<div class="panel-heading">
							<h3 class="panel-title">待分配角色</h3>
						</div>
						<div class="panel-body" draggable="true">
							<div class="table-responsive">
								<ul class="dnd" id="unassigne" draggable="true"></ul>	
			                </div>
			           </div>
			      </div>
            </div>
        </div>
    </div>
    
    <!-- 包含网页底部文件 -->
    <jsp:include page="Footer.jsp"></jsp:include>
</body>
</html>