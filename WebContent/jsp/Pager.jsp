<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
				<!-- 分页条 -->				
				
				<nav class="text-right">
				      <ul class="pagination">
				      	<li class="disabled" ><a href="#">页码${pageInfo.pageNum }/${pageInfo.pages }</a></li> 
				      					      
				        <c:if test="${pageInfo.hasPreviousPage}">
					      	<li><a href="${page_url}?currentPage=1">首页</a></li>
					        <li><a href="${page_url}?currentPage=${pageInfo.pageNum - 1}">上页</a></li>
				        </c:if>
				        
				        <c:forEach begin="1" end="${pageInfo.pages}" var="i">
				        	<li ${pageInfo.pageNum == i?'class=disabled':''}><a href="?currentPage=${i}">${i}</a></li>
				        </c:forEach>
				        
				        <c:if test="${pageInfo.hasNextPage}">
					        <li><a href="${page_url}?currentPage=${pageInfo.pageNum + 1}">下页</a></li>
					        <li><a href="${page_url}?currentPage=${pageInfo.pages}">末页</a></li>
				        </c:if> 
				      </ul>
				</nav>
				
				
				
