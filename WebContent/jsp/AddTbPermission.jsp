<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="application/x-www-form-urlencoded; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <title>增加权限</title>
	<link rel='icon' href='<%=request.getContextPath()%>/image/tm.ico ' type=‘image/x-ico’ />
    <link href="<%=request.getContextPath() %>/js/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<%=request.getContextPath() %>/js/validform/css/style.css" rel="stylesheet" type="text/css">
    <link href="<%=request.getContextPath() %>/js/datetimepicker/jquery.datetimepicker.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/validform/js/Validform_v5.3.2.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/datetimepicker/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		$(document).ready(function(e) {
	        
			//一行代码搞定，提交表单的时候会自动触发验证程序
			$('#form1').Validform({
				tiptype:3	
			});
			
	    });
	</script>
</head>
<body>

    <!-- 包含网页头部 -->
    <jsp:include page="Header.jsp"></jsp:include>

    <!-- 主体内容 -->
    <div class="container" style="min-height: 400px;">
        <!-- 导航路径 -->
        <ol class="breadcrumb">
            <li>
                <a href="#">系统管理</a>
            </li>
            <li class="active">权限管理</li> 
        </ol>

        <div class="row">

            <!-- 左侧 -->
            <div class="col-md-3">
            
            </div>

            <!-- 右侧 -->
            <div class="col-md-9">

                <form id="form1" class="form-horizontal" action="<%=request.getContextPath() %>/TbPermissionController/AddTbPermission.action" method="post" role="form">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">权限名</label>
                        <div class="col-sm-10">
                            <input type="text" id="name" name="name" class="form-control" placeholder="请输入角色名" datatype="*"></div>
                    </div>
                    
                    <div class="form-group">
                        <label for="type" class="col-sm-2 control-label">权限类型</label>
                        <div class="col-sm-10">
                            <input id="code" name="type" class="form-control" placeholder="请输入权限类型" datatype="*"></input>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="url" class="col-sm-2 control-label">url</label>
                        <div class="col-sm-10">
                            <input id="url" name="url" class="form-control" placeholder="请输入url" datatype="*"></input>
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="percode" class="col-sm-2 control-label">percode</label>
                        <div class="col-sm-10">
                            <input id="percode" name="percode" class="form-control" placeholder="请输入percode" datatype="*"></input>
                        </div>
                    </div>
                       <div class="form-group">
                        <label for="pid" class="col-sm-2 control-label">pid</label>
                        <div class="col-sm-10">
                            <input id="pid" name="pid" class="form-control" placeholder="请输入pid" datatype="n"></input>
                        </div>
                    </div>
                       <div class="form-group">
                        <label for="sort" class="col-sm-2 control-label">pids</label>
                        <div class="col-sm-10">
                            <input id="sort" name="sort" class="form-control" placeholder="请输入pids" datatype="n"></input>
                        </div>
                    </div>
                    
                       <div class="form-group">
                        <label for="sort" class="col-sm-2 control-label">权值</label>
                        <div class="col-sm-10">
                            <input id="sort" name="sort" class="form-control" placeholder="请输入权值" datatype="n"></input>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="available" class="col-sm-2 control-label">可用</label>
                        <div class="col-sm-10">
                            <input id="available" name="experiement" class="form-control" placeholder="请输入可用" datatype="n"></input>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <button type="submit" class="btn btn-primary">增加权限</button>
                            <button type="button" class="btn btn-success" onclick="history.back();">返回</button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>
    
    <!-- 包含网页底部文件 -->
    <jsp:include page="Footer.jsp"></jsp:include>
</body>
</html>