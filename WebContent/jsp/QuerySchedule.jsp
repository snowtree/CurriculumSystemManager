<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <title>实施进程表</title>
	<link rel='icon' href='<%=request.getContextPath()%>/images/tm.ico ' type=‘image/x-ico’ />
    <link href="<%=request.getContextPath() %>/js/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<%=request.getContextPath() %>/js/validform/css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/validform/js/Validform_v5.3.2.js"></script>
    
    <style>
	    .over{
			border: 2px dashed #F00;
		}
		
		.speical_td{
			display : none;
		}
		
		.total{
			color:#F00;
		}
    </style>
    
	<script type="text/javascript">
		$(function(){
			 $('.right').each(function(){
				 compute_total($(this));
			 });
			 
			set_dnd();
			 
			set_editable();
		})
		
		var dragSrcEl = null;
		var $dragSrcEl = null;
		var dragSrcTBODY = null;
		var dropDestTBODY = null;
		var $dragSrcTBODY = null;
		var $dropDestTBODY = null;

		function handleDragStart(evt){
			this.style.opacity = '0.4';
	
			dragSrcEl = this;
			$dragSrcEl = $(this);
			dragSrcTBODY = this.parentNode;
			$dragSrcTBODY = $(this.parentNode);
	 
			evt.dataTransfer.effectAllowed = 'all';
			evt.dataTransfer.setData('text/html',this.innerHTML);
		}
	
		function handleDargOver(evt){
			if(evt.preventDefault){
				evt.preventDefault();
			}
	
			evt.dataTransfer.dropEffect = 'copy';
			this.classList.add('over');
		}
	
		function handleDragEnter(evt){
			this.classList.add('over');
			dropDestTBODY = this.parentNode;
			$dropDestTBODY = $(this.parentNode);
		}
	
		function handleDragLeave(evt){
			this.classList.remove('over');
		}
	
		function handleDrop(evt){
			if(evt.stopPropagation){
				evt.stopPropagation();
			}
			
			//在同一张表中移动，不改变颜色
			if(dragSrcTBODY == dropDestTBODY){
				dragSrcEl.style.opacity = '1.0';
				//与当前元素不同
				if(dragSrcEl != this){
					dragSrcEl.innerHTML = this.innerHTML;
					this.innerHTML = evt.dataTransfer.getData('text/html');
				}
				
				if($dragSrcTBODY.hasClass('right')){
					set_button_state(true);
				}
			}else{//不在统一在表中
				if($dragSrcTBODY.hasClass('left')){//left ==> right
					var tr_context = "<tr draggable='true' class='course'>" + evt.dataTransfer.getData('text/html') + "</tr>";
					var $tr_context = $(tr_context);
					
					var $child_tds = $tr_context.children("td");
					$child_tds.each(function(index,element){
						$(element).removeClass('speical_td');
					})
					
					$dropDestTBODY.prepend($tr_context);
					
					/////////////////////////////////////////////////////////
					// 重新为所有的元素绑定事件处理
					set_dnd();
					
					set_editable();
					///////////////////////////////////////////////////////
					// 计算小计各个数据
					compute_total($dropDestTBODY);	
					
					set_button_state(true);
				}
			}
	
			return false;
		}
	
		function handleDragEnd(evt){			
			[].forEach.call($dnds, function (col) {
			    col.classList.remove('over');
			});
			
			if(dragSrcTBODY == dropDestTBODY){
				dragSrcEl.style.opacity = '1.0';
				//与当前元素不同
				if(dragSrcEl != this){
					dragSrcEl.innerHTML = this.innerHTML;
					this.innerHTML = evt.dataTransfer.getData('text/html');
				}
			}else{
				if($dragSrcTBODY.hasClass('right')){//right ==> left
					$dragSrcEl.remove();
					compute_total($dragSrcTBODY);
				}
			}
		}
		
		function compute_total($whichbody){
			var total = new Array()
			var $dest_trs = $whichbody.children('tr');
			
			for(var i = 0; i < $($dest_trs[0]).children().length - 1 ; i++){
				total[i] = 0;
			}
			
			for(var i = 0;i < $dest_trs.length; i++){
				var $dest_tds = $($dest_trs[i]).children();
				
				if(i < $dest_trs.length - 1){//前面的行
					
					$dest_tds[4].innerHTML = parseInt($dest_tds[5].innerHTML) + parseInt($dest_tds[6].innerHTML); + "";
					
					for(var j = 0; j < $dest_tds.length ; j ++ ){
						if(j >= 3 && j <= 7){
							total[j] += parseInt($dest_tds[j].innerHTML);
						}
					}
				
					
				}else{//total 行
					for(var j = 0; j < $dest_tds.length ; j ++ ){
						if(j >= 3 && j <= 7){
							$dest_tds[j].innerHTML = total[j] + "";
						}
					}
				}
			}
		}
		
		function set_button_state(state){
			if(state == true){
				$('#submit_id').removeClass('disabled');
			}else{
				$('#submit_id').addClass('disabled');
			}
		}
		
		function set_dnd(){
			$dnds = $('.dnd tr');
			[].forEach.call($dnds,function(tr){
				tr.addEventListener('dragstart',handleDragStart,false);
				tr.addEventListener('dragenter',handleDragEnter,false);
				tr.addEventListener('dragover',handleDargOver,false);
				tr.addEventListener('dragleave',handleDragLeave,false);
				tr.addEventListener('drop',handleDrop,false);
				tr.addEventListener('dragend',handleDragEnd,false);
			})
		}
		//
		function set_editable(){
			$("table .editable").each(function(){
				$(this).dblclick(function(event){
				    var td = $(this);
				    event.preventDefault();
				    event.stopPropagation();
				    
				    // 根据表格文本创建文本框 并加入表表中--文本框的样式自己调整
				    var text = td.text();
				    var $txt = $("<input style='width:35px;height:20px' type='text'>").val(text);
				    $txt.blur(function(){
				        // 失去焦点，保存值。于服务器交互自己再写,最好ajax
				        var newText = $(this).val();
				         
				        // 移除文本框,显示新值
				        $(this).remove();
				        td.text(newText);
				        
				        var tr_element = $(this).parent();
				        var tbody_element = tr_element.parent();
				        compute_total($dropDestTBODY);
				    });
				    td.text("");
				    td.append($txt);
				    $txt.focus();
				});
			})
		}
		
		function print_all_data(){
			<%-- $.ajax({
				url : '<%=request.getContextPath() %>/CurriculumController/ExportSchedule.action',
				type: 'POST',
				datatype : 'json',//后台返回的数据
				//contentType : 'application/json',
				success : function(data){
					console.log("success : " + data);
				}				
			})  --%>
		}
		
		// 保存所有学期的进程表
		function save_all_data(){
			for(var s = 1; s <= 8; s++){
				save_semester_data(s);
			}
			
			set_button_state(false);
		}
		
		function save_semester_data(no){
			var trs = $('#semester' + no + ' tbody').find('tr');
			var curriculums = new Array();
			for(var i = 0;i < trs.length;i++){
				var obj = new Object();
				if($(trs[i]).hasClass('course')){
					var tds = $(trs[i]).find('td');
					
					obj['id'] = 0;
					obj['specialityid'] = 0;
					obj['courseid'] = parseInt($(tds[0]).text());
					obj['semester'] = no;
					obj['start'] = parseInt($(tds[8]).text());
					obj['end'] = parseInt($(tds[9]).text());
					obj['hpw'] = parseInt($(tds[7]).text());
					obj['sort'] = 0;
					
					curriculums.push(obj);
					
					//
				}
			}
			
			console.log(JSON.stringify(curriculums));
			
			$.ajax({
				url : '<%=request.getContextPath() %>/CurriculumController/UpdateCurriculum.action',
				type: 'POST',
				data : { semester : no, curriculums : JSON.stringify(curriculums)},
				datatype : 'json',//后台返回的数据
				//contentType : 'application/json',
				success : function(data){
					console.log("success : " + data);
				}				
			}) 
		}
    </script>
</head>
<body>

    <!-- 包含网页头部 -->
    <jsp:include page="Header.jsp"></jsp:include>

    <!-- 主体内容 -->
    <div class="container" style="min-height: 400px;">
        <div class="row">

            <!-- 左侧 -->
			<div class="col-md-3">
			
			</div>

			<!-- 右侧 -->
            <div class="col-md-9">
	
				<c:forEach items="${allcurriculums}" var="coursetype">
	            	<div class="panel panel-success">
						<div class="panel-heading">
							<h3 class="panel-title">${coursetype.coursetypename }</h3>
						</div>
						<div class="panel-body">
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th style="width: 2%">ID</th>
										<th style="width: 13%">课程名称</th>
										<th style="width: 3%">课程编号</th>
										<th style="width: 3%">学分</th>
										<th style="width: 6%">学期学时</th>
										<th style="width: 6%">理论学时</th>
										<th style="width: 6%">实验学时</th>
										<th style="width: 5%">周学时</th>
										<th style="width: 5%">起始周</th>
										<th style="width: 5%">结束周</th>
										<th style="width: 4%">开课</th>
										<th style="width: 4%">学期</th>
									</tr>
								</thead>
								<tbody >
									<c:forEach items="${coursetype.curriculums}" var="curriculum">
										<tr class="course">
											<td>${curriculum.id }</td>
											<td>${curriculum.name }</td>
											<td>${curriculum.identifier }</td>
											<td>${curriculum.score }</td>
											<td>${curriculum.time }</td>
											<td>${curriculum.theoretical }</td>
											<td>${curriculum.experiement }</td>
											<td>${curriculum.hpw }</td>
											<td>${curriculum.start }</td>
											<td>${curriculum.end }</td>
											<td><c:choose>
												  <c:when test="${curriculum.available}">   
												    	已开 
												  </c:when>
												  <c:when test="${curriculum.available==false}">   
												     	未开
												  </c:when>
												  <c:otherwise>   
												    	未定 
												  </c:otherwise>
												</c:choose></td>
											<td>${curriculum.semester}</td>
										</tr>
									</c:forEach>
									<tr class="total">
										<td>##</td>
										<td>小计</td>
										<td>----</td>
										<td>${coursetype.total.score} </td>
										<td>${coursetype.total.time}</td>
										<td>${coursetype.total.theoretical}</td>
										<td>${coursetype.total.experiement}</td>
										<td>${coursetype.total.hpw}</td>
										<td>----</td>
										<td>----</td>
										<td>----</td>
										<td>----</td>
									</tr>
								</tbody>
							</table>
						</div>
						<!-- panel-body -->
					</div>
				</c:forEach>

				<form role="form"
					action='<%=request.getContextPath()%>/CurriculumController/ExportSchedule.action'>
					<button type="submit" class="btn btn-success">
						<span class="glyphicon glyphicon-print"
							style="padding-right: 5px;"></span>输出Word
					</button>
				</form>
			</div>
    </div>
    
    <!-- 包含网页底部文件 -->
    <jsp:include page="Footer.jsp"></jsp:include>
</body>
</html>