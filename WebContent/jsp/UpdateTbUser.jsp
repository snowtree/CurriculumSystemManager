<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="application/x-www-form-urlencoded; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <title>修改用户信息</title>
	<link rel='icon' href='<%=request.getContextPath()%>/image/tm.ico ' type=‘image/x-ico’ />
    <link href="<%=request.getContextPath() %>/js/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<%=request.getContextPath() %>/js/validform/css/style.css" rel="stylesheet" type="text/css">
    <link href="<%=request.getContextPath() %>/js/datetimepicker/jquery.datetimepicker.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/validform/js/Validform_v5.3.2.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/datetimepicker/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		$(document).ready(function(e) {
	        
			//一行代码搞定，提交表单的时候会自动触发验证程序
			$('#form1').Validform({
				tiptype:3	
			});
			
			
						
	    });
	</script>
</head>
<body>

    <!-- 包含网页头部 -->
    <jsp:include page="Header.jsp"></jsp:include>

    <!-- 主体内容 -->
    <div class="container" style="min-height: 400px;">
         <!-- 导航路径 -->
        <ol class="breadcrumb">
            <li>
                <a href="#">系统管理</a>
            </li>
            <li class="active">用户管理</li> 
        </ol>

        <div class="row">

            <!-- 左侧 -->
            <div class="col-md-3">
       
            </div>

            <!-- 右侧 -->
            <div class="col-md-9">

                <form id="form1" class="form-horizontal" action="<%=request.getContextPath() %>/TbUserController/UpdateTbUser.action" method="post" role="form">
                    <div class="form-group">
                        <label for="companyname" class="col-sm-2 control-label">用户名称</label>
                        <div class="col-sm-10">
                            <input value="${tbUser.username }" type="text" id="username" name="username" class="form-control" placeholder="请输入用户名称" datatype="*" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address" class="col-sm-2 control-label">用户密码</label>
                        <div class="col-sm-10">
                            <input id="address" name="address" class="form-control" placeholder="请输入用户密码" datatype="*"></input>
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="mobile" class="col-sm-2 control-label">手机</label>
                        <div class="col-sm-10">
                            <input id="mobile" name="mobile" class="form-control" value = "${tbUser.mobile}" datatype="*"/>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                        	<input type="hidden" name="id" value="${tbUser.id }">
                            <button type="submit" class="btn btn-primary">修改用户信息</button>
                            <button type="button" class="btn btn-success" onclick="history.back();">返回</button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>
    
    <!-- 包含网页底部文件 -->
    <jsp:include page="Footer.jsp"></jsp:include>
</body>
</html>