<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <title>课程列表</title>
	<link rel='icon' href='<%=request.getContextPath()%>/image/tm.ico ' type=‘image/x-ico’ />
    <link href="<%=request.getContextPath() %>/js/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<%=request.getContextPath() %>/js/validform/css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/validform/js/Validform_v5.3.2.js"></script>
	<script type="text/javascript">	
		/*删除确认*/
		function del(courseId) {
			var flag = window.confirm("确定要删除此数据吗");
			if (flag) {
				debugger;
				location.href = '<%=request.getContextPath()%>/TbUserController/DeleteTbUser.action?id=' + courseId;
			}
		}
	</script>
</head>
<body>

    <!-- 包含网页头部 -->
    <jsp:include page="Header.jsp"></jsp:include>

    <!-- 主体内容 -->
    <div class="container" style="min-height: 400px;">

        <div class="row">

            <!-- 左侧 -->
            <div class="col-md-3">
            
            </div>

            <!-- 右侧 -->
            <div class="col-md-9">
				<div class="table-responsive">
				<table class="table table-striped table-bordered">
	              <thead>
	                <tr>
	                  <th>ID</th>
	                  <th>登录名</th>                                   
	                  <th>操作</th>                                   
	                </tr>
	              </thead>
	              <tbody>
	              	<c:forEach items="${pageInfo.list }" var="user">
	                <tr>
	                  <td>${user.id }</td>
	                  <td>${user.loginname }</td>                                              
	                  <td>
	                  	<a href="<%=request.getContextPath()%>/TbUserController/toUpdateTbUser.action?id=${user.id}">更新</a>
	                  	<a href="javascript:del(${user.id});">删除</a>
	                  </td>
	                </tr>
	                </c:forEach>	                
	              </tbody>
	            </table>			
                </div>
                
                <!-- 包含分页文件 -->
                <jsp:include page="Pager.jsp"/>
                
                <a href="<%=request.getContextPath()%>/jsp/AddTbUser.jsp" class="btn btn-primary btn-block">添加用户</a>
            </div>

        </div>
    </div>
    
    <!-- 包含网页底部文件 -->
    <jsp:include page="Footer.jsp"></jsp:include>
</body>
</html>