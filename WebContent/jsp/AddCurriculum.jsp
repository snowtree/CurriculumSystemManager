<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="application/x-www-form-urlencoded; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <title>增加客户</title>
	<link rel='icon' href='<%=request.getContextPath()%>/image/tm.ico ' type=‘image/x-ico’ />
    <link href="<%=request.getContextPath() %>/js/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<%=request.getContextPath() %>/js/validform/css/style.css" rel="stylesheet" type="text/css">
    <link href="<%=request.getContextPath() %>/js/datetimepicker/jquery.datetimepicker.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/validform/js/Validform_v5.3.2.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/datetimepicker/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		$(document).ready(function(e) {
	        
			//一行代码搞定，提交表单的时候会自动触发验证程序
			$('#form1').Validform({
				tiptype:3	
			});
			
	    });
	</script>
</head>
<body>

    <!-- 包含网页头部 -->
    <jsp:include page="Header.jsp"></jsp:include>

    <!-- 主体内容 -->
    <div class="container" style="min-height: 400px;">
        <!-- 导航路径 -->
        <ol class="breadcrumb">
            <li>
                <a href="#">系统管理</a>
            </li>
            <li class="active">客户管理</li> 
        </ol>

        <div class="row">

            <!-- 左侧 -->
            <div class="col-md-3">
            
            </div>

            <!-- 右侧 -->
            <div class="col-md-9">

                <form id="form1" class="form-horizontal" action="<%=request.getContextPath() %>/CompanyController/AddCompany.action" method="post" role="form">
                    <div class="form-group">
                        <label for="companyname" class="col-sm-2 control-label">客户名称</label>
                        <div class="col-sm-10">
                            <input type="text" id="companyname" name="companyname" class="form-control" placeholder="请输入客户名称" datatype="*"></div>
                    </div>
                    
                    <div class="form-group">
                        <label for="address" class="col-sm-2 control-label">地址</label>
                        <div class="col-sm-10">
                            <input id="address" name="address" class="form-control" placeholder="请输入客户地址" datatype="*"></input>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="manager" class="col-sm-2 control-label">负责人</label>
                        <div class="col-sm-10">
                            <input id="manager" name="manager" class="form-control" placeholder="请输入负责人名称" datatype="*"></input>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="mobile" class="col-sm-2 control-label">手机</label>
                        <div class="col-sm-10">
                            <input id="mobile" name="mobile" class="form-control" placeholder="请输入客户手机" datatype="m"></input>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="phone" class="col-sm-2 control-label">电话</label>
                        <div class="col-sm-10">
                            <input id="phone" name="phone" class="form-control" placeholder="请输入客户电话" ></input>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <button type="submit" class="btn btn-primary">增加客户</button>
                            <button type="button" class="btn btn-success" onclick="history.back();">返回</button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>
    
    <!-- 包含网页底部文件 -->
    <jsp:include page="Footer.jsp"></jsp:include>
</body>
</html>