<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <title>课程列表</title>
	<link rel='icon' href='<%=request.getContextPath()%>/image/tm.ico ' type=‘image/x-ico’ />
    <link href="<%=request.getContextPath() %>/js/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<%=request.getContextPath() %>/js/validform/css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/validform/js/Validform_v5.3.2.js"></script>
	<script type="text/javascript">	
		/*删除确认*/
		function del(courseId) {
			var flag = window.confirm("确定要删除此数据吗");
			if (flag) {
				debugger;
				location.href = '<%=request.getContextPath()%>/CourseController/DeleteCourse.action?id=' + courseId;
			}
		}
		
		function makeCourseCode(){
			$.ajax({
				url : '<%=request.getContextPath()%>/CourseController/MakeCourseCode.action',
				async: false,
				timeout : 30000,//超时时间：30秒
			    dataType : "json",//设置返回数据的格式
				success : function(data){
					console.log(data);
					if(data == 'successful'){
						alert("编号成功");
						window.location.href = window.location.href;
					}else{
						alert("编号失败");
					}
				},
				beforeSend : function() {
							//请求前的处理
					$("#mymodal").modal('show');
				},
				complete : function() {
							//请求完成的处理
					$("#mymodal").modal('hide');
				},
				error : function() {
					//请求出错处理
					$("#mymodal").modal('hide');
				}
			})
		}
	</script>
</head>
<body>

    <!-- 包含网页头部 -->
    <jsp:include page="Header.jsp"></jsp:include>

    <!-- 主体内容 -->
    <div class="container" style="min-height: 400px;">
        <!-- 导航路径 -->
        <ol class="breadcrumb">
            <li>
                <a href="#">课程管理</a>
            </li>
            <li class="active">课程管理</li> 
        </ol>

        <div class="row">

            <!-- 左侧  根据条件查询按钮-->
            <div class="col-md-3">
            	<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">查询课程</h3>
					</div>
					
                <form id="form1" class="form-horizontal" action="<%=request.getContextPath() %>/CourseController/QueryCourse.action" method="post" role="form">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">课程名</label>
                        <div class="col-sm-10">
                            <input type="text" id="name" name="name" class="form-control" placeholder="请输入课程名" datatype="*">
                            
                            </div>
                    </div>            
                      <div class="form-group">	 
                     	<label for="academyid" class="col-sm-1 control-label">所属学院</label>                     
						 <div class="col-sm-10"> 
								<select id="academyid" name="academyid"  class="selectpicker show-tick form-control">
								    <c:forEach items="${academies}" var="academy">	 
								     	<option value="${academy.id}">${academy.name}</option>  
								    </c:forEach>
								</select>	     
						</div>
						       
                    </div>
						  <div class="form-group">
	                        <div class="col-sm-10 col-sm-offset-2">
	                            <button type="submit" class="btn btn-primary">查询课程</button>      
	                        </div>
                   		 </div>	       
                    </form>  
				</div>
            </div>

            <!-- 右侧 -->
            <div class="col-md-9">
				<div class="table-responsive">
				<table class="table table-striped table-bordered">
	              <thead>
	                <tr>
	                  <th>ID</th>
	                  <th>课程名</th>                                    
	                  <th>课程编码</th>                                    
	                  <th>理论学时</th>
	                  <th>实验学时</th>
	                  <th>实验学时单位</th>
	                  <th>学分</th>
	                  <th>开课学院</th>
	                  <th>操作</th>
	                </tr>
	              </thead>
	              <tbody>
	              	<c:forEach items="${pageInfo.list }" var="course">
	                <tr>
	                  <td>${course.id }</td>
	                  <td>${course.name }</td>                                
	                  <td>${course.identifier }</td>                                
	                  <td>${course.theoretical }</td>                                
	                  <td>${course.experiement }</td> 
	                  <c:if test="${course.unit == 0}">
	                  		<td>时</td> 
	                  	</c:if>
	                  	<c:if test="${course.unit == 1}">
	                  		<td>周</td> 
	                  	</c:if>                            
	                  <td>${course.score }</td>                                
	                  <%-- <td>${course.academyid }</td>  --%>
	                   
	                  <c:forEach items="${academies}" var="academy">	                  	
						<c:if test="${academy.id == course.academyid}">
	                  		<td>${academy.name }</td> 
	                  	</c:if>
	                  	<%-- <td>${user.id}</td>  --%>
	                  </c:forEach>                                        
	                  <td>
	                <c:if test="${course.specialityid == specialityid}"> 
	                  		<a href="<%=request.getContextPath()%>/CourseController/toUpdateCourse.action?id=${course.id}">更新</a>
	                  		<a href="javascript:del(${course.id});">删除</a>
	               </c:if> 
	                  </td>
	                </tr>
	                </c:forEach>	                
	              </tbody>
	            </table>			
                </div>
                
                <!-- 包含分页文件 -->
                <jsp:include page="Pager.jsp"/>
                
					<form role="form" action='<%=request.getContextPath()%>/CourseController/toAddCourse.action'>
						<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-plus-sign" style = "padding-right: 5px;"></span>增加课程</button>
		                <button type="button"
							class="btn btn-success" onclick="makeCourseCode();">
							<span class="glyphicon glyphicon-sort-by-order"
								style="padding-right: 5px;"></span>课程编号
						</button>
					</form>
						
            </div>

			

		</div>
    </div>
    
    <!-- 包含网页底部文件 -->
    <jsp:include page="Footer.jsp"></jsp:include>

	<div class="modal" id="mymodal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title">模态弹出窗标题</h4>
				</div>
				<div class="modal-body">
					<p>模态弹出窗主体内容</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary">保存</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

</body>
</html>