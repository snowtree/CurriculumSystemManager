<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

 <style>
	.span_minus:before {
		content: "－";
	}
	
	.span_plus:before {
		content: "+";
	}
</style>

<div>
	<div class="panel-group table-responsive" role="tablist">
		<div class="panel panel-success leftMenu">
			<!-- 利用data-target指定要折叠的分组列表 -->
			<div class="panel-heading" id="collapseListGroupHeading1"
				data-toggle="collapse" data-target="#collapseListGroup1" role="tab">
				<h4 class="panel-title">
					用户管理 <span class="glyphicon glyphicon-plus"></span>
				</h4>
			</div>
			<!-- .panel-collapse和.collapse标明折叠元素 .in表示要显示出来 -->
			<div id="collapseListGroup1" class="panel-collapse collapse"
				role="tabpanel" aria-labelledby="collapseListGroupHeading1">
				<ul class="list-group">
					<a href="<%=request.getContextPath()%>/jsp/AddTbUser.jsp" class="list-group-item"><span class="glyphicon glyphicon-user">&nbsp;</span>添加用户<span class="badge">50</span></a>
					<a href="#" class="list-group-item"><span class="glyphicon glyphicon-user">&nbsp;</span>修改用户<span class="badge">49</span></a>
					<a href="#" class="list-group-item"><span class="glyphicon glyphicon-user">&nbsp;</span>分配角色<span class="badge">49</span></a>
				</ul>
			</div>
		</div>
		<!--panel end-->
		<div class="panel panel-success leftMenu">
			<div class="panel-heading" id="collapseListGroupHeading2"
				data-toggle="collapse" data-target="#collapseListGroup2" role="tab">
				<h4 class="panel-title">
					角色管理 <span class="glyphicon glyphicon-plus"></span>
				</h4>
			</div>
			<div id="collapseListGroup2" class="panel-collapse collapse"
				role="tabpanel" aria-labelledby="collapseListGroupHeading2">
				<ul class="list-group">
					<a href="#" class="list-group-item"><span class="glyphicon glyphicon-user">&nbsp;</span>添加角色<span class="badge">50</span></a>
					<a href="#" class="list-group-item"><span class="glyphicon glyphicon-user">&nbsp;</span>修改角色<span class="badge">49</span></a>
					<a href="#" class="list-group-item"><span class="glyphicon glyphicon-user">&nbsp;</span>指派资源<span class="badge">49</span></a>
				</ul>
			</div>
		</div>
		<!--panel end-->
		<div class="panel panel-success leftMenu">
			<div class="panel-heading" id="collapseListGroupHeading3"
				data-toggle="collapse" data-target="#collapseListGroup3" role="tab">
				<h4 class="panel-title">
					资源管理 <span class="glyphicon glyphicon-plus"></span>
				</h4>
			</div>
			<div id="collapseListGroup3" class="panel-collapse collapse"
				role="tabpanel" aria-labelledby="collapseListGroupHeading3">
				<ul class="list-group">
					<a href="#" class="list-group-item"><span class="glyphicon glyphicon-user">&nbsp;</span>添加用户<span class="badge">50</span></a>
					<a href="#" class="list-group-item"><span class="glyphicon glyphicon-user">&nbsp;</span>修改用户<span class="badge">49</span></a>
				</ul>
			</div>
		</div>
		<!--panel end-->
		<div class="panel panel-success leftMenu">
			<div class="panel-heading" id="collapseListGroupHeading4"
				data-toggle="collapse" data-target="#collapseListGroup4" role="tab">
				<h4 class="panel-title">
					部门管理 <span class="glyphicon glyphicon-plus"></span>
				</h4>
			</div>
			<div id="collapseListGroup4" class="panel-collapse collapse"
				role="tabpanel" aria-labelledby="collapseListGroupHeading4">
				<ul class="list-group">
					<a href="<%=request.getContextPath()%>/jsp/AddSpeciality.jsp" class="list-group-item"><span class="glyphicon glyphicon-user">&nbsp;</span>添加教研室</a>
					<a href="#" class="list-group-item"><span class="glyphicon glyphicon-user">&nbsp;</span>查看教研室<span class="badge">49</span></a>
				</ul>
			</div>
		</div>
	</div>
</div>

<!-- <script type="text/javascript">	
	$(document).ready(function() {
		$('.panel-group').on('hide.bs.collapse show.bs.collapse', '.panel-collapse', function (e) {
			  var $this   = $(this)
			  $this.prev().find("span").toggleClass("glyphicon-plus");
			  $this.prev().find("span").toggleClass("glyphicon-minus");
		 })
	});
</script> -->
