<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="application/x-www-form-urlencoded; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <title>添加教研室</title>
	<link rel='icon' href='<%=request.getContextPath()%>/image/tm.ico ' type=‘image/x-ico’ />
    <link href="<%=request.getContextPath() %>/js/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<%=request.getContextPath() %>/js/validform/css/style.css" rel="stylesheet" type="text/css">
    <link href="<%=request.getContextPath() %>/js/datetimepicker/jquery.datetimepicker.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/validform/js/Validform_v5.3.2.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/datetimepicker/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.panel-collapse').each(function(index,element){
				if($(element).attr('id') == localStorage.whickone){
					$(element).addClass('in');
					$(element).prev().find("span").toggleClass("glyphicon-plus");
					$(element).prev().find("span").toggleClass("glyphicon-minus");
				}
			});
			
			$('.panel-group').on('hide.bs.collapse show.bs.collapse', '.panel-collapse', function (e) {
				  var $this = $(this)
				  
				  
				  if(localStorage.whickone != undefined){
					  var old_id = localStorage.whickone;
					  var $old = $('#' + old_id);
					  
					  $old.removeClass('in');
					  $old.prev().find("span").toggleClass("glyphicon-minus");
					  $old.prev().find("span").toggleClass("glyphicon-plus");
				  }
				  
				  if(typeof(Storage) !== "undefined") {
					  localStorage.whickone = $this.attr('id').toString();
				  } else {
				      console.log("unsuppert storage");
				  }  
				  
				  $this.addClass('in');
				  $this.prev().find("span").toggleClass("glyphicon-plus");
				  $this.prev().find("span").toggleClass("glyphicon-minus");
			});
			
			$.ajax({
				url : '<%=request.getContextPath() %>/TbUserController/QueryAllTbUser.action',
				datatype : 'json',
				success : function(users){
					//空白选项
					$('#tbuserid').append('<option selected="selected" disabled="disabled"  style="display: none" value=""></option>');
					users.forEach(function(user){
						$('#tbuserid').append('<option value=' + user.id + '>' + user.username + '</option>')
					})
				}
			});
			
	 	})
	
	</script>
</head>
<body>

    <!-- 包含网页头部 -->
    <jsp:include page="Header.jsp"></jsp:include>

    <!-- 主体内容 -->
    <div class="container" style="min-height: 400px;">
        <div class="row">

            <!-- 左侧 -->
            <div class="col-md-3">
            	<jsp:include page="SystemManagerMenu.jsp"></jsp:include>
            </div>
            
            <!-- 右侧 -->
            <div class="col-md-9">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">添加教研室</h3>
					</div>
					<div class="panel-body">
				
		                <form id="form1" role="form" action="<%=request.getContextPath() %>/TbUserController/AddSpeciality.action" method="post" role="form">
		                    <div class="form-group">
		                        <label for="name">教研室名称</label>
		                        <div>
		                            <input type="text" id="name" name="name" class="form-control" placeholder="请输入教研室名称" datatype="*">
		                        </div>
		                    </div>
		                    
		                    <div class="form-group">
		                    	<label for="tbuserid">教研室负责人</label>
								  <select class="form-control" id="tbuserid" name="tbuserid">
								  </select>
							</div>
		                    
		                    <button type="submit" class="btn btn-success">增加用户</button>
		                </form>

					</div>
	    		</div>
            </div>
        </div>
    </div>
    
    <!-- 包含网页底部文件 -->
    <jsp:include page="Footer.jsp"></jsp:include>
</body>
</html>