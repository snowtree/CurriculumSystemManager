<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <title>主页</title>
    <link rel='icon' href='<%=request.getContextPath()%>/images/favicon.png ' type=‘image/x-ico’ />
	<link rel='shortcut icon' href='<%=request.getContextPath()%>/images/favicon.png ' />
	<link rel="Bookmark" href='<%=request.getContextPath()%>/images/favicon.png '>
	
    <link href="<%=request.getContextPath() %>/js/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<%=request.getContextPath() %>/js/validform/css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/validform/js/Validform_v5.3.2.js"></script>
	<style>
		.align-center{ 
				margin:0 auto; /* 居中 这个是必须的，，其它的属性非必须 */ 
				
	</style>

	<script type="text/javascript">
		$(document).ready(function(e) {
			
	    });
	</script>
</head>
<body>

    <!-- 包含网页头部 -->
    <jsp:include page="Header.jsp"></jsp:include>
    
    <!-- 主体内容 -->
    <div class="container" style="min-height: 400px;">
        <!-- 导航路径 -->
        <ol class="breadcrumb">
            <li>
                <a href="<%=request.getContextPath()%>/jsp/index.jsp">CSM</a>
            </li>
            <li class="active">
                                           首页
            </li>
        </ol>

        <div class="row">

            <!-- 左侧 -->
            <div class="col-md-12">
            
            	<img src="images/index.jpg" class="img-rounded img-responsive"/>
            
            </div>
            <div id="code1" class="align-center" />
        </div>
    </div>
    <!-- 包含网页底部文件 -->
    <jsp:include page="Footer.jsp"></jsp:include>
    
</body>	
</html>