<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <title>实施进程表</title>
	<link rel='icon' href='<%=request.getContextPath()%>/images/tm.ico ' type=‘image/x-ico’ />
    <link href="<%=request.getContextPath() %>/js/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<%=request.getContextPath() %>/js/validform/css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/validform/js/Validform_v5.3.2.js"></script>
    
    <style>
	    .over{
			border: 2px dashed #F00;
		}
		
		.speical_td{
			display : none;
		}
		
		.total{
			color:#F00;
		}
    </style>
    
	<script type="text/javascript">
		$(function(){
			 $('.right').each(function(){
				 compute_total($(this));
			 });
			 
			set_dnd();
			 
			set_editable();
			
			init();
		})
		
		function init(){
			$("#search").click(function(){
				$tbody = $('.dnd .left');
				
				$.ajax({
					url : '<%=request.getContextPath()%>/CourseController/QueryCourseBy.action',
					data : { name : $('#coursename').val() },
					datatype : 'json',
					success : function(data){
						for(var i=0;i<data.length;i++){
							
						}
					}
				});
			});
		}
		
		var dragSrcEl = null;
		var $dragSrcEl = null;
		var dragSrcTBODY = null;
		var dropDestTBODY = null;
		var $dragSrcTBODY = null;
		var $dropDestTBODY = null;

		function handleDragStart(evt){
			this.style.opacity = '0.4';
	
			dragSrcEl = this;
			$dragSrcEl = $(this);
			dragSrcTBODY = this.parentNode;
			$dragSrcTBODY = $(this.parentNode);
	 
			evt.dataTransfer.effectAllowed = 'all';
			evt.dataTransfer.setData('text/html',this.innerHTML);
		}
	
		function handleDargOver(evt){
			if(evt.preventDefault){
				evt.preventDefault();
			}
	
			evt.dataTransfer.dropEffect = 'copy';
			this.classList.add('over');
		}
	
		function handleDragEnter(evt){
			this.classList.add('over');
			dropDestTBODY = this.parentNode;
			$dropDestTBODY = $(this.parentNode);
		}
	
		function handleDragLeave(evt){
			this.classList.remove('over');
		}
	
		function handleDrop(evt){
			if(evt.stopPropagation){
				evt.stopPropagation();
			}
			
			//在同一张表中移动，不改变颜色
			if(dragSrcTBODY == dropDestTBODY){
				dragSrcEl.style.opacity = '1.0';
				//与当前元素不同
				if(dragSrcEl != this){
					dragSrcEl.innerHTML = this.innerHTML;
					this.innerHTML = evt.dataTransfer.getData('text/html');
				}
				
				if($dragSrcTBODY.hasClass('right')){
					set_button_state(true);
				}
			}else{//不在统一在表中
				if($dragSrcTBODY.hasClass('left')){//left ==> right
					var tr_context = "<tr draggable='true' class='course'>" + evt.dataTransfer.getData('text/html') + "</tr>";
					var $tr_context = $(tr_context);
					
					var $child_tds = $tr_context.children("td");
					$child_tds.each(function(index,element){
						$(element).removeClass('speical_td');
					})
					
					$dropDestTBODY.prepend($tr_context);
					
					/////////////////////////////////////////////////////////
					// 重新为所有的元素绑定事件处理
					set_dnd();
					
					set_editable();
					///////////////////////////////////////////////////////
					// 计算小计各个数据
					compute_total($dropDestTBODY);	
					
					set_button_state(true);
				}
			}
	
			return false;
		}
	
		function handleDragEnd(evt){			
			[].forEach.call($dnds, function (col) {
			    col.classList.remove('over');
			});
			
			if(dragSrcTBODY == dropDestTBODY){
				dragSrcEl.style.opacity = '1.0';
				//与当前元素不同
				if(dragSrcEl != this){
					dragSrcEl.innerHTML = this.innerHTML;
					this.innerHTML = evt.dataTransfer.getData('text/html');
				}
			}else{
				if($dragSrcTBODY.hasClass('right')){//right ==> left
					$dragSrcEl.remove();
					compute_total($dragSrcTBODY);
				}
			}
		}
		
		function compute_total($whichbody){
			var total = new Array();
			var total_disperse = 0;//小计分散
			var theoretical_disperse = 0;//小计理论分散
			var experiment_disperse = 0;//小计实验分散
			var total_hpw = 0;//每周学时数
			var total_week = 0;//总周
			var total_week_disperse = 0;//总分散周
			
			var $dest_trs = $whichbody.children('tr');
			//var $dest_trs_avaliable = $(":checkbox:checked").closest("tr");
			var $dest_trs_avaliable = $whichbody.find("input[class='available']:checked").closest("tr");
			var $dest_trs_disperse = $whichbody.find("input[class='disperse']:checked").closest("tr");
			
			//设置分散的括号==============================================
			for(var i = 0;i < $dest_trs.length; i++){//所有行，先清除分散括号
				$dest_tds = $($dest_trs[i]).children();
			
				for(var j = 4 ; j <= 8; j++){
					str = $($dest_tds[j]).text();
					var s = str.replace("(","");
				    s = s.replace(")","");
					
				    $($dest_tds[j]).text(s);
				}
			}
			
			//先根据理论学时 + 实验学时 计算总学时
			for(var i = 0;i < $dest_trs.length; i++){//所有行
				var $dest_tds = $($dest_trs[i]).children();
				
				if(i < $dest_trs.length - 1){//前面的行 除了最后一行小计
					//计算出总学时（理论学时+实验学时）
					$dest_tds[4].innerHTML = parseFloat($dest_tds[5].innerHTML) + parseFloat($dest_tds[6].innerHTML); + "";
				}
			}
			
			for(var j = 0;j < $dest_trs.length; j++){//所有分散课
				var $dest__tds = $($dest_trs[j]).children();
				if($($dest__tds[12]).text() == '1'){//如果单位为周
					$($dest__tds[7]).text($dest__tds[4].innerHTML);
				}else{//单位为学时
					
				}
			}
			
			//===========================================================
			//初始化小计的数量
			for(var i = 0; i < 20 ; i++){
				total[i] = 0;
			}
			
			//计算开课、分散的小计
			var $dest_tds = undefined;
			for(var i = 0;i < $dest_trs_avaliable.length; i++){//所有开课行
				for(var j = 0;j < $dest_trs_disperse.length; j++){//所有分散课
					if($dest_trs_avaliable[i] == $dest_trs_disperse[j]){//开课，并分散进行
						var $dest_disperse_tds = $($dest_trs_disperse[j]).children();
						if($($dest_disperse_tds[12]).text() == '0'){//单位是学时的累加
							total_disperse += parseFloat($($dest_disperse_tds[4]).text());
							theoretical_disperse += parseFloat($($dest_disperse_tds[5]).text());
							experiment_disperse += parseFloat($($dest_disperse_tds[6]).text());
						}
						
						if($($dest_disperse_tds[12]).text() == '1'){//如果单位为周
							total_week_disperse += (parseFloat($($dest_disperse_tds[7]).text()));//总分散周数
						}
						
					}
				}
			}
			//计算开课、未分散的小计
			for(var i = 0;i < $dest_trs_avaliable.length; i++){//所有开课行
				var found_same = false;
				for(var j = 0;j < $dest_trs_disperse.length; j++){//所有分散课
					if($dest_trs_avaliable[i] == $dest_trs_disperse[j]){//开课，并分散进行
						found_same = true;
						break;
					}
				}
				//所有开课，计算学分学时
				$dest_tdss = $($dest_trs_avaliable[i]).children();
				for(var k = 0; k < $dest_tdss.length ; k ++ ){
					if(k == 3){//学分全累加
						total[k] += parseFloat($dest_tdss[k].innerHTML);
						continue;
					}
				}
				//没有找到相同的，计算
				if(!found_same){
					for(var k = 0; k < $dest_tdss.length ; k ++ ){
						if(k > 3 && k < 7){
							if($($dest_tdss[12]).text() == '0'){//单位是学时的累加
								total[k] += parseFloat($dest_tdss[k].innerHTML);
							}
						}
					}
					
					if($($dest_tdss[12]).text() == '1'){//如果单位为周
						var str = $($dest_tdss[7]).text();
						str = str.replace("周","");
					
						total_week += (parseFloat(str));//总周数
					}else{
						total[7] += parseFloat($dest_tdss[7].innerHTML)
					}
					
				}
				
			}
			
			//设置小计的内容
			$dest_tds = $($dest_trs[$dest_trs.length - 1]).children();
			for(var j = 0; j < $dest_tds.length ; j ++ ){
				if(j >= 3 && j <= 7){
					$dest_tds[j].innerHTML = total[j] + "";
					if(j == 4){//总学时数
						if(total_disperse > 0){
							$dest_tds[j].innerHTML = $dest_tds[j].innerHTML + "(" + total_disperse + ")";
						}
					}
					
					if(j == 5){//总理论学时数
						if(total_disperse > 0){
							$dest_tds[j].innerHTML = $dest_tds[j].innerHTML + "(" + theoretical_disperse + ")";
						}
					}
					
					if(j == 6){//总实验学时数
						if(total_disperse > 0){
							$dest_tds[j].innerHTML = $dest_tds[j].innerHTML + "(" + experiment_disperse + ")";
						}
					}
					
					if(j == 7){//周学时
						if(total_week > 0 || total_week_disperse > 0){
							$dest_tds[7].innerHTML += "/";
						}
					
						if(total_week > 0){
							$dest_tds[7].innerHTML += total_week;
						}
						
						if(total_week_disperse > 0){
							$dest_tds[7].innerHTML += "(" + total_week_disperse + ")";
						}
						
						if(total_week > 0 || total_week_disperse > 0){
							$dest_tds[7].innerHTML += "周";
						}
					}
				}
			}
			
			//对于分散的课学时，加上括号表示
			for(var i = 0 ; i < $dest_trs_disperse.length; i++){//
				var $dest_disperse_tds = $($dest_trs_disperse[i]).children();
				for(var j = 4 ; j <= 6; j++){
					$dest_disperse_tds[j].innerHTML = "(" + $dest_disperse_tds[j].innerHTML + ")";
				}
				
				if($($dest_disperse_tds[12]).text() == '1'){//如果单位为周
					$($dest_disperse_tds[7]).text("(" + $($dest_disperse_tds[7]).text() + ")" +"周");
				}
			}
			//单位为周的，加上单位
			for(var i = 0 ; i < $dest_trs.length - 1; i++){//
				var $tds = $($dest_trs[i]).children();
				
				if($($tds[12]).text() == '1'){//如果单位为周
					$($tds[7]).text($($tds[4]).text() +"周");
					$($tds[4]).css('color','white');
					$($tds[5]).css('color','white');
					$($tds[6]).css('color','white');
				}
			}
			
		}
		
		function set_button_state(state){
			if(state == true){
				$('#submit_id').removeClass('disabled');
			}else{
				$('#submit_id').addClass('disabled');
			}
		}
		
		function set_dnd(){
			$dnds = $('.dnd tr');
			[].forEach.call($dnds,function(tr){
				tr.addEventListener('dragstart',handleDragStart,false);
				tr.addEventListener('dragenter',handleDragEnter,false);
				tr.addEventListener('dragover',handleDargOver,false);
				tr.addEventListener('dragleave',handleDragLeave,false);
				tr.addEventListener('drop',handleDrop,false);
				tr.addEventListener('dragend',handleDragEnd,false);
			})
		}
		//
		function set_editable(){
			$("table .editable").each(function(){
				$(this).dblclick(function(event){
				    var td = $(this);
				    event.preventDefault();
				    event.stopPropagation();
				    
				    // 根据表格文本创建文本框 并加入表表中--文本框的样式自己调整
				    var text = td.text();
				    var $txt = $("<input style='width:35px;height:20px' type='text'>").val(text);
				    $txt.blur(function(){
				        // 失去焦点，保存值。于服务器交互自己再写,最好ajax
				        var newText = $(this).val();
				         
				        // 移除文本框,显示新值
				        $(this).remove();
				        td.text(newText);
				        
				        var tr_element = $(this).parent();
				        var tbody_element = tr_element.parent();
				        compute_total($dropDestTBODY);
				    });
				    td.text("");
				    td.append($txt);
				    $txt.focus();
				});
			})
		}
		
		// 保存所有学期的进程表
		function save_all_data(){
			for(var s = 1; s <= 8; s++){
				save_semester_data(s);
			}
			
			set_button_state(false); 
		}
		
		function save_semester_data(no){
			var trs = $('#semester' + no + ' tbody').find('tr');
			var curriculums = new Array();
			for(var i = 0;i < trs.length;i++){
				var obj = new Object();
				if($(trs[i]).hasClass('course')){
					var tds = $(trs[i]).find('td');
					
					obj['id'] = 0;
					obj['specialityid'] = 0;
					obj['courseid'] = parseInt($(tds[0]).text());
					obj['semester'] = no;
					obj['start'] = parseInt($(tds[8]).text());
					obj['end'] = parseInt($(tds[9]).text());
					obj['hpw'] = parseInt($(tds[7]).text());
					obj['sort'] = 0;
					obj['available'] = tds[10].firstChild.checked;
					obj['disperse'] = tds[11].firstChild.checked;
					//
					curriculums.push(obj);
				}
			}
			
			$.ajax({
				url : '<%=request.getContextPath() %>/CurriculumController/UpdateCurriculum.action',
				type: 'POST',
				data : { semester : no, curriculums : JSON.stringify(curriculums)},
				datatype : 'json',//后台返回的数据
				//contentType : 'application/json',
				success : function(data){
					console.log("success : " + data);
				}				
			})
		}
		
		function checkboxOnclick(element){
			var $TBODY = $(element).parent().parent().parent();
			compute_total($TBODY);			
			set_button_state(true);
		}
    </script>
</head>
<body>

    <!-- 包含网页头部 -->
    <jsp:include page="Header.jsp"></jsp:include>

    <!-- 主体内容 -->
    <div class="container" style="min-height: 400px;">
        <div class="row">

            <!-- 左侧 -->
			<div class="col-md-3">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">备选课程</h3>
					</div>
					<div class="panel-body">
						<div class="form-group">
						    <!--课程名称查询条件  -->
							<label for="coursename">
								请输入您查询的课程名称
							</label>
							<div class="input-group">
								<input type="text" class="form-control" id="coursename" name="coursename" placeholder="课程名称">
								<span class="input-group-btn">
									<button class="btn btn-defalut" type="button" id="search"><span class="glyphicon glyphicon-search" style = "padding-right: 5px;"></span>查询</button>
								</span>
							</div>
							<!--课程类型查询条件  -->
							<label for="coursetype">请输入您要查询课程类型</label>  
					         <div class="input-group">  
					             <input type="text" class="form-control" id="coursetype" name="coursetype" placeholder="课程类型" >  
					             <div class="input-group-btn">  
					                 <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">类型<span class="caret"></span></button>  
					                 <ul id="coursetype_id" class="dropdown-menu pull-right">  
					                 </ul>  
					             </div>  
					         </div>  
						</div>
						<!-- 查询结果表格 -->
						<div class="row pre-scrollable">
							<table class="table table-striped table-bordered" id="course">
								<thead>
									<tr>
										<th style="width: 20%">ID</th>
										<th style="width: 80g%">课程名称</th>
									</tr>
								</thead>
								<tbody class="dnd left">
									<c:forEach items="${courses}" var="course">
						                <tr  draggable="true" class="course">
											<td class="id">${course.id }</td>
											<td>${course.name}</td>
											<td class="speical_td">${course.identifier}</td>
											<td class="speical_td">${course.score}</td>
											<td class="speical_td">0</td>
											<td class="speical_td">${course.theoretical}</td>
											<td class="speical_td">${course.experiement}</td>
											<td class="speical_td editable">0</td>
											<td class="speical_td editable">1</td>
											<td class="speical_td editable">17</td>
											<td class="speical_td"><input class="available" type="checkbox" onclick="checkboxOnclick(this);" /></td>  
											<td class="speical_td"><input class="disperse" type="checkbox" onclick="checkboxOnclick(this);" /></td>  
											<td style="display:none">${course.unit}</td>  
										</tr>
						            </c:forEach>			                  
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<!-- 右侧 -->
            <div class="col-md-9">

            <div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">实施进程表</h3>
					</div>
					<div class="panel-body">
			            <!-- 导航区 -->			
						<ul class="nav nav-tabs" role="tablist">
						  <li role="presentation" class="active"><a href="#semester1" role="tab" data-toggle="tab">第一学期</a></li>
						  <li role="presentation"><a href="#semester2" role="tab" data-toggle="tab">第二学期</a></li>
						  <li role="presentation"><a href="#semester3" role="tab" data-toggle="tab">第三学期</a></li>
						  <li role="presentation"><a href="#semester4" role="tab" data-toggle="tab">第四学期</a></li>
						  <li role="presentation"><a href="#semester5" role="tab" data-toggle="tab">第五学期</a></li>
						  <li role="presentation"><a href="#semester6" role="tab" data-toggle="tab">第六学期</a></li>
						  <li role="presentation"><a href="#semester7" role="tab" data-toggle="tab">第七学期</a></li>
						  <li role="presentation"><a href="#semester8" role="tab" data-toggle="tab">第八学期</a></li>
						</ul>
						 
						<!-- 面板区 -->
						<div class="tab-content">
						  <div role="tabpanel" class="tab-pane active" id="semester1">
						  	<div class="table-responsive">
								<table class="table table-striped table-bordered">
					              <thead>
					                <tr>
					                  <th style="width: 2%">ID</th>
					                  <th style="width: 13%">课程名称</th>                                    
					                  <th style="width: 3%">课程编号</th>                                    
					                  <th style="width: 3%">学分</th>                                    
					                  <th style="width: 6%">学期学时</th>
					                  <th style="width: 6%">理论学时</th>
					                  <th style="width: 6%">实验学时</th>
					                  <th style="width: 5%">周学时</th>
					                  <th style="width: 5%">起始周</th>
					                  <th style="width: 5%">结束周</th>
					                  <th style="width: 4%">开课</th>
					                  <th style="width: 4%">分散</th>
					                </tr>
					              </thead>
					              <tbody class="dnd right">
					              	<c:forEach items="${curriculums1 }" var="curriculum">
						                <tr draggable="true" class="course">
						                  <td>${curriculum.id }</td>
						                  <td>${curriculum.name }</td>
						                  <td>${curriculum.identifier }</td>
						                  <td>${curriculum.score }</td>
						                  <td>${curriculum.time }</td>
						                  <td>${curriculum.theoretical }</td>
						                  <td>${curriculum.experiement }</td>
						                  <td class='editable'>${curriculum.hpw }</td>                                
						                  <td class='editable'>${curriculum.start }</td>                                
						                  <td class='editable'>${curriculum.end }</td>    
						                  <td><input type="checkbox" class="available"
						                  	<c:if test="${curriculum.avaliable}"> checked="checked" </c:if> 
						                  	onclick="checkboxOnclick(this);" />
						                  </td>
						                  <td><input type="checkbox" class="disperse"
						                  	<c:if test="${curriculum.disperse}"> checked="checked" </c:if> 
						                  	onclick="checkboxOnclick(this);" />
						                  </td>
						                  <td style="display:none">${curriculum.unit}</td>                               
						                </tr>
					                </c:forEach>	
						                <tr class="total">
						              		<td>##</td>
							              	<td>小计</td>
							              	<td>----</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>----</td>
							              	<td>----</td>
							              	<td>----</td>
							              	<td>----</td>
						              	</tr>                
					              </tbody>
					            </table>			
			                </div>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="semester2">
						  
							  <div class="table-responsive">
									<table class="table table-striped table-bordered">
					              <thead>
					                <tr>
					                  <th style="width: 2%">ID</th>
					                  <th style="width: 13%">课程名称</th>                                    
					                  <th style="width: 3%">课程编号</th>                                    
					                  <th style="width: 3%">学分</th>                                    
					                  <th style="width: 6%">学期学时</th>
					                  <th style="width: 6%">理论学时</th>
					                  <th style="width: 6%">实验学时</th>
					                  <th style="width: 5%">周学时</th>
					                  <th style="width: 5%">起始周</th>
					                  <th style="width: 5%">结束周</th>
					                  <th style="width: 4%">开课</th>
					                  <th style="width: 4%">分散</th>
					                </tr>
					              </thead>
					              <tbody class="dnd right">
					              	<c:forEach items="${curriculums2 }" var="curriculum">
						                <tr draggable="true" class="course">
						                  <td>${curriculum.id }</td>
						                  <td>${curriculum.name }</td>
						                  <td>${curriculum.identifier }</td>
						                  <td>${curriculum.score }</td>
						                  <td>${curriculum.time }</td>
						                  <td>${curriculum.theoretical }</td>
						                  <td>${curriculum.experiement }</td>
						                  <td class='editable'>${curriculum.hpw }</td>                                
						                  <td class='editable'>${curriculum.start }</td>                                
						                  <td class='editable'>${curriculum.end }</td>    
						                  <td><input type="checkbox" class="available"
						                  	<c:if test="${curriculum.avaliable}"> checked="checked" </c:if> 
						                  	onclick="checkboxOnclick(this);" />
						                  </td>                 
						                  <td><input type="checkbox" class="disperse"
						                  	<c:if test="${curriculum.disperse}"> checked="checked" </c:if> 
						                  	onclick="checkboxOnclick(this);" />
						                  </td>  
						                  <td style="display:none">${curriculum.unit}</td>            
						                </tr>
					                </c:forEach>	
						                <tr class="total">
						              		<td>##</td>
							              	<td>小计</td>
							              	<td>----</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>----</td>
							              	<td>----</td>
							              	<td>----</td>
							              	<td>----</td>
						              	</tr>                
					              </tbody>
					            </table>						
				                </div>
						  
						  </div>
						  <div role="tabpanel" class="tab-pane" id="semester3">
						  	
						  	<div class="table-responsive">
									<table class="table table-striped table-bordered">
					              <thead>
					                <tr>
					                  <th style="width: 2%">ID</th>
					                  <th style="width: 13%">课程名称</th>                                    
					                  <th style="width: 3%">课程编号</th>                                    
					                  <th style="width: 3%">学分</th>                                    
					                  <th style="width: 6%">学期学时</th>
					                  <th style="width: 6%">理论学时</th>
					                  <th style="width: 6%">实验学时</th>
					                  <th style="width: 5%">周学时</th>
					                  <th style="width: 5%">起始周</th>
					                  <th style="width: 5%">结束周</th>
					                  <th style="width: 4%">开课</th>
					                  <th style="width: 4%">分散</th>
					                </tr>
					              </thead>
					              <tbody class="dnd right">
					              	<c:forEach items="${curriculums3 }" var="curriculum">
						                <tr draggable="true" class="course">
						                  <td>${curriculum.id }</td>
						                  <td>${curriculum.name }</td>
						                  <td>${curriculum.identifier }</td>
						                  <td>${curriculum.score }</td>
						                  <td>${curriculum.time }</td>
						                  <td>${curriculum.theoretical }</td>
						                  <td>${curriculum.experiement }</td>
						                  <td class='editable'>${curriculum.hpw }</td>                                
						                  <td class='editable'>${curriculum.start }</td>                                
						                  <td class='editable'>${curriculum.end }</td>    
						                  <td><input type="checkbox" class="available"
						                  	<c:if test="${curriculum.avaliable}"> checked="checked" </c:if> 
						                  	onclick="checkboxOnclick(this);" />
						                  </td>            
						                  <td><input type="checkbox" class="disperse"
						                  	<c:if test="${curriculum.disperse}"> checked="checked" </c:if> 
						                  	onclick="checkboxOnclick(this);" />
						                  </td>
						                  <td style="display:none">${curriculum.unit}</td>                   
						                </tr>
					                </c:forEach>	
						                <tr class="total">
						              		<td>##</td>
							              	<td>小计</td>
							              	<td>----</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>----</td>
							              	<td>----</td>
							              	<td>----</td>
							              	<td>----</td>
						              	</tr>                
					              </tbody>
					            </table>			
				                </div>
						  	
						  </div>
						  <div role="tabpanel" class="tab-pane" id="semester4">
						  	
						  	<div class="table-responsive">
									<table class="table table-striped table-bordered">
					              <thead>
					                <tr>
					                  <th style="width: 2%">ID</th>
					                  <th style="width: 13%">课程名称</th>                                    
					                  <th style="width: 3%">课程编号</th>                                    
					                  <th style="width: 3%">学分</th>                                    
					                  <th style="width: 6%">学期学时</th>
					                  <th style="width: 6%">理论学时</th>
					                  <th style="width: 6%">实验学时</th>
					                  <th style="width: 5%">周学时</th>
					                  <th style="width: 5%">起始周</th>
					                  <th style="width: 5%">结束周</th>
					                  <th style="width: 4%">开课</th>
					                  <th style="width: 4%">分散</th>
					                </tr>
					              </thead>
					              <tbody class="dnd right">
					              	<c:forEach items="${curriculums4 }" var="curriculum">
						                <tr draggable="true" class="course">
						                  <td>${curriculum.id }</td>
						                  <td>${curriculum.name }</td>
						                  <td>${curriculum.identifier }</td>
						                  <td>${curriculum.score }</td>
						                  <td>${curriculum.time }</td>
						                  <td>${curriculum.theoretical }</td>
						                  <td>${curriculum.experiement }</td>
						                  <td class='editable'>${curriculum.hpw }</td>                                
						                  <td class='editable'>${curriculum.start }</td>                                
						                  <td class='editable'>${curriculum.end }</td>    
						                  <td><input type="checkbox" class="available"
						                  	<c:if test="${curriculum.avaliable}"> checked="checked" </c:if> 
						                  	onclick="checkboxOnclick(this);" />
						                  </td>  
						                  <td><input type="checkbox" class="disperse"
						                  	<c:if test="${curriculum.disperse}"> checked="checked" </c:if> 
						                  	onclick="checkboxOnclick(this);" />
						                  </td>
						                  <td style="display:none">${curriculum.unit}</td>                             
						                </tr>
					                </c:forEach>	
						                <tr class="total">
						              		<td>##</td>
							              	<td>小计</td>
							              	<td>----</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>----</td>
							              	<td>----</td>
							              	<td>----</td>
							              	<td>----</td>
						              	</tr>                
					              </tbody>
					            </table>			
				                </div>
						  	
						  </div>
						  <div role="tabpanel" class="tab-pane" id="semester5">
						  	<table class="table table-striped table-bordered">
					              <thead>
					                <tr>
					                  <th style="width: 2%">ID</th>
					                  <th style="width: 13%">课程名称</th>                                    
					                  <th style="width: 3%">课程编号</th>                                    
					                  <th style="width: 3%">学分</th>                                    
					                  <th style="width: 6%">学期学时</th>
					                  <th style="width: 6%">理论学时</th>
					                  <th style="width: 6%">实验学时</th>
					                  <th style="width: 5%">周学时</th>
					                  <th style="width: 5%">起始周</th>
					                  <th style="width: 5%">结束周</th>
					                  <th style="width: 4%">开课</th>
					                  <th style="width: 4%">分散</th>
					                </tr>
					              </thead>
					              <tbody class="dnd right">
					              	<c:forEach items="${curriculums5 }" var="curriculum">
						                <tr draggable="true" class="course">
						                  <td>${curriculum.id }</td>
						                  <td>${curriculum.name }</td>
						                  <td>${curriculum.identifier }</td>
						                  <td>${curriculum.score }</td>
						                  <td>${curriculum.time }</td>
						                  <td>${curriculum.theoretical }</td>
						                  <td>${curriculum.experiement }</td>
						                  <td class='editable'>${curriculum.hpw }</td>                                
						                  <td class='editable'>${curriculum.start }</td>                                
						                  <td class='editable'>${curriculum.end }</td>    
						                  <td><input type="checkbox" class="available"
						                  	<c:if test="${curriculum.avaliable}"> checked="checked" </c:if> 
						                  	onclick="checkboxOnclick(this);" />
						                  </td>        
						                  <td><input type="checkbox" class="disperse"
						                  	<c:if test="${curriculum.disperse}"> checked="checked" </c:if> 
						                  	onclick="checkboxOnclick(this);" />
						                  </td> 
						                  <td style="display:none">${curriculum.unit}</td>                      
						                </tr>
					                </c:forEach>	
						                <tr class="total">
						              		<td>##</td>
							              	<td>小计</td>
							              	<td>----</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>----</td>
							              	<td>----</td>
							              	<td>----</td>
							              	<td>----</td>
						              	</tr>                
					              </tbody>
					            </table>			
						  </div>
						  <div role="tabpanel" class="tab-pane" id="semester6">
						  	<table class="table table-striped table-bordered">
					              <thead>
					                <tr>
					                  <th style="width: 2%">ID</th>
					                  <th style="width: 13%">课程名称</th>                                    
					                  <th style="width: 3%">课程编号</th>                                    
					                  <th style="width: 3%">学分</th>                                    
					                  <th style="width: 6%">学期学时</th>
					                  <th style="width: 6%">理论学时</th>
					                  <th style="width: 6%">实验学时</th>
					                  <th style="width: 5%">周学时</th>
					                  <th style="width: 5%">起始周</th>
					                  <th style="width: 5%">结束周</th>
					                  <th style="width: 4%">开课</th>
					                  <th style="width: 4%">分散</th>
					                </tr>
					              </thead>
					              <tbody class="dnd right">
					              	<c:forEach items="${curriculums6 }" var="curriculum">
						                <tr draggable="true" class="course">
						                  <td>${curriculum.id }</td>
						                  <td>${curriculum.name }</td>
						                  <td>${curriculum.identifier }</td>
						                  <td>${curriculum.score }</td>
						                  <td>${curriculum.time }</td>
						                  <td>${curriculum.theoretical }</td>
						                  <td>${curriculum.experiement }</td>
						                  <td class='editable'>${curriculum.hpw }</td>                                
						                  <td class='editable'>${curriculum.start }</td>                                
						                  <td class='editable'>${curriculum.end }</td>    
						                  <td><input type="checkbox" class="available"
						                  	<c:if test="${curriculum.avaliable}"> checked="checked" </c:if> 
						                  	onclick="checkboxOnclick(this);" />
						                  </td>              
						                  <td><input type="checkbox" class="disperse"
						                  	<c:if test="${curriculum.disperse}"> checked="checked" </c:if> 
						                  	onclick="checkboxOnclick(this);" />
						                  </td>  
						                  <td style="display:none">${curriculum.unit}</td>               
						                </tr>
					                </c:forEach>	
						                <tr class="total">
						              		<td>##</td>
							              	<td>小计</td>
							              	<td>----</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>----</td>
							              	<td>----</td>
							              	<td>----</td>
							              	<td>----</td>
						              	</tr>                
					              </tbody>
					            </table>			
						  </div>
						  <div role="tabpanel" class="tab-pane" id="semester7">
						  	<table class="table table-striped table-bordered">
					              <thead>
					                <tr>
					                  <th style="width: 2%">ID</th>
					                  <th style="width: 13%">课程名称</th>                                    
					                  <th style="width: 3%">课程编号</th>                                    
					                  <th style="width: 3%">学分</th>                                    
					                  <th style="width: 6%">学期学时</th>
					                  <th style="width: 6%">理论学时</th>
					                  <th style="width: 6%">实验学时</th>
					                  <th style="width: 5%">周学时</th>
					                  <th style="width: 5%">起始周</th>
					                  <th style="width: 5%">结束周</th>
					                  <th style="width: 4%">开课</th>
					                  <th style="width: 4%">分散</th>
					                </tr>
					              </thead>
					              <tbody class="dnd right">
					              	<c:forEach items="${curriculums7 }" var="curriculum">
						                <tr draggable="true" class="course">
						                  <td>${curriculum.id }</td>
						                  <td>${curriculum.name }</td>
						                  <td>${curriculum.identifier }</td>
						                  <td>${curriculum.score }</td>
						                  <td>${curriculum.time }</td>
						                  <td>${curriculum.theoretical }</td>
						                  <td>${curriculum.experiement }</td>
						                  <td class='editable'>${curriculum.hpw }</td>                                
						                  <td class='editable'>${curriculum.start }</td>                                
						                  <td class='editable'>${curriculum.end }</td>    
						                  <td><input type="checkbox" class="available"
						                  	<c:if test="${curriculum.avaliable}"> checked="checked" </c:if> 
						                  	onclick="checkboxOnclick(this);" />
						                  </td>        
						                  <td><input type="checkbox" class="disperse"
						                  	<c:if test="${curriculum.disperse}"> checked="checked" </c:if> 
						                  	onclick="checkboxOnclick(this);" />
						                  </td>
						                  <td style="display:none">${curriculum.unit}</td>                       
						                </tr>
					                </c:forEach>	
						                <tr class="total">
						              		<td>##</td>
							              	<td>小计</td>
							              	<td>----</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>----</td>
							              	<td>----</td>
							              	<td>----</td>
							              	<td>----</td>
						              	</tr>                
					              </tbody>
					            </table>			
						  </div>
						  <div role="tabpanel" class="tab-pane" id="semester8">
						  	<table class="table table-striped table-bordered">
					              <thead>
					                <tr>
					                  <th style="width: 2%">ID</th>
					                  <th style="width: 13%">课程名称</th>                                    
					                  <th style="width: 3%">课程编号</th>                                    
					                  <th style="width: 3%">学分</th>                                    
					                  <th style="width: 6%">学期学时</th>
					                  <th style="width: 6%">理论学时</th>
					                  <th style="width: 6%">实验学时</th>
					                  <th style="width: 5%">周学时</th>
					                  <th style="width: 5%">起始周</th>
					                  <th style="width: 5%">结束周</th>
					                  <th style="width: 4%">开课</th>
					                  <th style="width: 4%">分散</th>
					                </tr>
					              </thead>
					              <tbody class="dnd right">
					              	<c:forEach items="${curriculums8 }" var="curriculum">
						                <tr draggable="true" class="course">
						                  <td>${curriculum.id }</td>
						                  <td>${curriculum.name }</td>
						                  <td>${curriculum.identifier }</td>
						                  <td>${curriculum.score }</td>
						                  <td>${curriculum.time }</td>
						                  <td>${curriculum.theoretical }</td>
						                  <td>${curriculum.experiement }</td>
						                  <td class='editable'>${curriculum.hpw }</td>                                
						                  <td class='editable'>${curriculum.start }</td>                                
						                  <td class='editable'>${curriculum.end }</td>    
						                  <td><input type="checkbox" class="available"
						                  	<c:if test="${curriculum.avaliable}"> checked="checked" </c:if> 
						                  	onclick="checkboxOnclick(this);" />
						                  </td> 
						                  <td><input type="checkbox" class="disperse"
						                  	<c:if test="${curriculum.disperse}"> checked="checked" </c:if> 
						                  	onclick="checkboxOnclick(this);" />
						                  </td> 
						                  <td style="display:none">${curriculum.unit}</td>                            
						                </tr>
					                </c:forEach>	
						                <tr class="total">
						              		<td>##</td>
							              	<td>小计</td>
							              	<td>----</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>0</td>
							              	<td>----</td>
							              	<td>----</td>
							              	<td>----</td>
							              	<td>----</td>
						              	</tr>                
					              </tbody>
					            </table>								  						  
						  </div>
						</div>
						
		                <form role="form" action='<%=request.getContextPath() %>/CurriculumController/ExportCurriculum.action'>
		                	<button id="submit_id" type="button" class="btn btn-success disabled" onclick="save_all_data();"><span class="glyphicon glyphicon-floppy-save" style = "padding-right: 5px;"></span>保存修改</button>
						  	<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-print" style = "padding-right: 5px;"></span>输出Word</button>
						</form>

                </div><!-- panel-body -->
            </div>

        </div>
    </div>
    
    <!-- 包含网页底部文件 -->
    <jsp:include page="Footer.jsp"></jsp:include>
</body>
</html>