<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!--网页底部-->
	<hr>
	
	<footer>
		<div class="container">			
			<div class="row">
				<div class="col-md-12">
					<h4 class="text-center">哈尔滨商业大学</h4>
					<p>
						本网站由<a href="http://www.hrbcu.edu.cn/">哈尔滨商业大学</a>计算机与信息工程学院软件工程教研室研发，该部门负责相关软件、文档维护和推广工作。该部门拥有该软件产品的所有权利。
					</p>
				</div>
			</div>
		</div>
	</footer>
	<!--网页底部结束-->
	
	<hr>
	<p class="text-center">&copy; 计算机与信息工程学院 2019 </p></html>